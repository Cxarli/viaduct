import json

import pytest

from app.models.oauth.client import OAuthClient
from app.models.oauth.token import OAuthToken
from conftest import CustomClient


@pytest.fixture
def via_owned_client(db_session):
    oauth_client = OAuthClient(
        client_id="via_owned_client",
        client_secret="via_owned_client",
        auto_approve=False,
    )
    oauth_client.set_client_metadata(
        {
            "client_name": "via_owned_client",
            "response_type": ["code", "token"],
            "scope": "",
            "token_endpoint_auth_method": "client_secret_post",
        }
    )

    db_session.add(oauth_client)
    db_session.commit()
    return oauth_client


@pytest.fixture
def user_owned_client(db_session, admin_user):
    oauth_client = OAuthClient(
        client_id="user_owned_client",
        client_secret="user_owned_client",
        auto_approve=False,
        user=admin_user,
    )
    oauth_client.set_client_metadata(
        {
            "client_name": "user_owned_client",
            "response_type": ["code", "token"],
            "scope": "",
            "token_endpoint_auth_method": "client_secret_post",
        }
    )

    db_session.add(oauth_client)
    db_session.commit()
    return oauth_client


@pytest.fixture
def via_owned_client_token(db_session, admin_user, via_owned_client):
    oauth_token = OAuthToken(
        user=admin_user,
        client_id=via_owned_client.client_id,
        token_type="Bearer",
        access_token="via_owned_access_token",
        issued_at=0,
        expires_in=2069451803,
        scope="",
    )
    db_session.add(oauth_token)
    db_session.commit()
    return oauth_token


@pytest.fixture
def user_owned_client_token(db_session, admin_user, user_owned_client):
    oauth_token = OAuthToken(
        user=admin_user,
        client_id=user_owned_client.client_id,
        token_type="Bearer",
        access_token="user_owned_access_token",
        issued_at=0,
        expires_in=2069451803,
        scope="",
    )
    db_session.add(oauth_token)
    db_session.commit()
    return oauth_token


def test_oauth_introspection(
    admin_client: CustomClient, oauth_client: OAuthClient, admin_token: OAuthToken
):
    rv = admin_client.post(
        "/oauth/introspect",
        data={
            "token": admin_token.access_token,
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret
            # Add base URL, because we require token introspection to be on https.
        },
        base_url="https://svia.nl/",
    )

    data = json.loads(rv.data)
    assert data["active"] is True
    assert data["aud"] == "admin"
    assert data["client_id"] == oauth_client.client_id
    assert "full_name" in data
    assert data["iss"] == "https://svia.nl/"
    assert data["token_type"] == "Bearer"
    assert "username" in data


def test_user_applications(
    admin_client,
    admin_user,
    via_owned_client_token: OAuthToken,
    user_owned_client_token,
):
    rv = admin_client.get(f"/api/users/{admin_user.id}/applications")
    assert rv.status_code == 200
    # The admin client and two normal clients
    assert len(rv.json) == 3

    rv = admin_client.post(
        f"/api/users/{admin_user.id}/applications/"
        f"{via_owned_client_token.client_id}/revoke"
    )
    assert rv.status_code == 204

    rv = admin_client.get(f"/api/users/{admin_user.id}/applications")
    assert rv.status_code == 200
    # The admin client and two normal clients
    assert len(rv.json) == 2

    rv = admin_client.post(
        f"/api/users/{admin_user.id}/applications/"
        f"{user_owned_client_token.client_id}/revoke"
    )
    assert rv.status_code == 204

    rv = admin_client.get(f"/api/users/{admin_user.id}/applications")
    assert rv.status_code == 200
    # The admin client and two normal clients
    assert len(rv.json) == 1
