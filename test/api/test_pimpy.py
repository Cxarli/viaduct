from app.models.meeting import Meeting
from app.models.pimpy import Task


def test_minute(admin_client, admin_group):
    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes",
        json={
            "date": "2020-09-01",
            "content": "content,",
            "group_id": admin_group.id,
        },
    )
    assert rv.status_code == 201, rv.data

    minute_id = rv.json["id"]
    rv = admin_client.get(f"/api/minutes/{minute_id}")
    assert rv.status_code == 200, rv.data

    rv = admin_client.get("/api/minutes")
    assert rv.status_code == 200, rv.data
    admin_group_minutes = rv.json[0]
    assert admin_group_minutes["group"]["id"] == admin_group.id
    assert admin_group_minutes["minutes"][0]["id"] == minute_id

    rv = admin_client.get(f"/api/groups/{admin_group.id}/minutes")
    assert rv.status_code == 200, rv.data
    admin_group_minutes = rv.json[0]
    assert admin_group_minutes["group"]["id"] == admin_group.id
    assert admin_group_minutes["minutes"][0]["id"] == minute_id


def test_minute_tasks_meeting_created(
    admin_client, admin_group, admin_user, member_user, db_session
):
    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes",
        json={
            "date": "2020-09-01",
            "content": f"ACTIE {admin_user.first_name}: Do something 1",
            "group_id": admin_group.id,
        },
    )
    assert rv.status_code == 201, rv.json

    task = db_session.query(Task).one()

    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes",
        json={
            "date": "2020-09-01",
            "content": f"""
ACTIE {admin_user.first_name}: Do something 2
DONE {task.b32_id}
ACTIES {admin_user.first_name}, {member_user.first_name}: Do something 3
MEETING 24-09-2020 1:00 test without length
MEETING 24-9-2020 15:00(9,9) test length as float with ,
MEETING 24-9-2020 21:01(9.9) test length as float with .
MEETING 25-07-2021 16:10(9) test length as int
            """,
            "group_id": admin_group.id,
        },
    )

    assert db_session.query(Task).count() == 4
    assert db_session.query(Meeting).count() == 4


def test_task(admin_client, admin_group, admin_user):
    rv = admin_client.post(
        "/api/tasks/",
        json={
            "title": "2020-09-01",
            "users": [admin_user.name],
            "group_id": admin_group.id,
            "status": "new",
        },
    )
    assert rv.status_code == 201, rv.data
    task_id = rv.json["b32_id"]

    rv = admin_client.get("/api/tasks/")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    rv = admin_client.get("/api/users/self/tasks")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    rv = admin_client.get(f"/api/groups/{admin_group.id}/tasks")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    rv = admin_client.patch(
        f"/api/tasks/{task_id}",
        json={
            "title": "new_title",
            "content": "content",
            "users": [admin_user.name],
            "status": "done",
        },
    )
    assert rv.status_code == 200
