def test_api_company_job(anonymous_client):
    rv = anonymous_client.get("/api/jobs/1/")
    assert rv.status_code == 404


def test_non_existing_company(admin_client):
    rv = admin_client.get("/api/companies/100000/")
    assert rv.status_code == 404


def test_get_company(admin_client, company_factory):
    c = company_factory()
    rv = admin_client.get(f"/api/companies/{c.id}/")
    assert rv.status_code == 200


def test_get_company_anonymous(anonymous_client, company_factory):
    c = company_factory()
    rv = anonymous_client.get(f"/api/companies/{c.id}/")
    assert rv.status_code == 401


def test_get_company_public_info(admin_client, company_factory):
    c = company_factory()
    rv = admin_client.get(f"/api/companies/{c.id}/public/")
    assert rv.status_code == 200


def test_get_company_public_info_anonymous(anonymous_client, company_factory):
    c = company_factory()
    rv = anonymous_client.get(f"/api/companies/{c.id}/public/")
    assert rv.status_code == 200


JOB_DATA = {
    "enabled": True,
    "start_date": "2020-02-10",
    "end_date": "2020-02-11",
    "title_nl": "title_nl",
    "title_en": "title_en",
    "contact_name": "Maico Timmerman",
    "contact_email": "john.doe@svia.nl",
    "contact_address": "Street",
    "contact_city": "city",
    "contract_of_service": "full-time",
    "website": "https://svia.nl",
    "phone": "0612345678",
    "description_nl": "description_nl",
    "description_en": "description_en",
}


def test_create_get_company_job(admin_client, company_factory):
    company = company_factory()
    rv = admin_client.post(f"/api/companies/{company.id}/jobs/", json=JOB_DATA.copy())
    assert rv.status_code == 201, rv.json
    job_id = rv.json["id"]

    new_data = JOB_DATA.copy()
    new_data["enabled"] = False
    rv = admin_client.put(f"/api/jobs/{job_id}/", json=new_data)
    assert rv.status_code == 200, rv.json

    rv = admin_client.get(f"/api/jobs/{job_id}/")
    assert rv.status_code == 200, rv.json
    assert rv.json["enabled"] is False
