import copy
from typing import Any, Dict


def test_page_non_existing(admin_client):
    page_id = 123456789
    rv = admin_client.get(f"/api/pages/{page_id}/")
    assert rv.status_code == 404

    rv = admin_client.get(f"/api/pages/{page_id}/rev/latest")
    assert rv.status_code == 404


DEFAULT_PAGE_REVISION: Dict[str, Any] = {
    "title": {"en": "en_title2", "nl": "nl_title2"},
    "content": {"en": "en_content2", "nl": "nl_content2"},
    "revision_comment": "comment",
}


def test_page_without_form(admin_client, page_revision):
    page, revision = page_revision
    rv = admin_client.get(f"/api/pages/{page.id}/")
    assert rv.status_code == 200

    rv = admin_client.get(f"/api/pages/{page.id}/rev/latest")
    assert rv.status_code == 200
    assert rv.json["content"]["en"]
    assert rv.json["content"]["nl"]
    assert rv.json["title"]["en"]
    assert rv.json["title"]["nl"]

    json = copy.deepcopy(DEFAULT_PAGE_REVISION)
    rv = admin_client.post(f"/api/pages/{page.id}/rev/", json=json)
    assert rv.status_code == 200


def test_page_delete_republish(admin_client, anonymous_client, page_revision):
    page, revision = page_revision

    #  Deletes the page
    rv = admin_client.delete(f"/api/pages/{page.id}")
    assert rv.status_code == 204  # No content

    rv = anonymous_client.get(f"/{page.path}")
    assert rv.status_code == 410  # Gone

    #  Republishes the page
    rv = admin_client.put(
        f"/api/pages/{page.id}",
        json={"path": page.path, "require_membership_to_view": False},
    )
    assert rv.status_code == 200

    #  The page is now available again
    rv = anonymous_client.get(f"/{page.path}")
    assert rv.status_code == 200


def test_page_no_authorization(
    admin_client, db_session, anonymous_client, page_revision
):
    page, revision = page_revision

    page.needs_paid = False

    db_session.commit()

    rv = anonymous_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 200

    rv = admin_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 200


def test_page_needs_authorization(
    admin_client, db_session, anonymous_client, page_revision
):
    page, revision = page_revision

    page.needs_paid = True

    db_session.commit()

    rv = anonymous_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 401

    rv = admin_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 200


def test_page_needs_authorization_unpaid_user(
    admin_client,
    db_session,
    unpaid_user_client,
    anonymous_client,
    page_revision,
    unpaid_user,
):
    page, revision = page_revision

    page.needs_paid = True

    db_session.commit()

    rv = anonymous_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 401

    rv = unpaid_user_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 403

    rv = admin_client.get(f"/api/pages/render/nl/{page.path}")
    assert rv.status_code == 200
