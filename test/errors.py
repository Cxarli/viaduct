import pytest


def test_404_non_existing_route(anonymous_client):
    rv = anonymous_client.get("/somepaththatwillneverexist")
    assert rv.status_code == 404
    assert rv.content_type.startswith("text/html")


@pytest.mark.parametrize(
    "url", ["/api/somepaththatwillneverexist", "/api/pages/astringinsteadofaninteger"]
)
def test_api_404_non_existing_route(url, anonymous_client):
    rv = anonymous_client.get(url)
    assert rv.status_code == 404
    assert rv.content_type == "application/json"
    assert rv.json == {
        "title": "Not Found",
        "code": 404,
        "type": "about:blank",
        "detail": "The requested URL was not found on the server. "
        "If you entered the URL manually please check your "
        "spelling and try again.",
    }


@pytest.mark.parametrize("object_url", [("activity", "activities"), ("page", "pages")])
def test_api_404_non_existing_object(object_url, anonymous_client):
    object_type, url = object_url
    rv = anonymous_client.get(f"/api/{url}/123456789")
    assert rv.status_code == 404
    assert rv.content_type == "application/json"
    assert rv.json == {
        "title": "Not Found",
        "code": 404,
        "type": "about:blank",
        "detail": f"Cannot find {object_type} identified by 123456789.",
    }


def test_api_400_bad_request(admin_client):
    rv = admin_client.post("/api/activities", json={})
    assert rv.status_code == 400
    assert rv.json["data"]["errors"]
