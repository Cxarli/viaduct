from sqlalchemy import select

from app.models.course import Course, EducationCourse
from app.service import datanose_service
from app.task.datanose import sync_datanose_courses

EDUCATION_CODE = "BScNonExisting"
COURSE_CODE1 = "NonExistingCode"
COURSE_CODE2 = "NonExistingCode2"

PROGRAMME_COURSES_RESPONSE = [
    {
        "CatalogNumber": COURSE_CODE1,
        "Name": "Some Course Name",
        "Programmes": [
            {"ProgrammeCode": EDUCATION_CODE, "Year": "1", "Type": "Standard"},
            {"ProgrammeCode": "MScUnknownToUs", "Year": "2", "Type": "Compulsory"},
        ],
        "EC": 6,
        "Periods": "4",
        "Attributes": [],
    },
    {
        "CatalogNumber": COURSE_CODE2,
        "Name": "Some Course Other Name",
        "Programmes": [
            {"ProgrammeCode": EDUCATION_CODE, "Year": "2", "Type": "Elective"},
        ],
        "EC": 6,
        "Periods": "4,5,6",
        "Attributes": [],
    },
]

COURSE_RESPONSE1 = [
    {
        "NameDutch": "Een vak naam",
        "NameEnglish": "Some Course Name",
        "EC": 1.0,
        "CatalogNumber": COURSE_CODE1,
        "AcademicYear": datanose_service.get_current_academic_year(),
        "SISCourseID": 171521,
        "CatalogCourseURL": "",
        "Owner": EDUCATION_CODE,
    }
]

COURSE_RESPONSE2 = [
    {
        "NameDutch": "Een vak andere naam",
        "NameEnglish": "Some Course Other Name",
        "EC": 1,
        "CatalogNumber": COURSE_CODE2,
        "AcademicYear": datanose_service.get_current_academic_year(),
        "SISCourseID": 171521,
        "CatalogCourseURL": "",
        "Owner": EDUCATION_CODE,
    }
]


def test_sync_datanose_courses(
    db_session, education_factory, course_factory, requests_mocker
):
    education = education_factory(datanose_code=EDUCATION_CODE, is_via_programme=True)

    deprecated_course = course_factory()
    ec = EducationCourse(
        education_id=education.id, course_id=deprecated_course.id, year="1", periods=[1]
    )
    db_session.add(ec)
    db_session.commit()

    requests_mocker.get(
        f"https://api.datanose.nl/2020/Faculty/FNWI/Programmes/"
        f"{EDUCATION_CODE}/Courses",
        json=PROGRAMME_COURSES_RESPONSE,
    )
    requests_mocker.get(
        f"https://api.datanose.nl/Courses/{COURSE_CODE1}",
        json=COURSE_RESPONSE1,
    )
    requests_mocker.get(
        f"https://api.datanose.nl/Courses/{COURSE_CODE2}",
        json=COURSE_RESPONSE2,
    )
    # First time to pull in new courses
    sync_datanose_courses()

    # Second time to update existing courses
    sync_datanose_courses()

    course1 = db_session.execute(
        select(Course).filter(Course.datanose_code == COURSE_CODE1)
    ).scalar()
    ed1 = db_session.execute(
        select(EducationCourse).filter(EducationCourse.course_id == course1.id)
    ).scalar()
    assert ed1.year == 1
    assert ed1.periods == [4]

    course2 = db_session.execute(
        select(Course).filter(Course.datanose_code == COURSE_CODE2)
    ).scalar()
    ed2 = db_session.execute(
        select(EducationCourse).filter(EducationCourse.course_id == course2.id)
    ).scalar()
    assert ed2.year == 2
    assert ed2.periods == [4, 5, 6]

    # Validate deprecated course.
    db_session.refresh(ec)
    assert ec.periods is None, "Courses not in datanose should not have periods"
    assert ec.year is None, "Courses not in datanose should not have year"
