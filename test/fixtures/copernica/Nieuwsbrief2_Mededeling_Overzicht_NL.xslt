<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <xsl:for-each select="items/item">
            <font size="2">
                <a href="#announcement{id}" style="text-decoration:none; color:#FFFFFF;"><xsl:value-of select="nl/title"/></a><br/><br />
            </font>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
