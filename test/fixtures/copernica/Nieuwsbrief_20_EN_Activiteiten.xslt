<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xsl:variable name="months" select="'  JANFEBMAAAPRMEIJUNJULAUGSEPOKTNOVDEC'" />

    <xsl:template match="/">
        <xsl:for-each select="items/item">
            <!-- START activity container -->
            <tr>
                <td>
                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="container_600px">
                        <tr>
                            <td style="padding: 10px 10px 0 10px;">
                                <!-- START activity side padding -->
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="580" class="container_580px">

                                    <!-- START activity header -->
                                    <xsl:variable name="activityDate" as="xs:date" select="sdate"/>
                                    <tr>
                                        <td>
                                            <table class="activity_table" role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #304BA3; border-top-left-radius: 5px; border-top-right-radius: 5px;" width="580">
                                                <tr>
                                                    <td style="padding: 10px;">
                                                        <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="color: #FFFFFF; font-size: 14px; width: 35px; text-align: center; background-color: #7C7EB3;"><xsl:value-of select="substring($months, substring($activityDate, 4, 2) * 3, 3)"/></td>
                                                                <td style="padding: 0 0 0 10px;" rowspan="2">
                                                                    <a href="https://svia.nl/activities/{id}/" target="_blank" style="color: #FFFFFF; text-decoration: none; font-size: 28px; font-family: 'Source Sans Pro', sans-serif;">
                                                                        <font color="#FFFFFF"><xsl:value-of select="en/title"/></font>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color: #000000; font-size: 14px; width: 35px; text-align: center; border: 1px solid #7C7EB3; background-color: #FFFFFF; font-weight: bold;"><b><xsl:value-of select="substring($activityDate, 1, 2)"/></b></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- END activity title -->

                                    <!-- START activity content -->
                                    <tr>
                                        <td>
                                            <table class="activity_table" role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; border-left: 1px solid #E5E5E5; border-bottom: 1px solid #E5E5E5; border-right: 1px solid #E5E5E5;" width="580">
                                                <tr>
                                                    <td style="padding: 10px; color:#333333; font-family: 'Source Sans Pro', sans-serif; font-size: 16px;">
                                                        <xsl:value-of select="en/desc" disable-output-escaping="yes" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- END activity content -->

                                </table>
                                <!-- END activity side padding -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- END activity container -->
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
