<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <xsl:for-each select="items/item">
            <!-- START story container -->
            <tr>
                <td>
                    <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="container_600px">
                        <tr>
                            <td style="padding: 0 10px 0 10px;">
                                <!-- START story side padding -->
                                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="580" class="container_580px">

                                    <!-- START story title -->
                                    <tr>
                                        <td style="padding: 5px 0 0 0;">
                                            <a href="{link}" target="_blank" style="color: #304BA3; text-decoration: none; font-size: 28px; font-family: 'Source Sans Pro', sans-serif;">
                                                <font color="#304BA3" style="font-size: 28px; font-family: 'Source Sans Pro', sans-serif;"><xsl:value-of select="en/title"/></font>
                                            </a>
                                        </td>
                                    </tr>
                                    <!-- END story title -->

                                    <!-- START author profile -->
                                    <tr>
                                        <td style="color: #000000; text-decoration: none; font-weight:bold; font-size: 16px; font-family: 'Source Sans Pro', sans-serif; padding: 5px 0 5px 0; display: inline-block;">
                                            <table rol="presentation" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div style="width: 40px; height: 40px; border-radius: 20px; background-image: url({author/pf_url}); background-repear: no-repeat; background-position: center; background-size: 100%">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <b style="padding: 0 5px;"><xsl:value-of select="author/name"/></b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- END author profile -->

                                    <!-- START story content -->
                                    <tr>
                                        <td style="color:#333333; font-family: 'Source Sans Pro', sans-serif; font-size: 16px;">
                                            <xsl:value-of select="en/desc" disable-output-escaping="yes" />
                                        </td>
                                    </tr>
                                    <!-- END story content -->

                                </table>
                                <!-- END story side padding -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- END story container -->
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
