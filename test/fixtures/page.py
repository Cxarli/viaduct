import pytest

from app.models.page import Page, PageRevision


@pytest.fixture
def page_revision(db_session, admin_user):
    page = Page(path="path", type="page")

    revision = PageRevision(
        page=page,
        nl_title="nl_title",
        en_title="en_title",
        comment="comment",
        user=admin_user,
        nl_content="nl_content",
        en_content="en_content",
    )
    db_session.add(page)
    db_session.add(revision)
    db_session.commit()
    return page, revision
