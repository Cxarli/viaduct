import re
from typing import List

from flask import Flask
from werkzeug import routing

EXCLUDED = {
    "oauth.authorize",
    "oauth.issue_token",
    "oauth.revoke_token",
    "oauth.introspect_token",
    "oauth.errors",
    "copernica.verification",
}


def test_trailing_slashes(app):
    """
    This test defines our rules regarding trailing_slashes and route conventions.

    In general we want all routes to end a slash. Our application uses strict_slashes,
    which means that any request without a slash to a route which has no slash, will
    result in a 308 Permanent Redirect.

    Examples of allowed routes:
    /
    /resource/
    /resource/create/
    /resource/<var>/edit/
    /resource/<var>/raw.txt
    """

    filtered_rules: List[routing.Rule] = [
        rule for rule in app.url_map.iter_rules() if rule.endpoint not in EXCLUDED
    ]

    for rule in filtered_rules:
        parsed_rule = list(routing.parse_rule(rule.rule))
        assert parsed_rule, f"{rule} should at least contain something"
        # parsed_rule is a list of (converter_str, converter_args, path) tuples.
        assert parsed_rule[0][2].startswith("/"), f"{rule} should start with a slash"

        # We use create, edit, remove for CRUD operations.
        for parsed_rule_part in parsed_rule:
            assert not re.match(
                r".*/new/?$", parsed_rule_part[2]
            ), f"'new' is not allowed in {rule}, use 'create'"
            assert not re.match(
                r".*/add/?$", parsed_rule_part[2]
            ), f"'add' is not allowed in {rule}, use 'create'"
            assert not re.match(
                r".*/delete/?$", parsed_rule_part[2]
            ), f"'delete' is not allowed in {rule}, use 'remove'"

        # Do not check for ending slash when the final url part is a filename.
        if "." in parsed_rule[-1][2]:
            continue

        # If the last part of the url is a PathConverter, we allow without slashes.
        converter = parsed_rule[-1][0]
        if converter and issubclass(
            app.url_map.converters[converter], routing.PathConverter
        ):
            continue

        assert parsed_rule[-1][2].endswith(
            "/"
        ), f"{rule} routes should end with a slash"


def test_strict_slashes_behaviour():
    app = Flask("slash_test")

    @app.route("/noslash", methods=["GET"])
    def noslash_strict():
        return "noslash", 200

    @app.route("/slash/", methods=["GET", "POST"])
    def slash_strict():
        return "slash", 200

    @app.route("/noslash2", methods=["GET"], strict_slashes=False)
    def noslash():
        return "noslash", 200

    @app.route("/slash2/", methods=["GET"], strict_slashes=False)
    def slash():
        return "slash", 200

    with app.test_client() as c:
        rv = c.get("/noslash")
        assert rv.status_code == 200, "/noslash"
        rv = c.get("/noslash/")
        assert rv.status_code == 404, "/noslash/"

        rv = c.get("/slash")
        assert rv.status_code == 308, "/slash"
        rv = c.get("/slash/")
        assert rv.status_code == 200, "/slash/"

        rv = c.get("/noslash2")
        assert rv.status_code == 200, "/noslash2"
        rv = c.get("/noslash/")
        assert rv.status_code == 404, "/noslash/"

        rv = c.get("/slash2")
        assert rv.status_code == 200, "/slash2"
        rv = c.get("/slash2/")
        assert rv.status_code == 200, "/slash2/"

        rv = c.post("/slash", data={"test": "test"})
        assert rv.status_code == 308

        rv = c.post("/slash", data={"test": "test"}, follow_redirects=True)
        assert rv.status_code == 200
