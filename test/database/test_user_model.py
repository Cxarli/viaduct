import pytest
from sqlalchemy.exc import IntegrityError

from app.models.user import User
from app.repository.user_repository import get_user_ids_like


def test_user_model_unique_email(db_session, user_factory):
    """Test persisting two users with same email."""
    user_factory(email="duplicate@example.com")

    db_session.begin_nested()
    with pytest.raises(IntegrityError):
        user_factory(email="duplicate@example.com")
    db_session.rollback()

    assert db_session.query(User).count() == 1


def test_get_user_ids_like(user_factory):
    user_factory(first_name="Hubert", last_name="Wolfeschlegelsteinhausenbergerdorff")

    res = get_user_ids_like(
        ["Hubert Wolfeschlegelsteinhausenbergerdorff", "Does n. Exist"]
    )

    assert len(res) == 2

    assert any(
        type(uid) is int and name == "Hubert Wolfeschlegelsteinhausenbergerdorff"
        for (uid, name) in res
    )
    assert any(uid is None and name == "Does n. Exist" for (uid, name) in res)


def test_get_user_ids_like_empty():
    res = get_user_ids_like([])

    assert len(res) == 0
