from unittest import mock
from unittest.mock import patch

from requests import Session

from app import app
from app.repository import gitlab_repository as gitlab_mock_spec
from app.service import gitlab_service

gitlab_repository_mock = mock.MagicMock(spec=gitlab_mock_spec)


@patch.object(gitlab_service, "gitlab_repository", gitlab_repository_mock)
class TestGitlabService:
    def setup_method(self):
        app.config["GITLAB_TOKEN"] = "MOCKED"
        gitlab_repository_mock.reset_mock()

    def test_create_gitlab_session(self):
        token = "abc"

        session = gitlab_service.create_gitlab_session(token)

        assert isinstance(session, Session)
        assert "PRIVATE-TOKEN" in session.headers
        assert token in session.headers.values()
