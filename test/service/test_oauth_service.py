import unittest
from unittest.mock import MagicMock, patch

from app.models.oauth.client import OAuthClient
from app.models.oauth.token import OAuthToken
from app.repository import oauth_repository as mock_spec  # rename for safety
from app.service import oauth_service

oauth_repo_mock = MagicMock(spec=dir(mock_spec))

client_mock = MagicMock(spec=OAuthClient.__table__.columns.keys())
token_mock = MagicMock(spec=OAuthToken.__table__.columns.keys())

access_token = "access_token"
refresh_token = "refresh_token"
client_id = "client_id"
client_secret = "client_secret"
grant_code = "grant_code"
code = {"code": "grant_code"}
scopes = "SCOPE1 SCOPE2 SCOPE3"
scopes_list = ["SCOPE1", "SCOPE2", "SCOPE3"]
redirect_uri = "http://svia.nl"
token_type = "Bearer"
expires = 3600
user_id = 1
grant_id = 2


@patch.object(oauth_service, "oauth_repository", oauth_repo_mock)
class TestOAuthService(unittest.TestCase):
    def setUp(self):
        oauth_repo_mock.reset_mock()
        client_mock.reset_mock()
        client_mock.client_id = client_id
        client_mock.client_secret = client_secret
        client_mock.user_id = user_id

    def test_query_token_with_type_hint(self):
        oauth_service._query_token(access_token, access_token, client_mock)
        oauth_repo_mock.get_token_by_access_token.assert_called_once_with(
            access_token=access_token, client_id=client_id
        )

        oauth_service._query_token(refresh_token, refresh_token, client_mock)
        oauth_repo_mock.get_token_by_refresh_token.assert_called_once_with(
            refresh_token=refresh_token, client_id=client_id
        )

    def test_query_access_token_without_type_hint(self):
        oauth_repo_mock.get_token_by_access_token.return_value = token_mock
        token = oauth_service._query_token(access_token, None, client_mock)

        oauth_repo_mock.get_token_by_access_token.assert_called_once_with(
            access_token=access_token, client_id=client_id
        )
        oauth_repo_mock.get_token_by_refresh_token.assert_not_called()
        self.assertEqual(token, token_mock)

    def test_query_refresh_token_without_type_hint(self):
        oauth_repo_mock.get_token_by_access_token.return_value = None
        oauth_repo_mock.get_token_by_refresh_token.return_value = token_mock

        token = oauth_service._query_token(refresh_token, None, client_mock)
        oauth_repo_mock.get_token_by_access_token.assert_called_once_with(
            access_token=refresh_token, client_id=client_id
        )

        oauth_repo_mock.get_token_by_refresh_token.assert_called_once_with(
            refresh_token=refresh_token, client_id=client_id
        )
        self.assertEqual(token_mock, token)

    def test_create_token_without_user(self):
        request_mock = MagicMock()
        request_mock.client = client_mock
        request_mock.user = None
        token = {"somekey": "somevalue"}

        oauth_service.create_token(token, request_mock)
        oauth_repo_mock.create_token.assert_called_once_with(
            client_id=client_id, user_id=user_id, somekey="somevalue"
        )

    def test_get_owned_clients_by_user_id(self):
        oauth_service.get_owned_clients_by_user_id(user_id=3)
        oauth_repo_mock.get_owned_clients_by_user_id.assert_called_once_with(user_id=3)
