from app.enums import ProgrammeType
from app.service import datanose_service


def test_single_bachelor(requests_mocker):
    code = "BSc KI"
    name = "Bachelor Kunstmatige Intelligentie"

    requests_mocker.get(
        "https://api.datanose.nl/Programmes/12345",
        json=[{"Code": code, "Name": name, "Type": "Bachelor"}],
    )

    result = datanose_service.get_study_programmes("12345")

    p = result[0]
    assert p.programme_type == ProgrammeType.BACHELOR
    assert p.name == name
    assert p.code == code


def test_single_master(requests_mocker):
    code = "MSc AI"
    name = "Master Artificial Intelligence"

    requests_mocker.get(
        "https://api.datanose.nl/Programmes/12345",
        json=[{"Code": code, "Name": name, "Type": "Master"}],
    )
    result = datanose_service.get_study_programmes("12345")

    assert len(result) == 1

    p = result[0]
    assert p.programme_type == ProgrammeType.MASTER
    assert p.name == name
    assert p.code == code


def test_multiple_programmes(requests_mocker):
    codes = [
        "SciProg",
        "BSc KI",
        "Maj BDA",
        "Min Prog",
        "PreMsc IS",
        "MSc SE",
        "Exchange",
    ]
    names = [
        "Scientific Programming",
        "Bachelor Kunstmatige Intelligentie",
        "Major BIg Biomedical Data Analysis",
        "Minor Programmeren",
        "Pre-master Information Studies",
        "Master Software Engineering",
        "Exchange Programme Faculty of Science",
    ]
    resp_types = [
        "None",
        "Bachelor",
        "Major",
        "Minor",
        "Premaster",
        "Master",
        "Exchange",
    ]
    result_types = [
        ProgrammeType.OTHER,
        ProgrammeType.BACHELOR,
        ProgrammeType.MAJOR,
        ProgrammeType.MINOR,
        ProgrammeType.PRE_MASTER,
        ProgrammeType.MASTER,
        ProgrammeType.OTHER,
    ]

    requests_mocker.get(
        "https://api.datanose.nl/Programmes/12345",
        json=[
            {"Code": code, "Name": name, "Type": r_type}
            for code, name, r_type in zip(codes, names, resp_types)
        ],
    )
    result = datanose_service.get_study_programmes("12345")

    assert len(result) == len(codes)

    for p, code, name, p_type in zip(result, codes, names, result_types):
        assert p.programme_type == p_type
        assert p.name == name
        assert p.code == code


def test_invalid_request(requests_mocker):
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/invalid_student_id",
        status_code=400,
        json={"Message": "The request is invalid."},
    )

    result = datanose_service.get_study_programmes("invalid_student_id")
    assert result == []


def test_empty_result(requests_mocker):
    requests_mocker.get(
        "https://api.datanose.nl/Programmes/12345", status_code=400, json=[]
    )

    result = datanose_service.get_study_programmes("12345")

    assert result == []
