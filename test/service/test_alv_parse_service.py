from typing import Dict

from app.service.alv_parse_service import AlvEvent, AlvParser, AlvEventData


def test_fixture_parse(alv_minutes_data: Dict[str, str]):
    parser = AlvParser(alv_minutes_data)
    parser.parse()

    assert parser.alv_data == {
        "title": "ALV titel",
        "present": ["Wilco Kruijer", "Mick Vermeulen"],
        "proxies": [("Wilco Kruijer", "Mick Vermeulen")],
        "chairman": "Voorzitter Y",
        "secretary": "Notulist Z",
        "start_time": "HH:MM",
        "stop_time": "HH:MM",
        "location": "XY.WYV",
    }

    assert len(parser.alv_events) == 11

    assert parser.alv_events[0] == AlvEventData(
        event_type=AlvEvent.SECTION, data={"title": "Machtigingen"}
    )
    assert parser.alv_events[2] == AlvEventData(
        event_type=AlvEvent.TIMESTAMP,
        data={"time": "13:37", "event": "X opent de vergadering"},
    )
    assert parser.alv_events[3] == AlvEventData(
        event_type=AlvEvent.DECISION, data={"decision": "Dit is een besluit"}
    )
    assert parser.alv_events[4] == AlvEventData(
        event_type=AlvEvent.TASK, data={"who": "Commissie", "what": "Doe dit actiepunt"}
    )
    assert parser.alv_events[6] == AlvEventData(
        event_type=AlvEvent.VOTE,
        data={
            "title": "De verwoording van de stemming aldus de voorzitter",
            "vote_data": {"Voor": 1, "Tegen": 3, "Blanco": 3, "Onthouden": 7},
        },
    )


def test_present():
    d = {
        "main.tex": r"""
\newcommand{\wilco}{\name{Wilco Kruijer}}
\newcommand{\mick}{\name{Mick Vermeulen}}

\present{
    \wilco, Klaas Vaak
    \mick (vanaf 13:37)
    Bram (van 18:48 tot 19:31)
Meneer Aanwezig % Sjaak Afhaak
    % toch niet
    Sjors (tot 20:00)
Piet van Schoorsteen

}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_data["present"] == [
        "Wilco Kruijer",
        "Klaas Vaak",
        "Mick Vermeulen",
        "Bram",
        "Meneer Aanwezig",
        "Sjors",
        "Piet van Schoorsteen",
    ]


def test_metadata():
    d = {
        "main.tex": r"""
\newcommand{\mick}{\name{Mick Vermeulen}}

\title{Oefen-ALV}
\chairman{Voorzitter Y\xspace}
\secretary{\mick\xspace}
\starttime{HH:MM\xspace}
\stoptime{13:3X\xspace}
\location{XY.WYV\xspace}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_data["title"] == "Oefen-ALV"
    assert parser.alv_data["chairman"] == "Voorzitter Y"
    assert parser.alv_data["secretary"] == "Mick Vermeulen"
    assert parser.alv_data["start_time"] == "HH:MM"
    assert parser.alv_data["stop_time"] == "13:3X"
    assert parser.alv_data["location"] == "XY.WYV"


def test_proxies():
    d = {
        "main.tex": r"""
\newcommand{\mick}{\name{Mick Vermeulen}}
\newcommand{\wilco}{\name{Wilco Kruijer}}
\newcommand{\eva}{\name{Eva Oostenbrink}}


\wilco machtigt \mick; \\
Piet van Deur machtigt Iemand Anders \\ Mark Rutte machtigt Geert Wilders
 % Jan machtigt Cornelis Comment


    Sjaak machtigt       Pieter
\proxy{Rachel de Haan}{\eva}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_data["proxies"] == [
        ("Wilco Kruijer", "Mick Vermeulen"),
        ("Piet van Deur", "Iemand Anders"),
        ("Mark Rutte", "Geert Wilders"),
        ("Sjaak", "Pieter"),
        ("Rachel de Haan", "Eva Oostenbrink"),
    ]


def test_decision():
    d = {
        "main.tex": r"""
\decision{De begroting is aangenomen.}
 % \decision{Maico Timmermans is bij deze erelid van \textbf{via}!}
               \decision{Het bestuur is afgezet.} \decision{22}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.DECISION,
            data={"decision": "De begroting is aangenomen."},
        ),
        AlvEventData(
            event_type=AlvEvent.DECISION, data={"decision": "Het bestuur is afgezet."}
        ),
        AlvEventData(event_type=AlvEvent.DECISION, data={"decision": "22"}),
    ]


def test_sections():
    d = {
        "main.tex": r"""
\section{Mededelingen}
\subsection{Iets van bestuur}
\subsection*{Iets vanuit de PresCo}
\section{
agenda}
\subsubsection{%
wederom iets}
\section*{Test}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(event_type=AlvEvent.SECTION, data={"title": "Mededelingen"}),
        AlvEventData(
            event_type=AlvEvent.SUBSECTION, data={"title": "Iets van bestuur"}
        ),
        AlvEventData(
            event_type=AlvEvent.SUBSECTION, data={"title": "Iets vanuit de PresCo"}
        ),
        AlvEventData(event_type=AlvEvent.SECTION, data={"title": "agenda"}),
        AlvEventData(event_type=AlvEvent.SUBSECTION, data={"title": "wederom iets"}),
        AlvEventData(event_type=AlvEvent.SECTION, data={"title": "Test"}),
    ]


def test_timestamp():
    d = {
        "main.tex": r"""
\timestamp{13:37}{Vergadering geopend.}
  \timestamp{13???}{Sinterklaas komt binnen.} \timestamp{XX:YY}{Af.}
\timestamp{}{}
% \timestamp{11am}{comment}
\timestamp{tijd:
3}{ lang ...
verhaal
% comment
}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.TIMESTAMP,
            data={"time": "13:37", "event": "Vergadering geopend."},
        ),
        AlvEventData(
            event_type=AlvEvent.TIMESTAMP,
            data={"time": "13???", "event": "Sinterklaas komt binnen."},
        ),
        AlvEventData(
            event_type=AlvEvent.TIMESTAMP, data={"time": "XX:YY", "event": "Af."}
        ),
        AlvEventData(
            event_type=AlvEvent.TIMESTAMP,
            data={"time": "tijd:\n3", "event": "lang ...\nverhaal"},
        ),
    ]


def test_task():
    d = {
        "main.tex": r"""
\task{Bestuur}{Dingen doen}
\task{Caviacommissie}{Archief op orde krijgen}
\task{ }{jajajajaj}
\task{Bestuur}
\task{Commissies}{}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.TASK, data={"who": "Bestuur", "what": "Dingen doen"}
        ),
        AlvEventData(
            event_type=AlvEvent.TASK,
            data={"who": "Caviacommissie", "what": "Archief op orde krijgen"},
        ),
        AlvEventData(event_type=AlvEvent.TASK, data={"who": "Commissies", "what": ""}),
    ]


def test_vote():
    d = {
        "main.tex": r"""
\begin{vote}{Een bepaalde kwestie}
Voor & 1 \\
Tegen &3 \\
Blanco& 3 \\
Onthouden&27 \\
\end{vote}

\begin{vote}{Alles op een regel met slashes}
Voor & 1 \\Tegen &3 \\Blanco& 3 \\Onthouden&27 \\
\end{vote}

\begin{vote}{Is voor deze latex-vote moeite gedaan?}
Voor & 100 Tegen &3 Blanco& 3 Onthouden&4
\end{vote}

\begin{vote}{Stem op multiline...
% comment
dingen}
Voor & 1 stemmen \\ Rechter-hoek & 5 \\
Tegen & 3mensen \\
Blanco & 6jes-cultuur \\
Onthouden&27 \\
\end{vote}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Een bepaalde kwestie",
                "vote_data": {"Voor": 1, "Tegen": 3, "Blanco": 3, "Onthouden": 27},
            },
        ),
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Alles op een regel met slashes",
                "vote_data": {"Voor": 1, "Tegen": 3, "Blanco": 3, "Onthouden": 27},
            },
        ),
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Is voor deze latex-vote moeite gedaan?",
                "vote_data": {"Voor": 100, "Tegen": 3, "Blanco": 3, "Onthouden": 4},
            },
        ),
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Stem op multiline...\n\ndingen",
                "vote_data": {
                    "Voor": 1,
                    "Rechter-hoek": 5,
                    "Tegen": 3,
                    "Blanco": 6,
                    "Onthouden": 27,
                },
            },
        ),
    ]


def test_resolve_input():
    d = {
        "names.tex": r"""
\newcommand{\wilco}{\name{Wilco Kruijer}}
\newcommand{\mick}{\name{Mick Vermeulen}}
% Heel belangrijk, \via dikgedrukt
\newcommand{\via}{\textbf{via}\xspace}
""",
        "present.tex": r"""
\present{
    \wilco,
    \mick (vanaf 17:44)
}
""",
        "negeer_dit.tex": r"""
\decision{We gaan dingen doen.}
""",
        "main.tex": r"""
\input{names.tex}
\input{present.tex}

\input{bestaat_niet.tex}

% \input{negeer_dit.tex}
Filler tekst.
""",
    }
    parser = AlvParser(d)
    parser.parse()

    assert len(parser.alv_events) == 0
    assert parser.alv_data["present"] == ["Wilco Kruijer", "Mick Vermeulen"]


def test_vote_empty():
    d = {
        "main.tex": r"""
\begin{vote}{Een bepaalde kwestie}
% asd

\end{vote}

\begin{vote}{Een bepaalde kwestie}\end{vote}

\begin{vote}{Een vote op de oude, niet ondersteunde, manier.}\end{vote}
\begin{table}[h!]
\centering
\begin{tabular}{ l c}
Voor & 30  \\
Tegen & 2 \\
Blanco & 1 \\
Onthouden & 3 \\
\end{tabular}
\end{table}
"""
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={"title": "Een bepaalde kwestie", "vote_data": {}},
        ),
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={"title": "Een bepaalde kwestie", "vote_data": {}},
        ),
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Een vote op de oude, niet ondersteunde, manier.",
                "vote_data": {},
            },
        ),
    ]


def test_newcommand_complex():
    d = {
        "main.tex": r"""
\newcommand{\via}{\textbf{via}\xspace}
\newcommand{\wilco}{\name{Wilco Kruijer}}
\newcommand{\klaas}{Klaas Achternaam}
\newcommand{\w}{\wilco}

\begin{vote}{Stemmen:}
\via & 5
\wilco & 8
\klaas & 3
\end{vote}

Iemand machtigt \w

"""
    }
    parser = AlvParser(d)
    parser.parse()
    assert parser.alv_data["proxies"] == [("Iemand", "Wilco Kruijer")]

    assert parser.alv_events == [
        AlvEventData(
            event_type=AlvEvent.VOTE,
            data={
                "title": "Stemmen:",
                "vote_data": {"via": 5, "Wilco Kruijer": 8, "Klaas Achternaam": 3},
            },
        )
    ]


def test_resolve_input_with_comment():
    d = {
        "1.tex": r"""
\decision{1 doen.}
% Hier staat een comment die dingen uitlegd.
""",
        "2.tex": r"""
\decision{2 doen.}
""",
        "main.tex": r"""
\input{1.tex}
\input{2.tex}
""",
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(event_type=AlvEvent.DECISION, data={"decision": "1 doen."}),
        AlvEventData(event_type=AlvEvent.DECISION, data={"decision": "2 doen."}),
    ]


def test_parse_errors():
    d = {
        "main.tex": r"""
\decision
\task{Blabla}
\input{does_not_exist.tex}
\proxy
\proxy{}{}
\proxy{X}{}
""",
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.parse_errors == [
        "File 'does_not_exist.tex' not found for \\input command.",
        "Metadata not found: '\\title'.",
        "Metadata not found: '\\chairman'.",
        "Metadata not found: '\\secretary'.",
        "Metadata not found: '\\location'.",
        "Metadata not found: '\\starttime'.",
        "Metadata not found: '\\stoptime'.",
        "\\present command not found.",
        "Invalid number of arguments for '\\proxy'.",
        "Empty first argument in '\\proxy'.",
        "Empty second argument in '\\proxy'.",
        "Invalid number of arguments for '\\decision'.",
        "Invalid number of arguments for '\\task'.",
    ]


def test_resolve_input_with_comment_no_parse_error():
    d = {
        "1.tex": r"""
\decision{1 doen.}
% Hier staat een comment die dingen uitlegd.
""",
        "main.tex": r"""
\input{1.tex}
% \input{3.tex}
""",
    }
    parser = AlvParser(d)
    parser.parse()

    assert parser.alv_events == [
        AlvEventData(event_type=AlvEvent.DECISION, data={"decision": "1 doen."})
    ]

    assert "File '3.tex' not found for \\input command." not in parser.parse_errors
