import pytest


def test_markdown_filter(app):
    md = app.jinja_env.filters["markdown"]

    assert md("**via**") == "<p><strong>via</strong></p>"


def test_markdown_remove_script(app):
    """Checks that Bleach is actually being used."""
    md = app.jinja_env.filters["markdown"]

    assert (
        md("<script> something(); </script>")
        == "<p>&lt;script&gt; something(); &lt;/script&gt;</p>"
    )


def test_markdown_remove_img_onload(app):
    md = app.jinja_env.filters["markdown"]

    assert md('<img src="a.png" onload="alert(1);"/>') == '<p><img src="a.png"></p>'


def test_truncated(app):
    md = app.jinja_env.filters["markdown_summary"]

    t = md(
        "**this is some long text *that [should](https://google.nl)* be _in_ BOLD.**",
        15,
    )

    assert t == "<p><strong>this is some lo</strong></p>"


def test_truncated_no_max_length(app):
    md = app.jinja_env.filters["markdown_summary"]

    t = md("**Nothing should happen here.**", 999)
    assert t == "<p><strong>Nothing should happen here.</strong></p>"


def test_truncated_plain_text_length(app):
    """HTML tags should not be removed in this test."""
    md = app.jinja_env.filters["markdown_summary"]

    actual = "Nothing should happen here."

    t = md(f"**{actual}**", len(actual))
    assert t == "<p><strong>Nothing should happen here.</strong></p>"


def test_truncated_remove_tags(app):
    md = app.jinja_env.filters["markdown_summary"]

    assert md("![img](some_logo.png)", 999) == "<p></p>"
    assert md("---\nasd", 999) == "<p>asd</p>"


def test_truncated_no_header(app):
    md = app.jinja_env.filters["markdown_summary"]

    assert (
        md("# Testing header\n### hthree", 999)
        == "<h6>Testing header</h6>\n<h6>hthree</h6>"
    )


def test_truncated_remove_lists(app):
    md = app.jinja_env.filters["markdown_summary"]

    assert md("* 1\n* 2", 999) == ""


def test_truncated_word_break(app):
    md = app.jinja_env.filters["markdown_summary"]

    t = md(
        "**this is some long text *that [should](https://google.nl)* be _in_ BOLD.**",
        max_length=15,
        truncate_method="word",
    )

    assert t == "<p><strong>this is some long</strong></p>"


def test_truncated_word_read_more(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "**this is some long text**",
        max_length=15,
        truncate_method="word",
        read_more="...",
    )

    assert t1 == "<p><strong>this is some long</strong>...</p>"


def test_truncated_word_read_more_hidden(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "**this is some long text**",
        max_length=30,
        truncate_method="word",
        read_more="...",
    )

    assert t1 == "<p><strong>this is some long text</strong></p>"


def test_truncated_hard_read_more(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "**this is some long text**",
        max_length=15,
        truncate_method="hard",
        read_more="...",
    )

    assert t1 == "<p><strong>this is some lo</strong>...</p>"


def test_truncated_hard_read_more_hidden(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "**this is some long text**",
        max_length=30,
        truncate_method="hard",
        read_more="...",
    )

    assert t1 == "<p><strong>this is some long text</strong></p>"


def test_truncated_paragraph_read_more(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "paragraph 1\n\nparagraph 2\n\nparagraph 3\n\nparagraph 4\n\nparagraph 5",
        max_length=15,
        truncate_method="paragraph",
        read_more="...",
    )

    assert t1 == "<p>paragraph 1</p>\n<p>paragraph 2</p>\n..."


def test_truncated_paragraph_read_more_hidden(app):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "paragraph 1\n\nparagraph 2\n\nparagraph 3\n\nparagraph 4\n\nparagraph 5",
        max_length=200,
        truncate_method="hard",
        read_more="...",
    )

    assert (
        t1 == "<p>paragraph 1</p>\n<p>paragraph 2</p>\n"
        "<p>paragraph 3</p>\n<p>paragraph 4</p>\n<p>paragraph 5</p>"
    )


@pytest.mark.parametrize("truncate_method", ["hard", "word", "paragraph"])
def test_truncated_read_more_hidden_whitespace(app, truncate_method):
    md = app.jinja_env.filters["markdown_summary"]

    t1 = md(
        "\t\n    \n\t\n",
        max_length=2,
        truncate_method=truncate_method,
        read_more="...",
    )

    assert t1 == ""


def test_truncated_word_read_more_no_tags(app):
    md = app.jinja_env.filters["markdown_summary"]
    t2 = md("no tags", max_length=2, truncate_method="word", read_more="...")

    assert t2 == "<p>no...</p>"


def test_truncated_word_read_more_hidden_multiple_paragraphs(app):
    md = app.jinja_env.filters["markdown_summary"]
    t3 = md(
        "paragraph 1\n\nparagraph 2",
        max_length=200,
        truncate_method="word",
        read_more="...",
    )

    assert t3 == "<p>paragraph 1</p>\n<p>paragraph 2</p>"


def test_truncated_word_read_more_multiple_paragraphs(app):
    md = app.jinja_env.filters["markdown_summary"]
    t3 = md(
        "paragraph 1\n\nparagraph 2",
        max_length=18,
        truncate_method="word",
        read_more="...",
    )

    assert t3 == "<p>paragraph 1</p>\n<p>paragraph...</p>"


def test_truncated_word_read_more_in_current_paragraph(app):
    md = app.jinja_env.filters["markdown_summary"]

    text = "1\n\nbladieblablablabla ha ha ha ha\n\n*22*"
    res = md(
        text,
        max_length=10,
        truncate_method="word",
        read_more="...",
    )

    assert res == "<p>1</p>\n<p>bladieblablablabla...</p>"


def test_truncated_word_read_more_position(app):
    md = app.jinja_env.filters["markdown_summary"]

    text = "bla *die* blabla *bla* bla ha ha ha ha"

    res = md(
        text,
        max_length=30,
        truncate_method="word",
        read_more="...",
    )

    assert res == "<p>bla <em>die</em> blabla <em>bla</em> bla ha ha ha...</p>"


def test_truncate_not_executed_when_empty_string(app):
    md = app.jinja_env.filters["markdown_summary"]

    assert (
        md(
            "",
            max_length=30,
            truncate_method="word",
            read_more="...",
        )
        == ""
    )


def test_truncated_paragraph_break(app):
    md = app.jinja_env.filters["markdown_summary"]

    text = "de eerste\n\nde tweede ha ha\n\n*DErDE*"

    res = md(text, max_length=15, truncate_method="paragraph", read_more="...")

    assert res == "<p>de eerste</p>\n<p>de tweede ha ha</p>\n..."


def test_truncated_read_more_html(app):
    md = app.jinja_env.filters["markdown_summary"]

    text = "Blabla\n\nblablablabla"

    res = md(
        text, max_length=5, truncate_method="paragraph", read_more="<div>...</div>"
    )

    assert res == "<p>Blabla</p>\n<div>...</div>"


def test_truncated_header_and_list(app):
    md = app.jinja_env.filters["markdown_summary"]

    text = "abc\n\n* list\n\n## titel"

    res = md(
        text,
        max_length=300,
        truncate_method="hard",
    )

    assert res == "<p>abc</p>\n<h6>titel</h6>"


def test_markdown_via_base_image_unchanged(app):
    md = app.jinja_env.filters["markdown"]
    assert md("![A logo](logo.png)") == '<p><img alt="A logo" src="logo.png" /></p>'


def test_markdown_via_base_youtube_embed(app):
    md = app.jinja_env.filters["markdown"]

    desired = (
        '<p>\n<iframe allow="autoplay; encrypted-media" alt="A Youtube video" '
        + 'frameborder="0" height="480" '
        + 'src="https://www.youtube-nocookie.com/embed/iwGFalTRHDA" width="640">'
        + "</iframe>\n</p>"
    )

    for m in [
        md("![A Youtube video](http://youtu.be/iwGFalTRHDA)"),
        md("![A Youtube video](http://www.youtube.com/watch?v=iwGFalTRHDA)"),
        md("![A Youtube video](youtube.com/watch?v=iwGFalTRHDA)"),
        md("![A Youtube video](https://youtu.be/iwGFalTRHDA)"),
        md("![A Youtube video](https://youtube.nl/watch?v=iwGFalTRHDA )"),
    ]:
        assert m == desired


def test_markdown_blockquotes(app):
    md = app.jinja_env.filters["markdown"]

    assert (
        md("> this is a quote\n> second line.")
        == "<blockquote>\n<p>this is a quote\nsecond line.</p>\n</blockquote>"
    )
