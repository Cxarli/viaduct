import pytest

from app.models.tutoring import Tutoring


@pytest.fixture
def tutoring(db_session, admin_user, course_factory):
    course = course_factory()
    t = Tutoring()
    t.course = course
    t.tutor = None
    t.tutee = admin_user
    t.num_hours = 3
    db_session.add(t)
    db_session.commit()
    return t


def test_get(anonymous_client, admin_user, tutoring):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get(f"/tutoring/{tutoring.id}")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/tutors/{admin_user.id}")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/tutors/{admin_user.id}/course")
    assert rv.status_code == 200
