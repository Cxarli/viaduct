import io

import pytest
from werkzeug.datastructures import FileStorage

from app.enums import FileCategory
from app.service import file_service
from conftest import CustomClient

_FILE_CONTENT = b"filecontentbyes"


@pytest.fixture(params=[FileCategory.UPLOADS, FileCategory.UPLOADS_MEMBER])
def uploaded_file(request, db_session):
    fs = FileStorage(io.BytesIO(_FILE_CONTENT), "somelonguniquefilename.pdf")
    _file = file_service.add_file(request.param, fs)
    return _file


def test_files_uploads(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/")
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name in data
    if uploaded_file.category == FileCategory.UPLOADS_MEMBER:
        assert "Members only" in data
    else:
        assert "Members only" not in data


def test_files_uploads_search(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/", query_string={"search": "someuniquefilename"})
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name in data
    if uploaded_file.category == FileCategory.UPLOADS_MEMBER:
        assert "Members only" in data
    else:
        assert "Members only" not in data


def test_files_uploads_search_not_found(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/", query_string={"search": "emanelifeuqinuemos"})
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name not in data


def test_files_content_anonymous(anonymous_client: CustomClient, uploaded_file):
    rv = anonymous_client.get(
        f"/files/content/{uploaded_file.id}/{uploaded_file.hash}/",
        follow_redirects=False,
    )
    if uploaded_file.category == FileCategory.UPLOADS:
        assert rv.status_code == 200
        assert _FILE_CONTENT in rv.data
        assert rv.content_type == "application/pdf"
    elif uploaded_file.category == FileCategory.UPLOADS_MEMBER:
        assert rv.status_code == 302
        assert "/sign-in/" in rv.location
    else:
        raise AssertionError()


def test_files_content(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)
    rv = admin_client.get(
        f"/files/content/{uploaded_file.id}/{uploaded_file.hash}/",
    )
    assert rv.status_code == 200
    assert _FILE_CONTENT in rv.data
    assert rv.content_type == "application/pdf"
