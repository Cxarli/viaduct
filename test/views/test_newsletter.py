import os

import pytest
from lxml import etree

from app.models.newsletter import Newsletter, NewsletterActivity, NewsletterNewsItem


@pytest.fixture
def newsletter(db_session, activity_factory, news_factory):
    activity = activity_factory()
    activity.nl_description = activity.en_description = """
**via** {lang}

[svia.nl](https://svia.nl)

![img](https://svia.nl/favicon.ico)
    """.strip()
    activity.nl_description = activity.nl_description.format(lang="NL")
    activity.en_description = activity.en_description.format(lang="EN")
    activity2 = activity_factory()
    activity2.nl_description = activity2.en_description = "OVERWRITTEN"

    news_item = news_factory()
    news_item.nl_content = news_item.en_content = """
**via** {lang}

[svia.nl](https://svia.nl)

![img](https://svia.nl/favicon.ico)
    """.strip()
    news_item.nl_content = news_item.nl_content.format(lang="NL")
    news_item.en_content = news_item.en_content.format(lang="EN")

    news_item2 = news_factory()
    news_item2.nl_content = news_item2.en_content = "OVERWRITTEN"
    newsletter = Newsletter()
    newsletter.activities.append(NewsletterActivity(activity=activity))
    newsletter.news_items.append(NewsletterNewsItem(news_item=news_item))
    newsletter.activities.append(
        NewsletterActivity(
            activity=activity2,
            nl_description="OVERWRITE ACTIVITY NL",
            en_description="OVERWRITE ACTIVITY EN",
        )
    )
    newsletter.news_items.append(
        NewsletterNewsItem(
            news_item=news_item2,
            nl_description="OVERWRITE NEWS NL",
            en_description="OVERWRITE NEWS EN",
        )
    )
    db_session.add(newsletter)
    db_session.commit()
    return newsletter


def test_create_edit(anonymous_client, admin_user, newsletter):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/newsletter/create")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/newsletter/{newsletter.id}/edit")
    assert rv.status_code == 200


@pytest.mark.usefixtures("newsletter")
@pytest.mark.parametrize("auth_token", ["", "SOME_TOKEN"])
def test_missing_wrong_auth_token(auth_token, app, anonymous_client, member_user):
    app.config["COPERNICA_NEWSLETTER_TOKEN"] = auth_token + "_CORRECT"
    anonymous_client.login(member_user)
    rv = anonymous_client.get(
        "/newsletter/latest/committees/",
        query_string={"auth_token": auth_token},
        follow_redirects=False,
    )
    assert rv.status_code == 403


@pytest.mark.parametrize("lang", ["NL", "EN"])
@pytest.mark.parametrize(
    "item",
    [
        ("activities", "Nieuwsbrief2_Activiteit_Overzicht_{lang}"),
        ("activities", "Nieuwsbrief_20_{lang}_Activiteiten"),
        ("news", "Nieuwsbrief2_Mededeling_Overzicht_{lang}"),
        ("news", "Nieuwsbrief_20_{lang}_Nieuws"),
    ],
)
def test_xslt(lang, item, app, anonymous_client, newsletter):
    url, filename = item
    app.config["COPERNICA_NEWSLETTER_TOKEN"] = "SOMETOKEN"
    rv = anonymous_client.get(
        f"/newsletter/latest/{url}/",
        query_string={"auth_token": app.config["COPERNICA_NEWSLETTER_TOKEN"]},
    )
    assert rv.status_code == 200
    assert rv.mimetype == "text/xml"

    dom = etree.fromstring(rv.data)

    path = os.path.join(
        os.path.abspath(""),
        "test/fixtures/copernica/{filename}.xslt".format(filename=filename).format(
            lang=lang
        ),
    )
    parser = etree.XMLParser(recover=True)
    xslt = etree.parse(path, parser=parser)
    transform = etree.XSLT(xslt)
    result = str(transform(dom))
    news_item = newsletter.news_items[0].news_item
    activity = newsletter.activities[0].activity

    if filename == "Nieuwsbrief_20_{lang}_Nieuws":
        assert f"/users/{news_item.user.id}/avatar" in result

    if filename in (
        "Nieuwsbrief_20_{lang}_Nieuws",
        "Nieuwsbrief_20_{lang}_Activiteiten",
    ):
        assert f"<strong>via</strong> {lang}" in result
        assert '<a href="https://svia.nl">svia.nl</a>' in result
        assert '<img alt="img" src="https://svia.nl/favicon.ico" />' in result
        assert "OVERWRITTEN" not in result

    if filename in (
        "Nieuwsbrief_20_{lang}_Nieuws",
        "Nieuwsbrief2_Mededeling_Overzicht_{lang}",
    ):
        news_item_title = getattr(news_item, "{lang}_title".format(lang=lang.lower()))
        assert news_item_title in result

    # The description is only in the full text xml, not the overview.
    if filename == "Nieuwsbrief_20_{lang}_Nieuws":
        assert "OVERWRITTEN" not in result
        assert f"OVERWRITE NEWS {lang}" in result

    if filename in (
        "Nieuwsbrief_20_{lang}_Activiteiten",
        "Nieuwsbrief2_Activiteit_Overzicht_{lang}",
    ):
        activity_title = getattr(activity, "{lang}_name".format(lang=lang.lower()))
        assert activity_title in result

    # The description is only in the full text xml, not the overview.
    if filename == "Nieuwsbrief_20_{lang}_Activiteiten":
        assert "OVERWRITTEN" not in result
        assert f"OVERWRITE ACTIVITY {lang}" in result
