from flask_wtf.csrf import generate_csrf


def test_create_alv(admin_user, admin_client, activity_factory):
    admin_client.login(admin_user)

    activity = activity_factory()

    rv = admin_client.get("/alv/create/")
    assert rv.status_code == 200

    rv = admin_client.post(
        "/alv/create/",
        data={
            "csrf_token": generate_csrf(),
            "nl_name": "nl_name",
            "en_name": "en_name",
            "date": "2019-11-16",
            "activity": activity.id,
            "chairman": admin_user.id,
            "secretary": admin_user.id,
        },
    )
    assert rv.status_code == 200
