from unittest import TestCase

from app.api.company.company_job import CompanyJobSchema


class TestCompanyJobSchema(TestCase):
    def test_create_company_job(self):
        data = {
            "title_nl": "Consulting Engineer - Master (Graduate)",
            "title_en": "Consulting Engineer - Master (Graduate)",
            "contact_name": "Justyna Starczala",
            "contact_email": "jstarcza@cisco.com",
            "contact_address": "Street 1",
            "contact_city": "City",
            "contract_of_service": "full-time",
            "website": "https://jobs.cisco.com/jobs/ProjectDetail/"
            "Consulting-Engineer-Master-Graduate-Netherlands"
            "-March-2020/1267721",
            "phone": "+48 123219746",
            "description_en": "**Program Start Date:**",
            "description_nl": "**Program Start Date:**",
            "start_date": "2019-09-24",
            "end_date": "2020-09-24",
            "enabled": True,
        }

        schema = CompanyJobSchema()
        schema.load(data)
