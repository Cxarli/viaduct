import glob


def test_title_present(app):
    template_dir = "app/templates/**/*.htm*"

    for template in glob.glob(template_dir, recursive=True):
        template = template[len("app/templates/") :]
        if template.startswith("macro") or template.startswith("email"):
            continue

        # Skip these templates, as they are included or have manual title
        if template in {
            "activity/view_home.htm",
            "content.htm",
            "home/jobs_simple.htm",
            "navigation/language_dropdown.htm",
            "navigation/sidenav.htm",
            "navigation/user_dropdown.htm",
            "navigation/view_backtrack.htm",
            "navigation/view_bar.htm",
            "navigation/view_sidebar.htm",
            "page/get_footer.htm",
            "page/view_super.htm",
            "slides/ovslide.html",
            "user/macros/signup_nav.htm",
            "user/macros/uvanetid.htm",
        }:
            continue

        template = app.jinja_env.get_template(template)
        assert "title" in template.blocks
