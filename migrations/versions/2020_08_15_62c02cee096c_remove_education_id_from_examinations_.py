"""Remove education_id from examinations and fill education_course.

Revision ID: 62c02cee096c
Revises: aa2be2d1d9e4
Create Date: 2020-08-15 17:03:15.316761

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.dialects import postgresql

from app.models.base_model import BaseEntity
from app.models.course import Course
from app.models.education import Education

# revision identifiers, used by Alembic.
revision = "62c02cee096c"
down_revision = "aa2be2d1d9e4"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


class EducationCourse(db.Model):
    __tablename__ = "education_course"
    education_id = Column(Integer, ForeignKey("education.id"), primary_key=True)
    course_id = Column(Integer, ForeignKey("course.id"), primary_key=True)
    year = Column(Integer, nullable=True)
    periods = Column(postgresql.ARRAY(Integer), nullable=True)


class IntermediateExamination(db.Model, BaseEntity):
    __tablename__ = "examination"

    course_id = db.Column(db.Integer, db.ForeignKey("course.id"))
    education_id = db.Column(db.Integer, db.ForeignKey("education.id"))


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # Add education-course items according to current examination table.
    unique_pairs = {
        (e.course_id, e.education_id)
        for e in db.session.query(IntermediateExamination).all()
        if e.course_id and e.education_id
    }

    for course_id, education_id in unique_pairs:
        stmt = education_course.insert().values(
            course_id=course_id, education_id=education_id
        )
        db.session.execute(stmt)

    # Add extra records for known courses with multiple educations.
    # Linear Algebra already has mixed educations in examination table.

    # Add BSc IN for Webtechnologie.
    bsc_in = db.session.query(Education).filter_by(datanose_code="BSc IN").one_or_none()
    webtech = (
        db.session.query(Course).filter_by(datanose_code="5082WEBT6Y").one_or_none()
    )

    if bsc_in and webtech:
        stmt = education_course.insert().values(
            course_id=webtech.id, education_id=bsc_in.id
        )
        db.session.execute(stmt)
    else:
        print("\n!~ Warning: Bsc IN or 5082WEBT6Y not found..\n")

    # Add BSc IK for Moderne Databases.
    bsc_ik = db.session.query(Education).filter_by(datanose_code="BSc IK").one_or_none()
    mod_dat = (
        db.session.query(Course).filter_by(datanose_code="5062MODA6Y").one_or_none()
    )

    if bsc_ik and mod_dat:
        stmt = education_course.insert().values(
            course_id=mod_dat.id, education_id=bsc_ik.id
        )
        db.session.execute(stmt)
    else:
        print("\n!~ Warning: Bsc IK or 5062MODA6Y not found..\n")

    # Commit above changes before dropping the education_id column.
    db.session.commit()

    # Drop education_id from examination table.
    op.drop_constraint(
        "fk_examination_education_id_education", "examination", type_="foreignkey"
    )
    op.drop_column("examination", "education_id")


def downgrade():
    create_session()

    # Add education_id from examination table.
    op.add_column(
        "examination",
        sa.Column("education_id", sa.INTEGER(), autoincrement=False, nullable=True),
    )
    op.create_foreign_key(
        "fk_examination_education_id_education",
        "examination",
        "education",
        ["education_id"],
        ["id"],
    )


# vim: ft=python
