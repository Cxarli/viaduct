"""Fix naming for navigation table.

Revision ID: e1e37ca6d57f
Revises: 73b5009436ae
Create Date: 2020-10-06 13:07:19.383004

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "e1e37ca6d57f"
down_revision = "565f53487975"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


class NavigationEntry(db.Model):
    __tablename__ = "nagivation_entry"

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(256))


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    for entry in db.session.query(NavigationEntry).all():
        if entry.url:
            entry.url = entry.url.lstrip("/")
            db.session.add(entry)
    db.session.commit()

    op.rename_table("nagivation_entry", "navigation_entry")


def downgrade():
    create_session()

    op.rename_table("navigation_entry", "nagivation_entry")

    for entry in db.session.query(NavigationEntry).all():
        if entry.url:
            entry.url = "/" + entry.url
            db.session.add(entry)
    db.session.commit()


# vim: ft=python
