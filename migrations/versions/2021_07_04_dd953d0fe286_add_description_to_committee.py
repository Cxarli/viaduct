"""Add description to committee.

Revision ID: dd953d0fe286
Revises: 2c8fd6a97258
Create Date: 2021-02-12 21:41:20.009231

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "dd953d0fe286"
down_revision = "3c5c79f6bee8"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    op.add_column(
        "committee",
        sa.Column("en_description", sa.Text(), server_default="", nullable=False),
    )
    op.add_column(
        "committee",
        sa.Column("nl_description", sa.Text(), server_default="", nullable=False),
    )


def downgrade():
    create_session()

    op.drop_column("committee", "nl_description")
    op.drop_column("committee", "en_description")


# vim: ft=python
