"""empty message.

Revision ID: ceb39ad3f5c5
Revises: ('b763ff436a2e', '6d8e94072935')
Create Date: 2019-03-05 11:22:43.800438

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "ceb39ad3f5c5"
down_revision = ("b763ff436a2e", "6d8e94072935")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    pass


def downgrade():
    create_session()

    pass


# vim: ft=python
