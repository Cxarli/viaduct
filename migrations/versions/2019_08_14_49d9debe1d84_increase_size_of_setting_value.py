"""Increase size of setting.value.

Revision ID: 49d9debe1d84
Revises: f27b45bad719
Create Date: 2019-08-14 13:26:59.118332

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "49d9debe1d84"
down_revision = "f27b45bad719"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    op.alter_column(
        "setting",
        "value",
        existing_type=sa.VARCHAR(length=256),
        type_=sa.String(length=4096),
        existing_nullable=True,
    )


def downgrade():
    create_session()

    op.alter_column(
        "setting",
        "value",
        existing_type=sa.String(length=1024),
        type_=sa.VARCHAR(length=256),
        existing_nullable=True,
    )


# vim: ft=python
