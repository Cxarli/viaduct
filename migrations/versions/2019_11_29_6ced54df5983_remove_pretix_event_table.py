"""Remove pretix event table..

Revision ID: 6ced54df5983
Revises: 8332912803c6
Create Date: 2019-11-29 17:41:25.508378

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "6ced54df5983"
down_revision = "8332912803c6"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("pretix_event")
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "pretix_event",
        sa.Column("id", sa.INTEGER(), nullable=False),
        sa.Column(
            "created",
            postgresql.TIMESTAMP(timezone=True),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column(
            "modified",
            postgresql.TIMESTAMP(timezone=True),
            autoincrement=False,
            nullable=True,
        ),
        sa.Column("group_id", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("event_slug", sa.VARCHAR(), autoincrement=False, nullable=False),
        sa.Column("quota_id", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.Column("product_id", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.ForeignKeyConstraint(
            ["group_id"], ["group.id"], name="fk_pretix_event_group_id_group"
        ),
        sa.PrimaryKeyConstraint("id", name="pk_pretix_event"),
        sa.UniqueConstraint("event_slug", name="uq_pretix_event_event_slug"),
    )
    # ### end Alembic commands ###


# vim: ft=python
