"""empty message.

Revision ID: 9b41ea14b536
Revises: ('99cac9949a03', '356b0c3ed4f1')
Create Date: 2019-11-13 23:08:29.473206

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "9b41ea14b536"
down_revision = ("99cac9949a03", "356b0c3ed4f1")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    pass


def downgrade():
    create_session()

    pass


# vim: ft=python
