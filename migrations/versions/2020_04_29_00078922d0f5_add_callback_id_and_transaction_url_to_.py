"""Add callback id and transaction url to transaction, make columns unique.

Revision ID: 00078922d0f5
Revises: bbb384b4b3e0
Create Date: 2020-04-29 12:20:44.894469

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "00078922d0f5"
down_revision = "e7ff2379123e"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # Add callback_id
    op.add_column(
        "transaction",
        sa.Column("callback_id", sa.String(length=256), nullable=True),
    )

    # Set the old transactions callback id to something that can't be used
    op.execute("UPDATE transaction SET callback_id = '_old_' || transaction.id")
    op.alter_column("transaction", "callback_id", nullable=False)
    op.create_unique_constraint(
        op.f("uq_transaction_callback_id"), "transaction", ["callback_id"]
    )

    # Add has_paid boolean
    # First set nullable to true
    op.add_column("transaction", sa.Column("has_paid", sa.Boolean(), nullable=True))
    # Set all the null values to true
    op.execute("UPDATE transaction SET has_paid = true")
    # Set the nullability to false again
    op.alter_column("transaction", "has_paid", nullable=False)

    op.create_unique_constraint(
        op.f("uq_transaction_mollie_id"), "transaction", ["mollie_id"]
    )


def downgrade():
    create_session()

    op.drop_constraint(op.f("uq_transaction_mollie_id"), "transaction", type_="unique")
    op.drop_constraint(
        op.f("uq_transaction_callback_id"), "transaction", type_="unique"
    )
    op.drop_column("transaction", "has_paid")
    op.drop_column("transaction", "callback_id")


# vim: ft=python
