import logging
from typing import List, NamedTuple, Optional

from googleapiclient.errors import HttpError

from app.utils.google import build_groups_service, build_user_service

_logger = logging.getLogger(__name__)


class GoogleGroup(NamedTuple):
    id: str
    email: str
    name: str
    emails: List[str]


def find_group_by_group_key(group_key: str) -> Optional[GoogleGroup]:
    service = build_groups_service()
    if service is None:
        return None
    try:
        response = service.groups().get(groupKey=group_key).execute()
        google_group = GoogleGroup(
            response.get("id"), response.get("email"), response.get("name"), list()
        )
        response = service.members().list(groupKey=group_key).execute()

        for member in response.get("members", list()):
            if "email" in member:
                google_group.emails.append(member.get("email"))
        return google_group
    except HttpError as e:
        if e.resp.status == 404:
            _logger.debug("404 on GET google group with groupKey '%s'", group_key)
        return None


def find_google_user(user_key: str):
    user_api = build_user_service()
    if user_api is None:
        return None

    try:
        return user_api.users().get(userKey=user_key).execute()
    except HttpError as e:
        if e.resp.status == 404:
            _logger.debug("404 on GET google user with userKey '%s'", user_key)
        return None
