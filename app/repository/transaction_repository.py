from typing import Optional

from app import db
from app.models.mollie import Transaction, TransactionCallbackMixin


def find_transaction_by_id(transaction_id: int) -> Optional[Transaction]:
    return db.session.query(Transaction).filter_by(id=transaction_id).one_or_none()


def find_transaction_by_mollie_id(mollie_id: str) -> Optional[Transaction]:
    return db.session.query(Transaction).filter_by(mollie_id=mollie_id).one_or_none()


def find_transaction_by_callback_id(callback_id: str) -> Optional[Transaction]:
    return (
        db.session.query(Transaction).filter_by(callback_id=callback_id).one_or_none()
    )


def create_transaction_with_callback(
    mollie_id: str,
    status: str,
    callback_id: str,
    callback: TransactionCallbackMixin,
) -> Transaction:
    transaction = Transaction.from_mollie_id_status_callback_id(
        mollie_id, status, callback_id
    )

    callback.transaction = transaction  # type: ignore
    db.session.add(transaction)
    db.session.add(callback)
    db.session.commit()

    return transaction


def update_has_paid(transaction: Transaction, has_paid: bool) -> Transaction:
    transaction.has_paid = has_paid
    db.session.add(transaction)
    db.session.commit()
    return transaction


def update_status(transaction: Transaction, status: str) -> Transaction:
    # workaround for compatibility with mollie api v2
    if status == "canceled":
        status = "cancelled"

    transaction.status = status
    db.session.add(transaction)
    db.session.commit()
    return transaction
