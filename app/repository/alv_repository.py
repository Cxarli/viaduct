from typing import List, Tuple
from sqlalchemy.orm import joinedload, raiseload

from app import db
from app.models.activity import Activity
from app.models.alv_model import (
    Alv,
    AlvDocument,
    AlvDocumentVersion,
    AlvEventEntry,
    AlvUser,
    AlvUserProxy,
)
from app.models.user import User


def create_document():
    return AlvDocument()


def create_document_version():
    return AlvDocumentVersion()


def save(alv):
    db.session.add(alv)
    db.session.commit()
    return alv


def save_document(alv_document):
    db.session.add(alv_document)
    db.session.commit()


def save_document_version(alv_doc_version):
    db.session.add(alv_doc_version)
    db.session.commit()


def find_all_alv():
    return (
        db.session.query(Alv)
        .order_by(Alv.date.desc())
        .options(
            joinedload(Alv.activity).load_only(
                Activity.id,
                Activity.nl_name,
                Activity.en_name,
                Activity.start_time,
            ),
            joinedload(Alv.chairman).load_only(
                User.id,
                User.first_name,
                User.last_name,
            ),
            joinedload(Alv.secretary).load_only(
                User.id,
                User.first_name,
                User.last_name,
            ),
            raiseload("*"),
        )
        .all()
    )


def find_alv_by_id(alv_id, include_presidium=True, include_documents=False):
    """Retrieve ALV from the database, with the minimal foreign key info."""
    q = (
        db.session.query(Alv)
        .filter_by(id=alv_id)
        .options(
            joinedload(Alv.activity).load_only(
                Activity.nl_name,
                Activity.en_name,
                Activity.nl_description,
                Activity.en_description,
                Activity.start_time,
            )
        )
    )

    if include_presidium:
        q = q.options(
            joinedload(Alv.chairman).load_only(User.first_name, User.last_name),
            joinedload(Alv.secretary).load_only(User.first_name, User.last_name),
        )
    if include_documents:
        q = q.options(
            joinedload(Alv.documents)
            .joinedload(AlvDocument.versions)
            .subqueryload(AlvDocumentVersion.file)
        ).options(joinedload(Alv.minutes_file))

    q = q.options(raiseload("*"))
    return q.one_or_none()


def find_alv_document_by_id(alv_document_id, include_versions=False):
    q = (
        db.session.query(AlvDocument)
        .filter_by(id=alv_document_id)
        .options(raiseload("*"))
    )

    if include_versions:
        q = q.options(
            joinedload(AlvDocument.versions).joinedload(AlvDocumentVersion.alv_document)
        )

    return q.one_or_none()


def find_alv_document_version_by_id(alv_document_version_id):
    return (
        db.session.query(AlvDocumentVersion)
        .filter_by(id=alv_document_version_id)
        .options(raiseload("*"))
        .one_or_none()
    )


def delete_alv(alv):
    db.session.delete(alv)
    db.session.commit()


def delete_parsed_alv_data(alv: Alv):
    alv.meta = None
    db.session.add(alv)

    db.session.query(AlvEventEntry).filter_by(alv_id=alv.id).delete(
        synchronize_session=False
    )

    db.session.query(AlvUser).filter_by(alv_id=alv.id).delete(synchronize_session=False)

    db.session.query(AlvUserProxy).filter_by(alv_id=alv.id).delete(
        synchronize_session=False
    )


def create_parsed_alv_data(
    alv: Alv,
    events: List[AlvEventEntry],
    alv_users: List[AlvUser],
    alv_proxies: List[AlvUserProxy],
):

    db.session.add(alv)
    db.session.add_all(events)
    db.session.add_all(alv_users)
    db.session.add_all(alv_proxies)


def get_parsed_alv_data(
    alv: Alv,
) -> Tuple[List[AlvEventEntry], List[AlvUser], List[AlvUserProxy]]:
    events = db.session.query(AlvEventEntry).filter_by(alv_id=alv.id).all()
    users = db.session.query(AlvUser).filter_by(alv_id=alv.id).all()
    proxies = db.session.query(AlvUserProxy).filter_by(alv_id=alv.id).all()

    return events, users, proxies
