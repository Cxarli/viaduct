import logging
from collections import defaultdict
from typing import Set

from app import db
from app.models.group import Group
from app.models.role_model import GroupRole
from app.models.user import User
from app.roles import Roles

_logger = logging.getLogger(__name__)


def load_user_roles(user_id):
    roles = (
        db.session.query(GroupRole.role)
        .join(GroupRole.group, Group.users)
        .filter(User.id == user_id)
        .group_by(GroupRole.role)
        .all()
    )
    rv = []
    for role in roles:
        try:
            rv.append(Roles[role.role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return rv


def load_user_roles_with_groups(user_id):
    roles = (
        db.session.query(GroupRole.role, Group)
        .join(GroupRole.group, Group.users)
        .filter(User.id == user_id)
        .all()
    )
    roles_with_groups = defaultdict(list)
    for role, group in roles:
        try:
            roles_with_groups[Roles[role]].append(group)
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return roles_with_groups


def load_user_groups_with_roles(user_id):
    roles = (
        db.session.query(GroupRole.role, Group)
        .join(GroupRole.group, Group.users)
        .filter(User.id == user_id)
        .all()
    )
    groups_with_roles = defaultdict(list)
    for role, group in roles:
        try:
            groups_with_roles[group].append(Roles[role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return groups_with_roles


def find_all_roles_by_group_id(group_id) -> Set[Roles]:
    roles = (
        db.session.query(GroupRole)
        .filter(GroupRole.group_id == group_id)
        .order_by(GroupRole.role)
        .all()
    )
    rv = []
    for role in roles:
        try:
            rv.append(Roles[role.role])
        except KeyError:
            # Handle unknown roles gracefully.
            pass
    return set(rv)


def delete_roles_by_group(group_id, removed_roles):
    roles = [role.name for role in removed_roles]
    db.session.query(GroupRole).filter(
        GroupRole.group_id == group_id, GroupRole.role.in_(roles)
    ).delete(synchronize_session="fetch")
    db.session.commit()


def insert_roles_by_group(group_id, added_roles):
    roles = []
    for role in added_roles:
        group_role = GroupRole()
        group_role.group_id = group_id
        group_role.role = role.name
        roles.append(group_role)

    db.session.add_all(roles)
    db.session.commit()


def get_groups_with_role(role):
    return (
        db.session.query(Group)
        .join(GroupRole)
        .filter(GroupRole.role == role.name)
        .all()
    )
