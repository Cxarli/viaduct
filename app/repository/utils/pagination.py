from typing import Union

from flask_sqlalchemy import BaseQuery
from sqlalchemy import Column
from sqlalchemy.sql.operators import ColumnOperators

from app import db


def search_columns(
    query: BaseQuery,
    search: str,
    *searchable_columns: Union[Column, ColumnOperators],
) -> BaseQuery:
    if search == "":
        return query

    filters = []
    for column in searchable_columns:
        filters.append(column.ilike(f"%{search}%"))
    return query.filter(db.or_(*filters))
