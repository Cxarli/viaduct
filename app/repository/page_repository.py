from datetime import datetime, timezone
from typing import List, Optional

from sqlalchemy import func
from sqlalchemy.sql.expression import case

from app import db
from app.api.schema import PageSearchParameters
from app.exceptions.base import DuplicateResourceException
from app.models.page import Page, PageRevision
from app.models.user import User
from app.repository.utils.pagination import search_columns
from app.utils.pagination import Pagination


def find_page_by_id(page_id):
    return db.session.query(Page).filter_by(id=page_id).one_or_none()


def find_page_by_path(path):
    return db.session.query(Page).filter(Page.path == _filter_path(path)).one_or_none()


def create_page(page_type: str, path: str, require_membership_to_view: bool) -> Page:
    if find_page_by_path(_filter_path(path)):
        raise DuplicateResourceException("page", path)

    page = Page(
        path=_filter_path(path), type=page_type, needs_paid=require_membership_to_view
    )

    db.session.add(page)
    db.session.commit()

    return page


def update_page(page: Page, path: str, require_membership_to_view: bool) -> Page:
    page.path = _filter_path(path)
    page.deleted = None
    page.needs_paid = require_membership_to_view

    db.session.add(page)
    db.session.commit()

    return page


def _filter_path(path):
    if not path:
        return path
    if path[0] == "/":
        path = path[1:]
    if path[-1] == "/":
        path = path[:-1]
    return path


def delete_page(page):
    page.deleted = datetime.now(timezone.utc)
    db.session.commit()


def paginated_search_all_pages(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Page).order_by(Page.deleted.desc(), Page.id.asc())

    q = search_columns(q, pagination.search, Page.path)

    return q.paginate(pagination.page, pagination.limit, error_out=False)


def create_page_revision(
    page: Page,
    author: User,
    title,
    content,
    revision_comment: str,
) -> PageRevision:
    rev = PageRevision(
        page=page,
        nl_title=title["nl"],
        en_title=title["en"],
        comment=revision_comment,
        user=author,
        nl_content=content["nl"],
        en_content=content["en"],
    )

    db.session.add(rev)
    db.session.commit()

    return rev


def get_all_revisions(page: Page) -> List[PageRevision]:
    return db.session.query(PageRevision).filter_by(page_id=page.id).all()


def get_latest_revision(page: Page) -> Optional[PageRevision]:
    return (
        db.session.query(PageRevision)
        .filter(PageRevision.page_id == page.id)
        .order_by(PageRevision.id.desc())
        .first()
    )


def get_latest_revisions(pages: List[Page]) -> List[PageRevision]:
    # TODO https://github.com/sqlalchemy/sqlalchemy2-stubs/pull/42/files
    ordering = case(  # type: ignore
        {id: index for index, id in enumerate([i.id for i in pages])},
        value=PageRevision.page_id,
    )
    subquery = (
        db.session.query(func.max(PageRevision.id))
        .group_by(PageRevision.page_id)
        .filter(PageRevision.page_id.in_([p.id for p in pages]))
        .subquery()
    )
    return (
        db.session.query(PageRevision)
        .filter(PageRevision.id.in_(subquery))
        .order_by(ordering)
        .all()
    )


def get_revision_by_id(page: Page, revision_id: int) -> Optional[PageRevision]:
    return (
        db.session.query(PageRevision)
        .filter(PageRevision.id == revision_id)
        .one_or_none()
    )


def find_all_committees() -> List[Page]:
    """Get the committee pages which are not deleted."""
    return (
        db.session.query(Page)
        .filter(Page.type == "committee", Page.deleted.is_(None))
        .all()
    )


def find_all_active_pages() -> List[Page]:
    return (
        db.session.query(Page).filter(Page.type == "page", Page.deleted.is_(None)).all()
    )


def find_latest_page_revisions_containing(search) -> List[PageRevision]:
    latest_revisions = (
        db.session.query(func.max(PageRevision.id))
        .group_by(PageRevision.page_id)
        .subquery()
    )

    return (
        db.session.query(PageRevision)
        .filter(
            PageRevision.id.in_(latest_revisions),
            db.or_(
                PageRevision.en_content.ilike(f"%{search}%"),
                PageRevision.nl_content.ilike(f"%{search}%"),
            ),
        )
        .all()
    )
