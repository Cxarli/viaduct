from flask_babel import lazy_gettext as _  # noqa
from wtforms import StringField
from wtforms.validators import Email, ValidationError


class EmailField(StringField):
    def __init__(self, *args, **kwargs):
        super(EmailField, self).__init__(*args, **kwargs)
        self._email_validator = Email()

    def pre_validate(self, form):
        self._email_validator(form, self)
        # The current version of WTForms does not check for spaces
        # this is fixed but not released yet, so we do it ourselves
        if " " in self.data:
            raise ValidationError(self.gettext("Invalid email address."))

        super().pre_validate(form)

    def process_formdata(self, valuelist):
        super().process_formdata([d.strip().lower() for d in valuelist])
