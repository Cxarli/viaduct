from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, SelectField, StringField
from wtforms.validators import InputRequired

from app.enums import ProgrammeType


class EducationForm(FlaskForm):
    programme_type = SelectField(
        _("Programme type"),
        choices=[(p.name, p.abbreviation) for p in ProgrammeType],
        validators=[InputRequired()],
    )

    nl_name = StringField(_("Dutch name"), validators=[InputRequired()])
    en_name = StringField(_("English name"), validators=[InputRequired()])
    is_via_programme = BooleanField(_("Programme belongs to via"))
    datanose_code = StringField(_("DataNose code"))
