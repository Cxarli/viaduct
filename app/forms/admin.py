from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import InputRequired, Optional


class BannerForm(FlaskForm):
    message_nl = StringField(_("Dutch message"), validators=[Optional()])
    message_en = StringField(_("English message"), validators=[Optional()])
    background = StringField(_("Background"), validators=[Optional()])


class PrivacyPoliciesForm(FlaskForm):
    url_en = StringField(_("URL English privacy policy"), validators=[InputRequired()])
    url_nl = StringField(_("URL Dutch privacy policy"), validators=[InputRequired()])
