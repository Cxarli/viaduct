from flask_restful import Resource
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import require_oauth
from app.oauth_scopes import Scopes
from app.service import google_service


class GoogleGroupSchema(RestSchema):

    # List view
    google_id = fields.String(dump_only=True, attribute="id")
    google_email = fields.String(dump_only=True, attribute="email")
    google_name = fields.String(dump_only=True, attribute="name")

    emails = fields.List(fields.String(), required=True, attribute="emails")


class GoogleGroupResource(Resource):
    schema_get = GoogleGroupSchema()

    @require_oauth(Scopes.group)
    def get(self, group_id: int):
        google_group = google_service.find_google_group_by_id(group_id)
        return self.schema_get.dump(google_group)
