from http import HTTPStatus

import pydantic
from authlib.integrations.flask_oauth2 import current_token
from flask import Response, request
from flask.views import MethodView
from flask_restful import Resource
from marshmallow import ValidationError, fields, validates_schema

from app import db
from app.api.schema import RestSchema, UserIdInt
from app.api.schema import schema_registry
from app.api.user.user import UserSchema
from app.decorators import json_schema, require_oauth, require_role
from app.exceptions.base import ValidationException
from app.models.challenge import Challenge
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import challenge_service, role_service


class ChallengeSubmissionSchema(RestSchema):
    submission = fields.String(required=True)


class ChallengeSubmissionResponseSchema(RestSchema):
    validated = fields.Boolean(dump_only=True)
    approved = fields.Boolean(dump_only=True)


class ChallengeSchema(RestSchema):
    id = fields.Int(dump_only=True)
    name = fields.String(dump_only=True)
    description = fields.String(dump_only=True)
    hint = fields.Str(dump_only=True)
    open = fields.Boolean(dump_only=True, attribute="open")
    start_date = fields.Date(dump_only=True)
    end_date = fields.Date(dump_only=True)
    weight = fields.Integer(dump_only=True)
    type = fields.String(dump_only=True)

    # Used in list overview.
    approved = fields.Boolean(dump_only=True)


class EditChallengeSchema(RestSchema):
    id = fields.Int(dump_only=True)
    name = fields.String(required=True)
    description = fields.String(required=True)
    hint = fields.Str(required=True)
    start_date = fields.Date(required=True)
    end_date = fields.Date(required=True)
    weight = fields.Integer(required=True)
    answer = fields.String(required=True)

    @validates_schema
    def validate_timerange(self, data, **kwargs):
        if data["start_date"] >= data["end_date"]:
            raise ValidationError({"start_time": "Start time must be before end time"})


class CompetitorSchema(RestSchema):
    user = fields.Nested(
        UserSchema(
            only=("id", "first_name", "last_name"),
            dump_only=("id", "first_name", "last_name"),
        )
    )
    points = fields.Integer(dump_only=True)
    challenges = fields.Nested(ChallengeSchema(only=("name", "weight")), many=True)


class ChallengeResource(Resource):
    schema = EditChallengeSchema()

    @require_oauth(Scopes.challenge)
    @require_role(Roles.CHALLENGE_WRITE)
    def get(self, challenge):
        return self.schema.dump(challenge)

    @require_oauth(Scopes.challenge)
    @require_role(Roles.CHALLENGE_WRITE)
    @json_schema(schema)
    def put(self, data, challenge):
        challenge_service.update(
            challenge,
            name=data["name"],
            description=data["description"],
            hint=data["hint"],
            start_date=data["start_date"],
            end_date=data["end_date"],
            weight=data["weight"],
            answer=data["answer"],
        )
        return self.schema.dump(challenge)


class ChallengeListResource(Resource):
    schema = EditChallengeSchema()

    @require_oauth(Scopes.challenge)
    @require_role(Roles.CHALLENGE_WRITE)
    @json_schema(schema)
    def post(self, data):
        challenge = challenge_service.create_challenge(
            name=data["name"],
            description=data["description"],
            hint=data["hint"],
            start_date=data["start_date"],
            end_date=data["end_date"],
            weight=data["weight"],
            type_="Text",
            answer=data["answer"],
        )
        return self.schema.dump(challenge), 201


class ChallengeRankingResource(Resource):
    """Resource to retrieve activity, focused on Pretix based activities."""

    schema = CompetitorSchema(many=True)

    @require_oauth(Scopes.challenge)
    def get(self):
        ranking = [
            {"points": c.points, "user": c.user}
            for c in challenge_service.get_ranking()
        ]

        ranking_with_challenges = []
        for r in ranking:
            challenges = challenge_service.find_with_approved_submission(
                False, r["user"].id
            )
            approved_challenges = [c[0] for c in challenges if c[1] is not None]
            r["challenges"] = approved_challenges
            ranking_with_challenges.append(r)
        return self.schema.dump(ranking_with_challenges)


class UserChallengeListResource(Resource):
    schema = ChallengeSchema(many=True)

    @require_oauth(Scopes.challenge)
    def get(self):
        user = current_token.user
        is_admin = role_service.user_has_role(user, Roles.CHALLENGE_WRITE)
        challenge_submissions = challenge_service.find_with_approved_submission(
            closed=is_admin, user_id=user.id
        )

        challenges = []
        for challenge, submission in challenge_submissions:
            challenge.approved = submission is not None
            challenges.append(challenge)
        return self.schema.dump(challenges)


class ChallengeSubmissionResource(Resource):
    schema_post = ChallengeSubmissionSchema()
    schema_resp = ChallengeSubmissionResponseSchema()

    @require_oauth(Scopes.challenge)
    @json_schema(schema_post)
    def post(self, data, challenge: Challenge):
        submission = challenge_service.create_submission(
            challenge=challenge,
            user_id=current_token.user.id,
            submission=data["submission"],
            image_path=None,
        )

        validated, approved = challenge_service.validate_question(submission, challenge)

        return self.schema_resp.dump({"validated": validated, "approved": approved})


class ChallengeAdminSubmissionResource(MethodView):
    @schema_registry.register
    class ChallengeAdminSubmission(pydantic.BaseModel):
        user_id: UserIdInt

    @require_oauth(Scopes.challenge)
    @require_role(Roles.CHALLENGE_WRITE)
    def post(self, challenge: Challenge):
        try:
            req_json = request.get_json(force=True)
            data = self.ChallengeAdminSubmission(**req_json)
            submission = challenge_service.create_submission(
                challenge=challenge,
                user_id=data.user_id,
                submission=None,
                image_path=None,
            )

            submission.approved = True
            challenge_service.assign_points_to_user(
                challenge.weight, submission.user_id
            )

            db.session.add(submission)
            db.session.commit()

            return Response(status=HTTPStatus.CREATED)
        except pydantic.ValidationError as e:
            raise ValidationException(e.errors()) from e
