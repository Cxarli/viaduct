from app import app, rest_api
from app.api.activity import (
    ActivityListResource,
    ActivityPictureResource,
    ActivityResource,
)
from app.api.alv import (
    AlvDocumentListResource,
    AlvDocumentResource,
    AlvListResource,
    AlvMinuteResource,
    AlvParseMinuteResource,
    AlvParsedMinuteResource,
    AlvResource,
)
from app.api.bug import BugReportView
from app.api.challenge import (
    ChallengeAdminSubmissionResource,
    ChallengeListResource,
    ChallengeRankingResource,
    ChallengeResource,
    ChallengeSubmissionResource,
    UserChallengeListResource,
)
from app.api.committee.committee import (
    CommitteeListResource,
    CommitteePictureResource,
    CommitteeResource,
    CommitteeTagsResource,
)
from app.api.company.company import (
    CompanyListResource,
    CompanyResource,
    CompanyPublicResource,
)
from app.api.company.company_banner import BannerResource, CompanyBannerResource
from app.api.company.company_job import (
    CompanyJobListResource,
    JobContractsOfServices,
    JobListResource,
    JobResource,
)
from app.api.company.company_logo import CompanyLogoResource
from app.api.company.company_profile import (
    CompanyProfileListResource,
    CompanyProfileIdResource,
    CompanyProfileSlugResource,
)
from app.api.declaration.declaration import (
    DeclarationResource,
    DeclarationUploadResource,
)
from app.api.domjudge import (
    DOMjudgeBannerResource,
    DOMjudgeContestListResource,
    DOMjudgeContestResource,
    DOMjudgeContestUserListResource,
)
from app.api.examination.course import (
    CourseListResource,
    CourseResource,
    UserCoursesResource,
)
from app.api.examination.education import (
    EducationListResource,
    EducationResource,
    UserEducationsResource,
)
from app.api.examination.examination import (
    CourseExaminationListResource,
    ExaminationAnswerUploadResource,
    ExaminationExamUploadResource,
    ExaminationListResource,
    ExaminationResource,
)
from app.api.group.google import GoogleGroupResource
from app.api.group.group import GroupListResource, GroupResource
from app.api.group.roles import GroupRoleSchema
from app.api.group.users import GroupUserListResource
from app.api.health import HealthCheckResource
from app.api.mailinglist.mailinglist import MailingListListResource, MailingListResource
from app.api.meeting import MeetingListResource, MeetingResource
from app.api.navigation.navigation import (
    NavigationListResource,
    NavigationOrderResource,
    NavigationResource,
)
from app.api.news import NewsListResource, NewsResource
from app.api.newsletter import (
    NewsletterInitialResource,
    NewsletterListResource,
    NewsletterResource,
)
from app.api.page.page import (
    PageListResource,
    PagePreviewResource,
    PageRenderResource,
    PageResource,
    PageRevisionResource,
)
from app.api.photos.album import AlbumListResource, AlbumResource, RandomPhotoResource
from app.api.pimpy.minutes import (
    GroupMinuteResource,
    MinuteListResource,
    MinuteResource,
)
from app.api.pimpy.tasks import (
    GroupTaskListResource,
    TaskListResource,
    TaskResource,
    UserTaskListResource,
)
from app.api.pretix.pretix import PretixUserCheck, PretixUserEvents
from app.api.redirect.redirect import RedirectListResource, RedirectResource
from app.api.schema import SchemaResource
from app.api.search.search import SearchIndexResource, SearchResource
from app.api.task import (
    AsyncCopernicaSync,
    AsyncDatanoseSync,
    AsyncTaskResource,
    AsyncTasksResource,
)
from app.api.tutoring.tutors import (
    TutorCourseResource,
    TutorCoursesResource,
    TutorTutoringAcceptResource,
    TutorTutoringListResource,
    TutorTutoringResource,
    TutoringResource,
    TutorsResource,
)
from app.api.user.applications import (
    UserApplicationResource,
    UserRevokeApplicationResource,
)
from app.api.user.avatar import UserAvatarResource
from app.api.user.declaration import UserDeclarationsResource
from app.api.user.groups import UserGroupsResource
from app.api.user.mailinglist_subscriptions import UserMailingListSubscriptionResource
from app.api.user.merit import MeritUserListResource
from app.api.user.role import UserRoleResource
from app.api.user.tfa import UserTfaResource
from app.api.user.tutoring import UserTutoringListResource, UserTutoringResource
from app.api.user.user import UserListResource, UserResource

# TODO We gebruiken geen rest_api meer, want dat gaat weg in Flask 2.0
#  https://gitlab.com/studieverenigingvia/viaduct/-/merge_requests/818
app.add_url_rule(
    "/api/_schema/<string:schema_name>/",
    view_func=SchemaResource.as_view("schema"),
    endpoint="api.schema",
)
app.add_url_rule(
    "/api/bugs/", view_func=BugReportView.as_view("report"), endpoint="api.bugs.report"
)

rest_api.add_resource(HealthCheckResource, "/health/", endpoint="api.health")
# Pimpy Tasks
rest_api.add_resource(TaskListResource, "/tasks/", endpoint="api.tasks")
rest_api.add_resource(TaskResource, "/tasks/<string:task_id>/", endpoint="api.task")
rest_api.add_resource(
    GroupTaskListResource, "/groups/<group:group>/tasks/", endpoint="api.group.tasks"
)
rest_api.add_resource(
    UserTaskListResource, "/users/<user_self:user>/tasks/", endpoint="api.user.tasks"
)

# Pimpy Minutes
rest_api.add_resource(MinuteListResource, "/minutes/", endpoint="api.minutes")
rest_api.add_resource(
    MinuteResource, "/minutes/<minute:minute>/", endpoint="api.minute"
)
rest_api.add_resource(
    GroupMinuteResource,
    "/groups/<group:group>/minutes/",
    endpoint="api.group.minutes",
)

# Meeting
rest_api.add_resource(
    MeetingResource,
    "/meetings/<meeting:meeting>/",
    endpoint="api.meeting",
)
rest_api.add_resource(
    MeetingListResource,
    "/meetings/",
    endpoint="api.meetings",
)

# Alv
rest_api.add_resource(
    AlvResource,
    "/alvs/<alv:alv>/",
    endpoint="api.alv",
)
rest_api.add_resource(
    AlvListResource,
    "/alvs/",
    endpoint="api.alvs",
)
rest_api.add_resource(
    AlvParseMinuteResource, "/alvs/parse_minutes/", endpoint="api.alv.parse_minutes"
)
rest_api.add_resource(
    AlvParsedMinuteResource,
    "/alvs/<alv:alv>/parsed_minutes/",
    endpoint="api.alv.parsed_minutes",
)
rest_api.add_resource(
    AlvDocumentResource, "/alvs/document/<int:doc_id>/", endpoint="api.alv.document"
)
rest_api.add_resource(
    AlvMinuteResource, "/alvs/<alv:alv>/minutes/", endpoint="api.alv.minutes"
)
rest_api.add_resource(
    AlvDocumentListResource, "/alvs/<alv:alv>/documents/", endpoint="api.alv.documents"
)

# Activity
rest_api.add_resource(
    ActivityResource, "/activities/<activity:activity>/", endpoint="api.activity"
)
rest_api.add_resource(ActivityListResource, "/activities/", endpoint="api.activities")
rest_api.add_resource(
    ActivityPictureResource,
    "/activities/<activity:activity>/picture/",
    endpoint="api.activities.picture",
)

# User
rest_api.add_resource(UserListResource, "/users/", endpoint="api.users")
rest_api.add_resource(UserResource, "/users/<user_self:user>/", endpoint="api.user")
rest_api.add_resource(
    UserGroupsResource, "/users/<user_self:user>/groups/", endpoint="api.user.groups"
)
rest_api.add_resource(
    UserRoleResource, "/users/<user_self:user>/roles/", endpoint="api.user.roles"
)
rest_api.add_resource(
    UserApplicationResource,
    "/users/<user_self:user>/applications/",
    endpoint="api.user.applications",
)
rest_api.add_resource(
    UserRevokeApplicationResource,
    "/users/<user_self:user>/applications/<string:client_id>/revoke/",
    endpoint="api.user.applications.revoke",
)
rest_api.add_resource(
    UserTutoringListResource,
    "/users/<user_self:user>/tutoring/",
    endpoint="api.user.tutorings",
)
rest_api.add_resource(
    UserTutoringResource,
    "/users/<user_self:user>/tutoring/<tutoring:tutoring>/",
    endpoint="api.user.tutoring",
)
rest_api.add_resource(
    UserDeclarationsResource,
    "/users/<user_self:user>/declarations/",
    endpoint="api.user.declarations",
)

rest_api.add_resource(MeritUserListResource, "/users/merit/", endpoint="api.user.merit")

# Group
rest_api.add_resource(GroupListResource, "/groups/", endpoint="api.groups")
rest_api.add_resource(GroupResource, "/groups/<group:group>/", endpoint="api.group")
rest_api.add_resource(
    GroupUserListResource, "/groups/<int:group_id>/users/", endpoint="api.group.users"
)
rest_api.add_resource(
    GroupRoleSchema, "/groups/<group:group>/roles/", endpoint="api.group.roles"
)
rest_api.add_resource(
    GoogleGroupResource, "/groups/<int:group_id>/google/", endpoint="api.group.google"
)

# Courses
rest_api.add_resource(CourseListResource, "/courses/", endpoint="api.courses")
rest_api.add_resource(
    UserCoursesResource, "/users/<user_self:user>/courses/", endpoint="api.user.courses"
)
rest_api.add_resource(
    CourseResource, "/courses/<course:course>/", endpoint="api.course"
)

# Education
rest_api.add_resource(EducationListResource, "/educations/", endpoint="api.educations")
rest_api.add_resource(
    UserEducationsResource,
    "/users/<user_self:user>/educations/",
    endpoint="api.user.educations",
)
rest_api.add_resource(
    EducationResource, "/educations/<int:education_id>/", endpoint="api.education"
)

# Examination
rest_api.add_resource(
    ExaminationListResource,
    "/examinations/",
    endpoint="api.examinations",
)
rest_api.add_resource(
    ExaminationResource,
    "/examinations/<examination:examination>/",
    endpoint="api.examination",
)
rest_api.add_resource(
    ExaminationExamUploadResource,
    "/examinations/<examination:examination>/exam/",
    endpoint="api.examination.exam",
)
rest_api.add_resource(
    ExaminationAnswerUploadResource,
    "/examinations/<examination:examination>/answers/",
    endpoint="api.examination.answers",
)
rest_api.add_resource(
    CourseExaminationListResource,
    "/courses/<course:course>/examinations/",
    endpoint="api.course.examinations",
)

# Tutoring
rest_api.add_resource(TutorsResource, "/tutors/", endpoint="api.tutors")
rest_api.add_resource(TutoringResource, "/tutorings/", endpoint="api.tutorings")
rest_api.add_resource(
    TutorTutoringListResource,
    "/tutors/<user_self:user>/tutorings/",
    endpoint="api.tutors.tutorings",
)
rest_api.add_resource(
    TutorCoursesResource,
    "/tutors/<user_self:user>/courses/",
    endpoint="api.tutor.courses",
)
rest_api.add_resource(
    TutorCourseResource,
    "/tutors/<user_self:user>/courses/<tutor:tutor>/",
    endpoint="api.tutor.course",
)
rest_api.add_resource(
    TutorTutoringResource,
    "/tutors/<user_self:user>/tutorings/<tutoring:tutoring>/",
    endpoint="api.tutors.tutoring",
)
rest_api.add_resource(
    TutorTutoringAcceptResource,
    "/tutors/<user_self:user>/tutorings/<tutoring:tutoring>/accept/",
    endpoint="api.tutors.tutoring.accept",
)
# Company
rest_api.add_resource(
    CompanyResource, "/companies/<company:company>/", endpoint="api.company"
)
rest_api.add_resource(
    CompanyPublicResource,
    "/companies/<company:company>/public/",
    endpoint="api.company.public",
)
rest_api.add_resource(
    CompanyLogoResource,
    "/companies/<company:company>/logo/",
    endpoint="api.company_logo",
)
rest_api.add_resource(
    CompanyBannerResource,
    "/companies/<company:company>/banner/",
    endpoint="api.company_banner",
)
rest_api.add_resource(
    CompanyProfileIdResource,
    "/companies/<company:company>/profile/",
    endpoint="api.company_profile.id",
)
rest_api.add_resource(
    CompanyProfileSlugResource,
    "/companies/<company_path:company>/profile/",
    endpoint="api.company_profile.slug",
)
rest_api.add_resource(CompanyListResource, "/companies/", endpoint="api.companies")

# Company jobs
rest_api.add_resource(JobListResource, "/jobs/", endpoint="api.jobs")
rest_api.add_resource(JobContractsOfServices, "/jobs/types/", endpoint="api.jobs.types")
rest_api.add_resource(JobResource, "/jobs/<int:job_id>/", endpoint="api.job")
rest_api.add_resource(
    CompanyJobListResource,
    "/companies/<company:company>/jobs/",
    endpoint="api.company.jobs",
)
rest_api.add_resource(
    CompanyJobListResource,
    "/companies/<company_path:company>/jobs/",
    endpoint="api.company.jobs.slug",
)

# Banner (for footer)
rest_api.add_resource(BannerResource, "/banners/", endpoint="api.banners")

# Company pages
rest_api.add_resource(
    CompanyProfileListResource, "/profiles/", endpoint="api.companies.profilelist"
)

# Search
rest_api.add_resource(SearchResource, "/search/", endpoint="api.search")
rest_api.add_resource(
    SearchIndexResource, "/search/index/", endpoint="api.search.index"
)
rest_api.add_resource(UserAvatarResource, "/users/<int:user_id>/avatar/")

# User tfa
rest_api.add_resource(UserTfaResource, "/users/self/tfa/", endpoint="api.user.tfa")

# User role
rest_api.add_resource(UserRoleResource, "/users/self/role/", endpoint="api.user.role")

# User mailinglist subscriptions
rest_api.add_resource(
    UserMailingListSubscriptionResource,
    "/users/<user_self:user>/mailinglist-subscriptions/",
    endpoint="api.user.mailinglist_subscriptions",
)

# Photos
rest_api.add_resource(AlbumListResource, "/photos/", endpoint="api.albums")
rest_api.add_resource(AlbumResource, "/photos/<int:album_id>/", endpoint="api.album")
rest_api.add_resource(RandomPhotoResource, "/photos/random/", endpoint="api.photo")

# Pages
rest_api.add_resource(PageListResource, "/pages/", endpoint="api.pages")
rest_api.add_resource(PageResource, "/pages/<page:page>/", endpoint="api.page")
rest_api.add_resource(
    PageRevisionResource, "/pages/<int:page_id>/rev/", endpoint="api.page_revision.new"
)
rest_api.add_resource(
    PageRevisionResource,
    "/pages/<int:page_id>/rev/<int:revision_id>/",
    endpoint="api.page_revision",
)
rest_api.add_resource(
    PageRevisionResource,
    "/pages/<int:page_id>/rev/latest/",
    endpoint="api.page_revision.latest",
)
rest_api.add_resource(
    PagePreviewResource,
    "/pages/preview/",
    endpoint="api.page_preview",
)
rest_api.add_resource(
    PageRenderResource,
    "/pages/render/<string:lang>/<path:path>/",
    endpoint="api.page.preview",
)

# Pretix
rest_api.add_resource(
    PretixUserCheck, "/pretix/users/<string:fernet_token>/", endpoint="api.pretix.user"
)
rest_api.add_resource(PretixUserEvents, "/pretix/events/", endpoint="api.pretix.events")

# MailingList
rest_api.add_resource(
    MailingListResource,
    "/mailinglists/<int:mailinglist_id>/",
    endpoint="api.mailinglist",
)
rest_api.add_resource(
    MailingListListResource, "/mailinglists/", endpoint="api.mailinglists"
)

# Navigation
rest_api.add_resource(
    NavigationOrderResource, "/navigation/ordering/", endpoint="api.navigation.order"
)
rest_api.add_resource(
    NavigationResource,
    "/navigation/<navigation_entry:entry>/",
    endpoint="api.navigation",
)
rest_api.add_resource(
    NavigationListResource,
    "/navigation/",
    endpoint="api.navigations",
)

# Challenge
rest_api.add_resource(
    ChallengeRankingResource, "/challenges/ranking/", endpoint="api.challenges.ranking"
)
rest_api.add_resource(
    UserChallengeListResource, "/challenges/users/self/", endpoint="api.challenges.user"
)
rest_api.add_resource(
    ChallengeResource,
    "/challenges/<challenge:challenge>/",
    endpoint="api.challenge",
)
rest_api.add_resource(
    ChallengeListResource,
    "/challenges/",
    endpoint="api.challenges",
)
rest_api.add_resource(
    ChallengeSubmissionResource,
    "/challenges/<challenge:challenge>/submission/",
    # TODO Check if this does not change with Flask 2.0
    endpoint="api.challenges.submission",
)
rest_api.add_resource(
    ChallengeAdminSubmissionResource,
    # TODO Check if this does not change with Flask 2.0
    "/challenges/<challenge:challenge>/submission/admin/",
    endpoint="api.challenge_submission_admin",
)

# News
rest_api.add_resource(NewsListResource, "/news/", endpoint="api.news")
rest_api.add_resource(NewsResource, "/news/<news:news>/", endpoint="api.news_item")
rest_api.add_resource(
    NewsletterInitialResource,
    "/newsletters/initial/",
    endpoint="api.newsletter.initial",
)
rest_api.add_resource(
    NewsletterResource,
    "/newsletters/<newsletter:newsletter>/",
    endpoint="api.newsletter",
)
rest_api.add_resource(
    NewsletterListResource, "/newsletters/", endpoint="api.newsletters"
)

# Declaration
rest_api.add_resource(
    DeclarationUploadResource, "/declaration/upload/", endpoint="api.declaration.upload"
)
rest_api.add_resource(
    DeclarationResource, "/declaration/send/", endpoint="api.declaration.send"
)

rest_api.add_resource(CommitteeListResource, "/committees/", endpoint="api.committees")
rest_api.add_resource(
    CommitteeResource, "/committees/<committee:committee>/", endpoint="api.committee"
)
rest_api.add_resource(
    CommitteePictureResource,
    "/committees/<committee:committee>/picture/",
    endpoint="api.committees.picture",
)
rest_api.add_resource(
    CommitteeTagsResource, "/committees/tags/", endpoint="api.committees.tags"
)

# Redirects
rest_api.add_resource(RedirectListResource, "/redirect/", endpoint="api.redirects")
rest_api.add_resource(
    RedirectResource, "/redirect/<redirect:redirect>/", endpoint="api.redirect"
)

# Async Tasks
rest_api.add_resource(AsyncTasksResource, "/async/tasks/", endpoint="api.async.tasks")
rest_api.add_resource(
    AsyncTaskResource, "/async/tasks/<string:task_id>/", endpoint="api.async.task"
)
rest_api.add_resource(
    AsyncCopernicaSync, "/async/copernica/", endpoint="api.async.copernica"
)
rest_api.add_resource(
    AsyncDatanoseSync, "/async/datanose/", endpoint="api.async.datanose"
)

# DOMjudge
rest_api.add_resource(
    DOMjudgeContestListResource, "/domjudge/contests/", endpoint="api.domjudge.contests"
)
rest_api.add_resource(
    DOMjudgeContestResource,
    "/domjudge/contests/<int:contest_id>/",
    endpoint="api.domjudge.contest",
)
rest_api.add_resource(
    DOMjudgeBannerResource,
    "/domjudge/contests/<int:contest_id>/banner/",
    endpoint="api.domjudge.contest.banner",
)
rest_api.add_resource(
    DOMjudgeContestUserListResource,
    "/domjudge/contests/<int:contest_id>/users/",
    endpoint="api.domjudge.contest.users",
)
