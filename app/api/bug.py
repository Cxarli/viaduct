from typing import Literal

import pydantic
from authlib.integrations.flask_oauth2 import current_token
from flask import Response, jsonify, request
from flask.views import MethodView

from app.api.schema import TextAreaStr, schema_registry
from app.decorators import require_oauth
from app.exceptions.base import ValidationException
from app.service import gitlab_service


class BugReportView(MethodView):
    @schema_registry.register
    class BugReportRequest(pydantic.BaseModel):
        project: Literal["viaduct", "pretix", "pos"] = "viaduct"
        title: str
        description: TextAreaStr

    def get(self) -> str:
        return self.BugReportRequest.schema_json()

    @require_oauth()
    def post(self) -> Response:
        try:
            req_json = request.get_json(force=True)
            data = self.BugReportRequest(**req_json)
            return jsonify(
                gitlab_service.create_gitlab_issue(
                    project_name=data.project,
                    summary=data.title,
                    user=current_token.user,
                    description=data.description,
                )
            )
        except pydantic.ValidationError as e:
            raise ValidationException(e.errors())
