from typing import List

from authlib.integrations.flask_oauth2 import current_token
from flask_restful import Resource
from marshmallow import fields, pre_dump

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.exceptions.base import ResourceNotFoundException
from app.models.company import Company
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service, role_service


class CompanyModuleSchemaMixin:
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    company_id = fields.Integer(dump_only=True)
    active = fields.Boolean(dump_only=True)
    enabled = fields.Boolean(required=True)
    start_date = fields.Date(required=True)
    end_date = fields.Date(required=True)


class CompanySchema(RestSchema):
    id = fields.Integer(dump_only=True)

    name = fields.String(required=True)
    website = fields.URL(required=True)
    slug = fields.String(required=True)
    logo = fields.Boolean(dump_only=True)

    active = fields.Boolean(dump_only=True)

    contract_start_date = fields.Date()
    contract_end_date = fields.Date()

    @pre_dump
    def transform_fields(self, company, **kwargs):
        company.logo = bool(company.logo_file_id)
        return company

    @classmethod
    def get_list_schema(cls):
        return cls(many=True, only=("id", "name", "website", "slug"))


class CompanyPublicSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    slug = fields.String(dump_only=True)
    name = fields.String(dump_only=True)
    profile = fields.Boolean(dump_only=True)
    jobs = fields.List(fields.Integer, dump_only=True)


class CompanyResource(Resource):
    schema = CompanySchema()

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def get(self, company: Company):
        return self.schema.dump(company)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, company_update, company: Company):
        company = company_service.edit_company(
            company_id=company.id,
            name=company_update.get("name"),
            slug=company_update.get("slug"),
            website=company_update.get("website"),
            contract_start_date=company_update.get("contract_start_date"),
            contract_end_date=company_update.get("contract_end_date"),
        )
        return self.schema.dump(company)


class CompanyListResource(Resource):
    schema_get = CompanySchema.get_list_schema()
    schema_get_admin = CompanySchema(many=True)
    schema_post = CompanySchema()

    @require_oauth(Scopes.company)
    def get(self):
        is_admin = role_service.user_has_role(current_token.user, Roles.COMPANY_WRITE)

        # If the user is not an administrator, filter the inactive companies.
        if is_admin:
            companies = company_service.find_all_companies(filter_inactive=False)
            return self.schema_get_admin.dump(companies)
        companies = company_service.find_all_companies()
        return self.schema_get.dump(companies)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema_post)
    def post(self, company_details):
        company = company_service.create_company(
            name=company_details.get("name"),
            slug=company_details.get("slug"),
            website=company_details.get("website"),
            contract_start_date=company_details.get("contract_start_date"),
            contract_end_date=company_details.get("contract_end_date"),
        )
        return self.schema_post.dump(company)


class CompanyPublic:
    id: int
    slug: str
    name: str
    profile: bool
    jobs: List[int]

    def __init__(self, id: int, slug: str, name: str, profile: bool, jobs: List[int]):
        self.id = id
        self.slug = slug
        self.name = name
        self.profile = profile
        self.jobs = jobs


class CompanyPublicResource(Resource):
    schema_get = CompanyPublicSchema()

    def get(self, company: Company):
        slug = company.slug
        name = company.name

        try:
            company_service.get_company_profile_by_company_id(company.id)
            has_profile = True
        except ResourceNotFoundException:
            has_profile = False

        jobs = [
            job.id
            for job in company_service.find_company_jobs_by_company_id(company.id)
        ]

        return self.schema_get.dump(
            CompanyPublic(
                id=company.id, slug=slug, name=name, profile=has_profile, jobs=jobs
            )
        )
