from flask_restful import Resource
from marshmallow import fields

from app.api.schema import RestSchema
from app.service import user_service


class MemberOfMeritSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    first_name = fields.String(dump_only=True)
    last_name = fields.String(dump_only=True)
    member_of_merit_date = fields.Date(dump_only=True)
    path = fields.String(dump_only=True)


class MeritUserListResource(Resource):
    schema = MemberOfMeritSchema()

    def get(self):
        return self.schema.dump(user_service.find_member_of_merit(), many=True)
