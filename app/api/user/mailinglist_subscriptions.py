from typing import Any, Dict

from flask_restful import Resource
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth
from app.models.user import User
from app.service import mailinglist_service, user_mailing_service


class UserMailingListSubscriptionSchema(RestSchema):
    id = fields.Integer(required=True)
    nl_name = fields.Str(dump_only=True)
    en_name = fields.Str(dump_only=True)
    nl_description = fields.Str(dump_only=True)
    en_description = fields.Str(dump_only=True)
    members_only = fields.Bool(dump_only=True)
    subscribed = fields.Bool(required=True)


class UserMailingListSubscriptionResource(Resource):
    schema_get = UserMailingListSubscriptionSchema(many=True)
    schema_post = UserMailingListSubscriptionSchema()

    @require_oauth()
    def get(self, user: User):
        subscribed_mailinglists = set(
            ml.id for ml in user_mailing_service.get_user_subscribed_mailing_lists(user)
        )

        mailinglists = self.schema_get.dump(mailinglist_service.get_all_mailinglists())

        for ml in mailinglists:
            ml["subscribed"] = ml["id"] in subscribed_mailinglists

        return mailinglists

    @require_oauth()
    @json_schema(schema_post)
    def post(self, data: Dict[str, Any], user: User):
        mailinglist = mailinglist_service.get_mailinglist(data["id"])

        task = user_mailing_service.set_mailing_list_subscribed(
            user, mailinglist, data["subscribed"]
        )
        return {"task_id": task.id}
