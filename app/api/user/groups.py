from flask_restful import Resource

from app.api.group.group import GroupSchema
from app.decorators import require_oauth
from app.models.user import User
from app.oauth_scopes import Scopes
from app.service import group_service


class UserGroupsResource(Resource):
    schema_get = GroupSchema.get_list_schema()

    @require_oauth(Scopes.user)
    def get(self, user: User):
        groups = group_service.get_groups_for_user(user)
        return self.schema_get.dump(groups)
