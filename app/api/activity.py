from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response, request
from flask_restful import Resource
from marshmallow import fields

from app.api.schema import (
    AutoMultilangStringField,
    PageSearchParameters,
    PaginatedResponseSchema,
    PaginatedSearchSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.activity import Activity
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import activity_service
from app.service.activity_service import ActivityApiData


class ActivitySchema(RestSchema):
    id = fields.Int(dump_only=True)
    name = AutoMultilangStringField(required=True)
    description = AutoMultilangStringField(required=True)
    start_time = fields.DateTime(required=True)
    end_time = fields.DateTime(required=True)
    location = fields.Str(required=True)
    price = fields.Str(required=True)
    form_id = fields.Int()
    picture = fields.Boolean(dump_only=True)
    pretix_event_slug = fields.String()


class ActivityResource(Resource):
    """Resource to retrieve activity, focused on Pretix based activities."""

    schema = ActivitySchema()

    @require_oauth(Scopes.activity)
    def get(self, activity: Activity):
        return self.schema.dump(activity)

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @json_schema(schema)
    def put(self, data: ActivityApiData, activity: Activity):
        activity_service.edit_activity_from_pretix(activity, data)
        return Response(status=HTTPStatus.NO_CONTENT)


class ActivityListResource(Resource):
    schema = ActivitySchema()
    schema_get = PaginatedResponseSchema(ActivitySchema(many=True))
    schema_search = PaginatedSearchSchema()

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @query_parameter_schema(schema_search)
    def get(self, pagination: PageSearchParameters):
        return self.schema_get.dump(
            activity_service.paginated_search_all_activities(pagination)
        )

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @json_schema(schema)
    def post(self, data: ActivityApiData):
        activity = activity_service.create_pretix_activity_from_pretix(
            data, current_token.user
        )
        return self.schema.dump(activity)


class ActivityPictureResource(Resource):
    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def put(self, activity: Activity):
        if "file" not in request.files:
            return Response(status=HTTPStatus.BAD_REQUEST)

        activity_service.set_activity_picture(activity, request.files["file"])
        return Response(status=HTTPStatus.NO_CONTENT)
