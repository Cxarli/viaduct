from http import HTTPStatus

from flask import Response, jsonify
from flask_restful import Resource
from marshmallow import fields

from app import app
from app.api.activity import ActivitySchema
from app.api.news import NewsSchema
from app.api.schema import MultilangStringField, RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.newsletter import Newsletter
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import news_service


def newsletter_item_schema_factory(obj_schema):
    class NewsletterItemSchema(RestSchema):
        obj = fields.Nested(obj_schema, required=True)
        overwrite_text = MultilangStringField()

    return NewsletterItemSchema


class NewsletterItemRequestSchema(RestSchema):
    item_id = fields.Integer()
    overwrite_text = MultilangStringField()


class NewsletterResponseSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.DateTime(dump_only=True)
    news = fields.Nested(newsletter_item_schema_factory(NewsSchema), many=True)
    activities = fields.Nested(
        newsletter_item_schema_factory(ActivitySchema), many=True
    )


class NewsletterRequestSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    news = fields.Nested(NewsletterItemRequestSchema, many=True)
    activities = fields.Nested(NewsletterItemRequestSchema, many=True)


class NewsletterInitialResource(Resource):
    schema = NewsletterResponseSchema(only=("news", "activities"))

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def get(self):
        activities, news_items = news_service.get_initial_newsletter_items()
        return self.schema.dump(
            {
                "news": [
                    {
                        "obj": n,
                        "overwrite_text": {
                            "nl": "",
                            "en": "",
                        },
                    }
                    for n in news_items
                ],
                "activities": [
                    {
                        "obj": a,
                        "overwrite_text": {
                            "nl": "",
                            "en": "",
                        },
                    }
                    for a in activities
                ],
            }
        )


class NewsletterResource(Resource):
    schema = NewsletterResponseSchema()

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def get(self, newsletter: Newsletter):
        return self.schema.dump(
            {
                "id": newsletter.id,
                "created": newsletter.created,
                "news": [
                    {
                        "obj": n.news_item,
                        "overwrite_text": {
                            "nl": n.nl_description,
                            "en": n.en_description,
                        },
                    }
                    for n in newsletter.news_items
                ],
                "activities": [
                    {
                        "obj": a.activity,
                        "overwrite_text": {
                            "nl": a.nl_description,
                            "en": a.en_description,
                        },
                    }
                    for a in newsletter.activities
                ],
            }
        )

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    @json_schema(NewsletterRequestSchema())
    def put(self, data, newsletter):
        news_service.update_newsletter(newsletter, data["news"], data["activities"])

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def delete(self, newsletter):
        news_service.delete_newsletter(newsletter)
        return Response(status=HTTPStatus.NO_CONTENT)


class NewsletterListResource(Resource):
    schema = NewsletterResponseSchema()

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def get(self):
        newsletters = news_service.get_all_newsletters()
        return jsonify(
            {
                "auth_token": app.config["COPERNICA_NEWSLETTER_TOKEN"],
                "entries": [
                    {
                        "id": n.id,
                        "date": n.created.isoformat(),
                        "activities": [
                            {
                                "id": a.activity_id,
                                "title": {
                                    "en": a.activity.en_name,
                                    "nl": a.activity.nl_name,
                                },
                            }
                            for a in n.activities
                        ],
                        "news_items": [
                            {
                                "id": ni.news_id,
                                "title": {
                                    "en": ni.news_item.en_title,
                                    "nl": ni.news_item.nl_title,
                                },
                            }
                            for ni in n.news_items
                        ],
                    }
                    for n in newsletters
                ],
            }
        )

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    @json_schema(NewsletterRequestSchema())
    def post(self, data):
        newsletter = news_service.create_newsletter(data["news"], data["activities"])
        return self.schema.dump(
            {
                "id": newsletter.id,
                "news": [n.news_item for n in newsletter.news_items],
                "activities": [a.activity for a in newsletter.activities],
            }
        )
