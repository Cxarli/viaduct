from authlib.integrations.flask_oauth2 import current_token
from flask_restful import Resource
from marshmallow import ValidationError, fields, validate, validates

from app.api.group.group import GroupSchema
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.group import Group
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import group_service, pimpy_service


class MinuteSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    minute_date = fields.Date(data_key="date", required=True)
    content = fields.String(validate=validate.Length(min=1), required=True)
    group_id = fields.Integer(required=True)

    @validates("content")
    def validate_empty_content(self, value):
        if "\n".join(value).strip() == "":
            raise ValidationError("Minute content is empty")


class AllMinutesSchema(RestSchema):
    group = fields.Nested(GroupSchema)
    minutes = fields.Nested(MinuteSchema, many=True)


class MinuteResource(Resource):
    schema_get = MinuteSchema()

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self, minute):
        pimpy_service.check_user_can_access_minute(current_token.user, minute)

        return self.schema_get.dump(minute)


class MinuteListResource(Resource):
    schema_get = AllMinutesSchema(many=True)

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self):
        minutes = pimpy_service.get_all_minutes_for_user(current_token.user)
        return self.schema_get.dump(minutes)


class GroupMinuteResource(Resource):
    schema_get = AllMinutesSchema(many=True)
    schema_post = MinuteSchema()

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_READ)
    def get(self, group):
        group_service.check_user_member_of_group(current_token.user, group.id)

        minutes = pimpy_service.get_minutes_for_group(group)

        return self.schema_get.dump([{"group": group, "minutes": minutes}])

    @require_oauth(Scopes.pimpy)
    @require_role(Roles.PIMPY_WRITE)
    @json_schema(schema_post)
    def post(self, new_minute: dict, group: Group):
        group_service.check_user_member_of_group(current_token.user, group.id)
        minute = pimpy_service.add_minute(
            content=new_minute["content"],
            date=new_minute["minute_date"],
            group=group,
        )

        return self.schema_post.dump(minute), 201
