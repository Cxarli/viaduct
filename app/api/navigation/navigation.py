import logging
from http import HTTPStatus
from typing import List, Tuple

from flask import Response, request
from flask_restful import Resource
from marshmallow import fields, pre_dump, validate

from app.api.page.page import PageSchema
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.navigation import NavigationEntry
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import navigation_service
from app.service.navigation_service import NavigationChildren

_logger = logging.getLogger(__name__)


class NavigationOrderSchema(RestSchema):
    id = fields.Integer()
    nl_title = fields.Str(required=True, dump_only=True)
    en_title = fields.Str(
        validate=validate.Length(max=64), required=True, dump_only=True
    )
    href = fields.Str(validate=validate.Length(max=64), required=True, dump_only=True)
    children = fields.Nested("self", many=True, attribute="children_data")

    @pre_dump(pass_many=True)
    def load_children(
        self, in_data: Tuple[List[NavigationEntry], NavigationChildren], **kwargs
    ):
        """
        This pre-processor sets the children from the ordering dict.

        This Schema unpacks the list of entries and ordering, it will select the
        children of the all the entries from the ordering dict. The children and the
        ordering together are set on the children_loaded attribute for the nested
        recursion of this schema.

        Only the return value will be used for the actual schema, therefore we do not
        return the ordering.
        """
        entries, ordering = in_data
        for entry in entries:
            entry.children_data = (ordering[entry.id], ordering)  # type: ignore
        return entries


class NavigationSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    nl_title = fields.Str(validate=validate.Length(max=64), required=True)
    en_title = fields.Str(validate=validate.Length(max=64), required=True)
    type = fields.Str(validate=validate.OneOf(("page", "url", "activities")))
    url = fields.Str(validate=validate.Length(max=255))
    external = fields.Bool(default=False)
    order_children_alphabetically = fields.Bool(default=False)
    page = fields.Nested(PageSchema(), dump_only=True)
    page_id = fields.Integer(load_only=True, default=None)

    @pre_dump
    def set_type(self, in_data: NavigationEntry, **kwargs):
        if in_data.activity_list:
            in_data.type = "activities"  # type: ignore
        elif in_data.page:
            in_data.type = "page"  # type: ignore
        elif in_data.url:
            in_data.type = "url"  # type: ignore
        else:
            _logger.error(f"Could not determine navigation entry type: {in_data}")
        return in_data


class NavigationOrderResource(Resource):
    schema = NavigationOrderSchema(many=True)

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    def get(self):
        root, ordering = navigation_service.get_navigation_ordering()
        return self.schema.dump((root, ordering))

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    @json_schema(schema)
    def put(self, entries):
        navigation_service.save_navigation_entries(entries, None)
        return Response(status=HTTPStatus.NO_CONTENT)


class NavigationListResource(Resource):
    schema = NavigationSchema()

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    @json_schema(schema)
    def post(self, entry):
        return self.schema.dump(
            navigation_service.create_entry(
                type=entry["type"],
                nl_title=entry["nl_title"],
                en_title=entry["en_title"],
                parent_id=request.args.get("parentId"),
                page_id=entry.get("page_id"),
                url=entry.get("url"),
                external=entry.get("external"),
                order_children_alphabetically=entry.get(
                    "order_children_alphabetically"
                ),
            )
        )


class NavigationResource(Resource):
    schema = NavigationSchema()

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    def get(self, entry):
        return self.schema.dump(entry)

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    @json_schema(schema)
    def put(self, data, entry):
        return self.get(
            navigation_service.update_entry(
                entry=entry,
                type=data["type"],
                nl_title=data["nl_title"],
                en_title=data["en_title"],
                page_id=data.get("page_id"),
                url=data.get("url"),
                external=data.get("external"),
                order_children_alphabetically=data.get("order_children_alphabetically"),
            )
        )

    @require_oauth(Scopes.navigation)
    @require_role(Roles.NAVIGATION_WRITE)
    def delete(self, entry):
        navigation_service.delete_navigation_entry(
            entry, remove_page=request.args.get("include_page", False)
        )
        return Response(status=HTTPStatus.NO_CONTENT)
