from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response
from flask_restful import Resource
from marshmallow import ValidationError, fields, validate, validates_schema

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth
from app.oauth_scopes import Scopes
from app.repository import model_service
from app.service import group_service, meeting_service
from app.views.meeting import require_access_meeting


class MeetingSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True, validate=validate.Length(max=512))
    description = fields.String(validate=validate.Length(max=2048), missing="")
    group_id = fields.Integer(required=True)

    start_time = fields.DateTime(required=True)

    # End time will be set to 1 hour after start time, when missing.
    end_time = fields.DateTime()

    location = fields.String(validate=validate.Length(max=64), missing="")
    cancelled = fields.Boolean()

    @validates_schema
    def validate_timerange(self, data, **kwargs):
        # If start and ending time are the same, configure_end_time() (in
        # meeting_service) will automatically set the end time one hour later
        if data.get("end_time") and data["start_time"] > data["end_time"]:
            raise ValidationError({"start_time": "Start time must be before end time"})


class MeetingResource(Resource):
    schema = MeetingSchema()

    @require_oauth(Scopes.meeting)
    @require_access_meeting
    def get(self, meeting):
        return self.schema.dump(meeting)

    @require_oauth(Scopes.meeting)
    @require_access_meeting
    @json_schema(schema)
    def put(self, data, meeting):
        meeting_service.update(
            meeting,
            group_id=data["group_id"],
            name=data["name"],
            cancelled=data["cancelled"],
            start_time=data["start_time"],
            end_time=data.get("end_time"),
            location=data["location"],
            description=data["description"],
        )
        return self.schema.dump(meeting)

    @require_oauth(Scopes.meeting)
    @require_access_meeting
    def delete(self, meeting):
        model_service.delete(meeting)
        return Response(status=HTTPStatus.NO_CONTENT)


class MeetingListResource(Resource):
    schema = MeetingSchema()

    @require_oauth(Scopes.meeting)
    @json_schema(schema)
    def post(self, data):
        group_id = data["group_id"]
        group_service.check_user_member_of_group(current_token.user, group_id)
        meeting = meeting_service.add(
            group_id=group_id,
            name=data["name"],
            cancelled=data["cancelled"],
            start_time=data["start_time"],
            end_time=data.get("end_time"),
            location=data["location"],
            description=data["description"],
        )
        return self.schema.dump(meeting), 201
