from http import HTTPStatus
from typing import Optional, Dict, Any

from flask import request, Response
from flask_restful import Resource
from marshmallow import fields, validate

from app.api.schema import RestSchema
from app.decorators import require_oauth, require_role, json_schema
from app.exceptions.base import ValidationException
from app.models.domjudge import DOMjudgeContestSettings
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import domjudge_service
from app.service.domjudge_service import Contest


class DOMjudgeContestSchema(RestSchema):
    id = fields.Integer()
    name = fields.String(dump_only=True)
    banner_file_id = fields.Integer(dump_only=True, default=None)
    banner_url = fields.String(
        default=None, allow_none=True, validate=validate.Length(max=512)
    )


class DOMjudgeContestUserSchema(RestSchema):
    id = fields.Integer()
    team_id = fields.Integer()
    name = fields.String()
    email = fields.Email(dump_only=True)


def _create_contest_data(
    contest: Contest, settings: Optional[DOMjudgeContestSettings] = None
) -> Dict[str, Any]:
    data = dict(id=contest.id, name=contest.name)
    if settings:
        data["banner_file_id"] = settings.banner_file_id
        data["banner_url"] = settings.banner_url

    return data


class DOMjudgeContestListResource(Resource):
    schema = DOMjudgeContestSchema(many=True)

    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    def get(self):
        contests = domjudge_service.get_all_contests()
        contest_settings = domjudge_service.get_all_contest_settings()

        data_list = []
        for contest in contests:
            contest_data = _create_contest_data(
                contest, contest_settings.get(contest.id)
            )
            data_list.append(contest_data)

        return self.schema.dump(data_list)


class DOMjudgeContestResource(Resource):
    schema_get = DOMjudgeContestSchema()
    schema_patch = DOMjudgeContestSchema(partial=True)

    def _dump_contest(self, contest: Contest) -> Dict[str, Any]:
        contest_settings = domjudge_service.get_contest_settings(contest)
        contest_data = _create_contest_data(contest, contest_settings)
        return self.schema_get.dump(contest_data)

    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    def get(self, contest_id: int):
        contest = domjudge_service.get_contest_by_id(contest_id)

        return self._dump_contest(contest)

    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    @json_schema(schema_patch)
    def patch(self, contest_update: Dict[str, Any], contest_id: int):
        contest = domjudge_service.get_contest_by_id(contest_id)

        domjudge_service.save_contest_settings(
            contest, banner_url=contest_update.get("banner_url")
        )

        return self._dump_contest(contest)


class DOMjudgeBannerResource(Resource):
    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    def put(self, contest_id: int):
        if "file" not in request.files:
            raise ValidationException("File is missing.")

        contest = domjudge_service.get_contest_by_id(contest_id)
        domjudge_service.set_contest_banner(contest, request.files["file"])

        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    def delete(self, contest_id: int):
        contest = domjudge_service.get_contest_by_id(contest_id)
        domjudge_service.delete_contest_banner(contest)

        return Response(status=HTTPStatus.NO_CONTENT)


class DOMjudgeContestUserListResource(Resource):
    schema = DOMjudgeContestUserSchema(many=True)

    @require_oauth(Scopes.domjudge)
    @require_role(Roles.DOMJUDGE_ADMIN)
    def get(self, contest_id: int):
        contest = domjudge_service.get_contest_by_id(contest_id)
        teams = domjudge_service.get_contest_teams(contest)
        scoreboard = domjudge_service.get_contest_scoreboard(
            contest, include_rows_without_submissions=False
        )
        scoreboard_team_ids = {row.team_id for row in scoreboard}
        users = [
            dict(
                id=team.via_user.id,
                team_id=team.id,
                name=team.via_user.name,
                email=team.via_user.email,
            )
            for team in teams
            if team.via_user is not None and team.id in scoreboard_team_ids
        ]

        return self.schema.dump(users)
