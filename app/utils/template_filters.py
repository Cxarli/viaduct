from datetime import date, datetime
from typing import Union, Optional, Literal
from urllib.parse import quote_plus

from flask import Markup
from jinja2 import contextfilter
from pytz import timezone

from app import constants
from app.service.domjudge_service import color_to_rgb
from app.service.markdown_service import (
    render_markdown_safely,
    render_markdown_summarized_safely,
)
from app.utils.markdown_summary import SummaryOptions


def register_filters(app):
    app.add_template_filter(quote_plus, "quote_plus")

    @app.template_filter("markdown")
    def markdown(markdown_content: str):
        return Markup(render_markdown_safely(markdown_content))

    @app.template_filter("markdown_escaped")
    def markdown_escaped(markdown_content: str):
        """Renders the markdown without marking the html as safe for Jinja.

        This is useful for usage inside of xml."""
        return render_markdown_safely(markdown_content)

    @app.template_filter("markdown_summary")
    def markdown_summary(
        markdown_content,
        max_length: int,
        truncate_method: Literal["hard", "word", "paragraph"] = "hard",
        read_more: str = "",
    ):
        """Summarized markdown without marking the html as safe for Jinja."""
        options = SummaryOptions(max_length, truncate_method, read_more)
        return Markup(render_markdown_summarized_safely(markdown_content, options))

    @app.template_filter("date")
    def date_filter(dt: Union[datetime, date], dt_format=None):
        if dt is None:
            return ""
        if isinstance(dt, datetime):
            tz = timezone("Europe/Amsterdam")
            dt = dt.astimezone(tz)
        if dt_format is None:
            dt_format = constants.DATE_FORMAT
        return dt.strftime(dt_format)

    @app.template_filter("datetime")
    def datetime_filter(dt: datetime, dt_format=None):
        if dt is None:
            return ""
        tz = timezone("Europe/Amsterdam")
        local_dt = dt.astimezone(tz)
        if dt_format is None:
            dt_format = constants.DT_FORMAT
        return local_dt.strftime(dt_format)

    @app.template_filter("call_macro")
    @contextfilter
    def call_macro_filter(context, field, macro_name, *args, **kwargs):
        return context.get_all()[macro_name](field, *args, **kwargs)

    @app.template_filter("domjudge_problem_darken_color")
    def domjudge_problem_darken_color(color: Optional[str]) -> str:
        """
        Darken a color.

        Small utility function to compute the color for the border of
        the problem name badges.

        When no color is passed, a dark border is set.
        """
        rgb = color_to_rgb(color)
        if not rgb:
            return "#000"  # black

        r, g, b = rgb
        return "#{:0>6s}".format(
            hex(int(r * 0.5) << 16 | int(g * 0.5) << 8 | int(b * 0.5))[2:]
        )

    @app.template_filter("domjudge_problem_text_color")
    def domjudge_problem_text_color(color: Optional[str]) -> str:
        rgb = color_to_rgb(color)
        if not rgb:
            return "#fff"  # white

        r, g, b = rgb

        # Compute brightness using formula from:
        # https://www.w3.org/TR/AERT/#color-contrast
        brightness = 1 - (0.299 * r + 0.587 * g + 0.144 * b) / 255

        if brightness < 0.5:
            # Bright color, use black for text
            return "#000"
        else:
            # Dark color, use white for text
            return "#fff"
