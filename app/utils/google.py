import base64
import logging
import mimetypes
from datetime import datetime, timedelta
from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any, Optional

from flask import flash
from google.oauth2 import service_account
from googleapiclient import errors
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from app import app, constants
from app.task.mail import MailCommand

domain = "svia.nl"
info_email = "info@svia.nl"
_logger = logging.getLogger(__name__)


class GoogleApiException(Exception):
    pass


def build_service(service_type, api_version, scope, email) -> Optional[Any]:
    if app.debug or app.testing:
        _logger.warning("Not enabling google group api in debug or testing modus")
        return None

    try:
        # https://developers.google.com/identity/protocols/OAuth2ServiceAccount
        credentials = service_account.Credentials.from_service_account_file(
            filename=constants.GOOGLE_API_KEY, scopes=[scope]
        ).with_subject(email)

        return build(
            service_type, api_version, credentials=credentials, cache_discovery=False
        )
    except Exception as e:
        _logger.error(e, exc_info=True)
        raise


def build_user_service():
    return build_service(
        "admin",
        "directory_v1",
        "https://www.googleapis.com/auth/admin.directory.user.readonly",
        "bestuur@svia.nl",
    )


def build_calendar_service():
    return build_service(
        "calendar", "v3", "https://www.googleapis.com/auth/calendar", "bestuur@svia.nl"
    )


def build_groups_service():
    return build_service(
        "admin",
        "directory_v1",
        ("https://www.googleapis.com" "/auth/admin.directory.group"),
        "bestuur@svia.nl",
    )


def build_gmail_service():
    return build_service(
        "gmail", "v1", "https://www.googleapis.com/auth/gmail.send", info_email
    )


# Provide a calendar_id
def insert_activity(
    title: str, description: str, location: str, start: datetime, end: datetime
):
    calendar_id = app.config["GOOGLE_CALENDAR_ID"]

    service = build_calendar_service()

    if service:
        # All day events require only the date part.
        start_field = {"timeZone": "Europe/Amsterdam"}
        end_field = {"timeZone": "Europe/Amsterdam"}
        if end - start >= timedelta(days=1):
            start_field["date"] = start.date().isoformat()
            # End field date is exclusive.
            end_field["date"] = (end.date() + timedelta(days=1)).isoformat()
        else:
            start_field["dateTime"] = start.isoformat()
            end_field["dateTime"] = end.isoformat()

        # Event to insert
        event = {
            "summary": title,
            "description": description,
            "location": location,
            "start": start_field,
            "end": end_field,
        }

        try:
            return service.events().insert(calendarId=calendar_id, body=event).execute()
        except Exception as e:
            _logger.error(e, exc_info=True)
            flash(
                "Er ging iets mis met het toevogen van het event aan de"
                "Google Calender"
            )
    _logger.warning("Google calendar service not available.")
    return None


def update_activity(
    event_id: str,
    title: str,
    description: str,
    location: str,
    start: datetime,
    end: datetime,
):
    calendar_id = app.config["GOOGLE_CALENDAR_ID"]
    service = build_calendar_service()

    if service:
        # All day events require only the date part.
        start_field = {"timeZone": "Europe/Amsterdam"}
        end_field = {"timeZone": "Europe/Amsterdam"}
        if end - start >= timedelta(days=1):
            start_field["date"] = start.date().isoformat()
            # End field date is exclusive.
            end_field["date"] = (end.date() + timedelta(days=1)).isoformat()
        else:
            start_field["dateTime"] = start.isoformat()
            end_field["dateTime"] = end.isoformat()

        # Event to update
        event = {
            "summary": title,
            "description": description,
            "location": location,
            "start": start_field,
            "end": end_field,
        }

        try:
            service.events().update(
                calendarId=calendar_id, eventId=event_id, body=event
            ).execute()
        except Exception as e:
            _logger.error(e, exc_info=True)
            insert_activity(title, description, location, start, end)
    else:
        _logger.warning("Google calendar service not available.")


def delete_activity(event_id):
    calendar_id = app.config["GOOGLE_CALENDAR_ID"]
    service = build_calendar_service()

    if service:
        try:
            service.events().delete(calendarId=calendar_id, eventId=event_id).execute()
        except Exception as e:
            _logger.error(e, exc_info=True)
            flash(
                "Er ging iets mis met het verwijderen van het event uit de"
                "Google Calender, het kan zijn dat ie al verwijderd was"
            )
    else:
        _logger.warning("Google calendar service not available.")


def create_group(groupname, listname):
    service = build_groups_service()
    if not service:
        return

    email = listname + "@" + domain
    service.groups().insert(body={"email": email, "name": groupname}).execute()


def create_group_if_not_exists(groupname, listname):
    try:
        create_group(groupname, listname)
    except HttpError as e:
        if e.resp.status != 409:
            # Something else went wrong than the list already existing
            raise e


def send_email(command: MailCommand):
    # https://developers.google.com/gmail/api/guides/sending
    service = build_gmail_service()
    if not service:
        _logger.warning("Google gmail service not available.")
        return

    mime_document = MIMEMultipart()
    mime_document["To"] = command.to
    mime_document["From"] = command.sender
    mime_document["Subject"] = command.subject
    mime_document["reply-to"] = command.reply_to

    text_document = MIMEText(command.html, "html")
    mime_document.attach(text_document)

    for cid, filepath in command.added_inline_attachments:
        mime_document.attach(
            _get_mime_document_for_file(cid, filepath=filepath, inline=True)
        )

    for name, filepath in command.added_attachments:
        mime_document.attach(
            _get_mime_document_for_file(name, filepath=filepath, inline=False)
        )

    for name, content, cont_type in command.loaded_attachments:
        mime_document.attach(
            _get_mime_document_for_file(
                name, content=content, content_type=cont_type, inline=False
            )
        )

    # Gmail requires a raw field with the base64url encoded document.
    message = {"raw": base64.urlsafe_b64encode(mime_document.as_bytes()).decode()}

    try:
        email = (
            service.users().messages().send(userId=info_email, body=message).execute()
        )
        _logger.info("Email sent to '%s': '%s'", command.to, command.subject)
        return email
    except errors.HttpError as e:
        _logger.error(e, exc_info=True)
        raise GoogleApiException() from e


def _get_mime_document_for_file(
    name, filepath=None, content=None, content_type=None, inline=False
):
    """Get the mime document that can be attached to the main document."""

    if filepath is not None:
        content_type, encoding = mimetypes.guess_type(filepath)

        if content_type is None or encoding is not None:
            content_type = "application/octet-stream"

    main_type, sub_type = content_type.split("/", 1)

    if content is None:
        with open(filepath, "rb") as fp:
            content = fp.read()

    if main_type == "text":
        message = MIMEText(content, sub_type)
    elif main_type == "image":
        message = MIMEImage(content, sub_type)
    elif main_type == "audio":
        message = MIMEAudio(content, sub_type)
    else:
        message = MIMEBase(main_type, sub_type)
        message.set_payload(content)
        encoders.encode_base64(message)

    # Reference the name in the Content-ID header.
    # The email must have an <img> tag with the src set to 'cid:name_here'
    # for it to be inlined.
    if inline:
        message.add_header("Content-ID", "<{}>".format(name))
    else:
        message.add_header("Content-Disposition", "attachment", filename=name)

    return message
