from datetime import datetime
from subprocess import check_output


class GitVersion:
    def __init__(self):
        self.hash = self.get_value("git rev-parse --short=12 HEAD")
        self.timestamp = datetime.strptime(
            self.get_value("git show -s --format=%ci HEAD"), "%Y-%m-%d %H:%M:%S %z"
        )

    @staticmethod
    def get_value(command):
        return check_output(command, shell=True).decode().strip()
