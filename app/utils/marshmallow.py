import datetime as dt
from functools import partial
from typing import (
    Any,
    Callable,
    Dict,
    Generic,
    Iterable,
    Optional,
    Type,
    TypeVar,
    Union,
    get_args,
    get_origin,
    get_type_hints,
)

import marshmallow
from marshmallow import fields, post_load
from marshmallow.fields import Field

from app.api.schema import RestSchema

DictStr = Dict[str, Any]
T = TypeVar("T")
U = TypeVar("U")


class AbstractSchemaWrapper(RestSchema, Generic[T]):
    _type_constructor: Callable[..., T]

    @post_load
    def _schema_dict_to_object(self, data: DictStr, **kwargs: Any) -> T:
        return self._type_constructor(**data)


def _get_optional_type(typehint: Type[Any]) -> Optional[Type[U]]:
    origin = get_origin(typehint)
    args = get_args(typehint)

    if not (origin is Union and len(args) == 2 and type(None) in args):
        return None

    optional_types = [
        t for t in get_args(typehint) if t is not type(None)  # noqa: E721
    ]
    return optional_types[0]


def _get_list_type(typehint: Type[Any]) -> Optional[Type[U]]:
    origin = get_origin(typehint)

    if origin is not list:
        return None

    args = get_args(typehint)
    return args[0]


def _create_field(typehint: Type[T], default_value: Optional[Any] = None) -> Field:
    optional_type: Optional[Type[Any]] = _get_optional_type(typehint)
    is_optional_type = optional_type is not None
    has_default_value = default_value is not None

    if optional_type:
        typehint = optional_type

    required = not is_optional_type and not has_default_value
    allow_none = is_optional_type

    if has_default_value:
        missing = default_value
    elif is_optional_type:
        missing = None
    else:
        missing = marshmallow.missing

    list_type: Optional[Type[Any]] = _get_list_type(typehint)

    field_constructor: Callable[..., Field]

    if list_type:
        inner_field = _create_field(list_type)
        field_constructor = partial(fields.List, inner_field)
    elif typehint is str:
        field_constructor = fields.Str
    elif typehint is int:
        field_constructor = fields.Int
    elif typehint is float:
        field_constructor = fields.Float
    elif typehint is bool:
        field_constructor = fields.Bool
    elif typehint is dt.date:
        field_constructor = fields.Date
    elif typehint is dt.datetime:
        field_constructor = fields.DateTime
    else:
        hints = get_type_hints(typehint)
        if not hints:
            raise TypeError(f"Unsupported type for converting NamedTuple: {typehint}")

        inner_schema = create_schema_from_named_tuple(
            typehint, f"{typehint.__name__}Schema"
        )
        field_constructor = partial(fields.Nested, inner_schema)

    return field_constructor(required=required, allow_none=allow_none, missing=missing)


def create_schema_from_named_tuple(
    named_tuple_class: Type[T],
    name: Optional[str] = None,
    ignore_fields: Optional[Iterable[str]] = None,
) -> Type[AbstractSchemaWrapper[T]]:

    ignore_fields_set = set(ignore_fields) if ignore_fields else {}
    if not name:
        name = f"{named_tuple_class.__name__}Schema"

    # Only NamedTuple has _field_defaults
    if hasattr(named_tuple_class, "_field_defaults"):
        # _field_defaults is documented, so we're allowed to use it
        field_defaults = named_tuple_class._field_defaults  # type: ignore
    else:
        field_defaults = {}

    schema_dict: Dict[str, Union[Field, Any]] = {
        field_name: _create_field(field_type, field_defaults.get(field_name))
        for field_name, field_type in get_type_hints(named_tuple_class).items()
        if field_name not in ignore_fields_set
    }

    schema_cls: Any = RestSchema.from_dict(schema_dict, name=name)

    class SchemaWrapper(AbstractSchemaWrapper[T], schema_cls):
        _type_constructor = named_tuple_class

    return SchemaWrapper[named_tuple_class]  # type: ignore
