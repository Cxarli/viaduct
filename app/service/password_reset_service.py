import random
import string
from datetime import datetime, timezone

from app.exceptions.base import ResourceNotFoundException
from app.repository import password_reset_repository
from app.service import mail_service, user_service
from app.task.mail import MailCommand


def build_hash(n):
    available_characters = string.ascii_letters + string.digits
    return "".join(random.choice(available_characters) for _ in range(n))


def get_valid_ticket(hash_):
    ticket = password_reset_repository.find_password_ticket_by_hash(hash_)
    now = datetime.now(timezone.utc)
    if (
        ticket is None
        or (now - ticket.created).seconds > 3600
        or ticket.used_at is not None
    ):
        raise ResourceNotFoundException("password reset ticket", hash_)
    return ticket


def reset_password(ticket, new_password):
    ticket.used_at = datetime.now()
    password_reset_repository.save(ticket)

    return user_service.set_password(ticket.user_id, new_password)


def create_password_ticket(email, reset_link):
    user = user_service.get_user_by_email(email)

    hash_ = build_hash(20)

    ticket = password_reset_repository.create_password_ticket()
    ticket.user_id = user.id
    ticket.hash = hash_

    password_reset_repository.save(ticket)

    command = MailCommand(to=user.email)
    command.with_template(
        "forgot_password", user.locale, user=user, reset_link=reset_link + hash_
    )

    mail_service.send_mail(command)
