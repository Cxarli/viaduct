from typing import Any, Dict, Optional, List, Tuple, TypedDict

from TexSoup.data import TexArgs
from werkzeug.datastructures import FileStorage
from zipfile import ZipFile
from enum import Enum
from TexSoup import TexSoup, TexNode
import re


class AlvEvent(Enum):
    DECISION = 1
    TASK = 2
    SECTION = 3  # Nieuw agendapunt
    SUBSECTION = 4
    TIMESTAMP = 5  # Bv. schorsing, heropening, binnenkomst persoon
    VOTE = 6
    PARSE_WARNING = 7  # Er mist iets in de notulen

    def __str__(self):
        return self.name.lower()


class AlvMetaData(TypedDict, total=False):
    #  All are optional since the minutes can omit these commands
    title: Optional[str]
    chairman: Optional[str]
    secretary: Optional[str]
    start_time: Optional[str]
    stop_time: Optional[str]
    location: Optional[str]

    #  Aanwezigheidslijst
    present: List[str]  #
    #  "Machtigingen"
    proxies: List[Tuple[str, str]]


class AlvEventData(TypedDict):
    event_type: AlvEvent
    data: Any


class AlvParseError(Exception):
    """Raised when parsing fails."""


CLEAN_NAME_REGEX = re.compile(
    r"[;:,\.\\0-9\(\)^\{\}\"'`]|vanaf|tot|van\s\d", re.IGNORECASE
)
PROXY_REGEX = re.compile(r"(.*)\s+machtigt\s+(.*)", re.IGNORECASE | re.MULTILINE)
VOTE_RESULT_REGEX = re.compile(r"(.*?)\s*\&\s*(\d+)", re.IGNORECASE | re.MULTILINE)
INPUT_REGEX = re.compile(r"\\input\s?{(.*)}", re.IGNORECASE | re.MULTILINE)


class AlvParser:
    alv_data: AlvMetaData
    alv_events: List[AlvEventData]
    parse_errors: List[str]
    _alv_events: List[Tuple[int, AlvEventData]]
    soup: Optional[TexSoup]

    def __init__(self, file_data: Dict[str, str]) -> None:
        self.soup = None
        self.file_data = file_data

        self.alv_data = AlvMetaData(present=[], proxies=[])
        self.alv_events = []
        self.parse_errors = []
        self._alv_events = []

    def parse(self):
        entry_name = self._find_entry_tex()

        if entry_name is None:
            raise AlvParseError("No LaTeX entrypoint found.")

        try:
            self.soup = self._resolve(entry_name)
        except TypeError:
            raise AlvParseError(
                "Could not parse LaTeX content. " "Content is possibily malformed."
            )

        self._parse_newcommands()
        self._parse_metadata()

        #  Aanwezigheidslijst
        self._parse_present()

        #  Machtigingen
        self._parse_proxies_text()
        self._parse_proxies_command()

        #  Events
        self._parse_decisions()
        self._parse_sections()
        self._parse_timestamp()
        self._parse_task()
        self._parse_vote()

        self._sort_events()

    def _sort_events(self):
        self.alv_events = [ev[1] for ev in sorted(self._alv_events, key=lambda e: e[0])]

    def _parse_decisions(self):
        for decision in self.soup.find_all("decision"):
            if len(decision.args) != 1:
                self.parse_errors.append(
                    "Invalid number of arguments for '\\decision'."
                )
                continue

            decision_text = self._clean_text(decision)
            self._alv_events.append(
                (
                    decision.position,
                    AlvEventData(
                        event_type=AlvEvent.DECISION, data={"decision": decision_text}
                    ),
                )
            )

    def _parse_sections(self):
        for cmd in [
            "section",
            "subsection",
            "subsubsection",
            "section*",
            "subsection*",
            "subsubsection*",
        ]:
            for section in self.soup.find_all(cmd):
                section_text = self._clean_text(section)

                ev = AlvEvent.SECTION
                if not cmd.startswith("section"):
                    ev = AlvEvent.SUBSECTION

                self._alv_events.append(
                    (
                        section.position,
                        AlvEventData(event_type=ev, data={"title": section_text}),
                    )
                )

    def _parse_timestamp(self):
        for timestamp in self.soup.find_all("timestamp"):
            if len(timestamp.args) != 2:
                self.parse_errors.append(
                    "Invalid number of arguments for '\\timestamp'."
                )
                continue

            #  First argument is left empty, invalid timestamp
            if timestamp.args[0].string.strip() == "":
                self.parse_errors.append("Empty first argument in '\\timestamp'.")
                continue

            self._alv_events.append(
                (
                    timestamp.position,
                    AlvEventData(
                        event_type=AlvEvent.TIMESTAMP,
                        data={
                            "time": self._clean_text_list(timestamp.text[:1]),
                            "event": self._clean_text_list(timestamp.text[1:]),
                        },
                    ),
                )
            )

    def _parse_task(self):
        for task in self.soup.find_all("task"):
            if len(task.args) != 2:
                self.parse_errors.append("Invalid number of arguments for '\\task'.")
                continue

            #  First argument is left empty, invalid task
            if task.args[0].string.strip() == "":
                self.parse_errors.append("Empty first argument in '\\task'.")
                continue

            self._alv_events.append(
                (
                    task.position,
                    AlvEventData(
                        event_type=AlvEvent.TASK,
                        data={
                            "who": self._clean_text_list(task.text[:1]),
                            "what": self._clean_text_list(task.text[1:]),
                        },
                    ),
                )
            )

    def _parse_vote(self):
        for vote in self.soup.find_all("vote"):
            text = self._clean_text(vote, delim="\n")
            title = text

            m = VOTE_RESULT_REGEX.search(text)
            first_pos = None
            vote_data = {}

            while m:
                if first_pos is None:
                    first_pos = m.start()

                choice, votes = m.groups()
                #  Cast to int is save because of the Regex.
                vote_data[choice.strip()] = int(votes)

                m = VOTE_RESULT_REGEX.search(text, pos=m.end())

            if first_pos is not None:
                title = text[:first_pos]
            else:
                #  No matches found, thus no vote information
                self.parse_errors.append("No vote result found for '\\vote'.")

            self._alv_events.append(
                (
                    vote.position,
                    AlvEventData(
                        event_type=AlvEvent.VOTE,
                        data={"title": title.strip(), "vote_data": vote_data},
                    ),
                )
            )

    def _parse_metadata(self):
        for cmd in ["title", "chairman", "secretary", "location"]:
            data = self.soup.find(cmd)

            if data:
                self.alv_data[cmd] = self._clean_text(data)
            else:
                self.parse_errors.append(f"Metadata not found: '\\{cmd}'.")

        if self.soup.starttime is not None:
            self.alv_data["start_time"] = self._clean_text(self.soup.starttime)
        else:
            self.parse_errors.append("Metadata not found: '\\starttime'.")

        if self.soup.stoptime is not None:
            self.alv_data["stop_time"] = self._clean_text(self.soup.stoptime)
        else:
            self.parse_errors.append("Metadata not found: '\\stoptime'.")

    def _parse_present(self):
        r"""Finds the \present command and parses the names."""
        if self.soup.present is None:
            self.parse_errors.append("\\present command not found.")
            return

        # commas are sometimes used instead of newlines.
        present_text = self._clean_text(self.soup.present).replace(",", "\n")

        names = []
        for n in present_text.splitlines():
            n = self._clean_name(n)

            if n != "":
                names.append(n)

        self.alv_data["present"] = names

    def _parse_proxies_text(self):
        """Parses `X machtigt Y`."""
        clean_soup = self._clean_text(self.soup, delim="\n")
        m = PROXY_REGEX.search(clean_soup)

        while m:
            person1, person2 = m.groups()

            self.alv_data["proxies"].append(
                (self._clean_name(person1), self._clean_name(person2))
            )

            m = PROXY_REGEX.search(clean_soup, pos=m.end())

    def _parse_proxies_command(self):
        r"""Parses \proxy{X}{Y}."""
        for proxy in self.soup.find_all("proxy"):
            if len(proxy.args) != 2:
                self.parse_errors.append("Invalid number of arguments for '\\proxy'.")
                continue

            if proxy.args[0].string.strip() == "":
                self.parse_errors.append("Empty first argument in '\\proxy'.")
                continue

            if proxy.args[1].string.strip() == "":
                self.parse_errors.append("Empty second argument in '\\proxy'.")
                continue

            self.alv_data["proxies"].append(
                (self._clean_name(proxy.text[0]), self._clean_name(proxy.text[1]))
            )

    def _clean_text(self, node: TexNode, delim="") -> str:
        return self._clean_text_list(node.text, delim)

    def _clean_text_list(self, list: List[str], delim="") -> str:
        text = ""
        for item in list:
            if not item.startswith("%"):
                if item.startswith(delim):
                    #  Don't add an unnecessary delimiter
                    text += item
                else:
                    text += delim + item

        return text.strip()

    def _clean_name(self, s: str) -> str:
        """Cleans a name of all irrelevant symbols.

        Also removes the following:
        * (vanaf 13:37)
        * (tot xx)
        * (van x tot y)
        """
        res = re.sub(CLEAN_NAME_REGEX, "", s)
        #  Removes multiple spaces in between as well
        return re.sub(r"\s+", " ", res).strip()

    def _parse_newcommands(self):
        r"""Finds \newcommand directives and their usages.

        First, it finds commands like:
            `\newcommand{\wilco}{\name{Wilco Kruijer}}`,
        Then, it will search for all usages of `\wilco` and replace them with
            `\name{Wilco Kruijer}
        """
        for cmd in self.soup.find_all("newcommand"):
            if len(cmd.args) != 2:
                #  Only parse simple \newcommand directives.
                continue

            orig = cmd.contents[0]
            replace = cmd.args[1]

            for cmd_usage in self.soup.find_all(orig.name):
                #  Limitation in TexSoup causes replace_with not to work on
                #  arguments of a node. This is a workaround with unknown
                #  implications, but it works.
                cmd_usage.name = "noop"
                cmd_usage.args = TexArgs([replace])

    def _resolve(self, file_name: str) -> TexSoup:
        r"""Parses LaTeX content to a TexSoup tree.

        It first replaces \input directives by their contents.
        """
        if file_name not in self.file_data:
            raise AlvParseError(f"Could not resolve {file_name}.")

        content = self.file_data[file_name]

        #  We use Regex instead of TexSoup to parse \input directives, since
        #  something causes the parse_tree to be in a wrong state after using
        #  replace_with.

        m = INPUT_REGEX.search(content)
        while m:
            input_name = m.groups()[0]
            if not input_name.endswith(".tex"):
                input_name += ".tex"

            #  If we find a % after the last \n, this line is a comment
            last_newline_pos = content.rfind("\n", 0, m.start())
            last_comment_token_pos = content.rfind("%", 0, m.start())
            is_comment = last_comment_token_pos > last_newline_pos

            pos = m.end()
            if input_name in self.file_data and not is_comment:
                content = (
                    content[: m.start()]
                    + self.file_data[input_name]
                    + "\n"
                    + content[m.end() + 1 :]
                )

                #  Start the next search from the start of the just inserted
                #  content, so we parse input commands within input commands.
                pos = m.start()

            if input_name not in self.file_data and not is_comment:
                self.parse_errors.append(
                    f"File '{input_name}' not found for \\input command."
                )

            m = INPUT_REGEX.search(content, pos=pos)

        return TexSoup(content)

    def _find_entry_tex(self) -> Optional[str]:
        r"""Finds the entry point of this Latex project.

        When there is a file named main.tex we assume that is the entrypoint
        to the LaTeX project. If it doesn't exist we search for the first
        .tex file containing \documentclass.
        """

        if "main.tex" in self.file_data.keys():
            return "main.tex"

        for name in self.file_data.keys():
            content = self.file_data[name]

            if content.find("\\documentclass") >= 0:
                return name

        return None


def _extract_files(zf: ZipFile) -> Dict[str, str]:
    data = {}
    for name in zf.namelist():
        if not name.endswith(".tex"):
            continue

        data[name] = zf.read(name).decode("utf-8")

    return data


def parse_minutes(
    minutes: FileStorage,
) -> Tuple[AlvMetaData, List[AlvEventData], List[str]]:
    with ZipFile(minutes) as zf:
        file_data = _extract_files(zf)

        parser = AlvParser(file_data)
        parser.parse()

    return parser.alv_data, parser.alv_events, parser.parse_errors
