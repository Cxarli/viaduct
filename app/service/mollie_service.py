import logging
import secrets
import string
from typing import Optional, Tuple
from urllib.parse import parse_qs, urlparse

from flask import url_for
from flask_babel import _
from mollie.api.client import Client
from mollie.api.error import Error as MollieError

from app.exceptions.base import ResourceNotFoundException
from app.models.mollie import TransactionCallbackMixin, Transaction
from app.models.user import User
from app.repository import transaction_repository
from app.repository.transaction_repository import find_transaction_by_mollie_id
from app.service import membership_service
from config import Config

_logger = logging.getLogger(__name__)

_mollie_client = Client()


@Config.post_load
def _mollie_config(config: Config):
    """Set the mollie key after it has been loaded from the database."""

    if config.MOLLIE_KEY:
        _mollie_client.set_api_key(config.MOLLIE_KEY)
        _logger.info("Using MOLLIE_KEY: %s", config.MOLLIE_KEY)
    else:
        _logger.info("Using MOLLIE_KEY: NOTSET")


def on_webhook_called(mollie_id: str):
    transaction = find_transaction_by_mollie_id(mollie_id)
    # must silently fail if the transaction id was not found.
    if transaction:
        update_transaction_and_handle_callbacks(transaction)


def on_return(callback_id: str) -> Transaction:
    if not is_valid_callback_id(callback_id):
        raise ResourceNotFoundException("Transaction", callback_id)

    transaction = transaction_repository.find_transaction_by_callback_id(callback_id)
    if not transaction:
        raise ResourceNotFoundException("Transaction", callback_id)

    update_transaction_and_handle_callbacks(transaction)

    return transaction


def update_transaction_and_handle_callbacks(transaction: Transaction) -> Transaction:
    # note: might be called from the admin page, a webhook, or a user returning
    # after a payment. don't rely on current_user etc.

    try:
        payment = _mollie_client.payments.get(transaction.mollie_id)
    except MollieError as e:
        raise Exception(
            "Mollie api returned an error while getting a transaction"
        ) from e

    transaction_repository.update_status(transaction, payment["status"])

    # note: payment.is_paid() is not the same as status.payment == "paid",
    # instead it internally checks the paidAt field.
    if not transaction.has_paid and payment.is_paid():
        transaction_repository.update_has_paid(transaction, True)

        _handle_paid_callbacks(transaction)

    return transaction


def _handle_paid_callbacks(transaction: Transaction):
    if transaction.callback_membership:
        membership_service.on_membership_paid(transaction.callback_membership)


def create_transaction(
    amount: int, description: str, user: User, callback: TransactionCallbackMixin
) -> Tuple[Transaction, str]:
    callback_id = create_callback_id()

    callback_url = url_for("mollie.callback", callback_id=callback_id, _external=True)

    try:
        payment = _mollie_client.payments.create(
            {
                "amount": {"currency": "EUR", "value": _cents_to_float(amount)},
                "description": description,
                "redirectUrl": callback_url,
                "metadata": {
                    "id": callback_id,
                    "user_id": user.id,
                    "user_name": user.name,
                    "user_email": user.email,
                },
            }
        )
    except MollieError as e:
        # raise Exception(
        #     "Mollie api returned an error while creating a transaction"
        # ) from e
        raise e

    return (
        transaction_repository.create_transaction_with_callback(
            payment["id"], payment["status"], callback_id, callback
        ),
        payment.checkout_url,
    )


def _cents_to_float(cents: int):
    prefix = "-" if cents < 0 else ""
    cents = abs(cents)
    return prefix + f"{cents // 100}.{(cents % 100):0>2}"


def create_callback_id():
    available_characters = string.ascii_letters + string.digits
    return "".join(secrets.choice(available_characters) for _ in range(32))


def is_valid_callback_id(callback_id: str):
    if len(callback_id) != 32:
        return False

    available_characters = string.ascii_letters + string.digits
    return all(c in available_characters for c in callback_id)


def get_payments(from_payment_id: Optional[str], count=10):
    try:
        if from_payment_id is None:
            # If no payment ID is given, the API will return the first payments
            # by default.
            params = {"limit": count}
        else:
            params = {"from": from_payment_id, "limit": count}

        payments = _mollie_client.payments.list(**params)

        if payments["_links"]["previous"] is None:
            previous_payments = None
        else:
            # Gets the payment ID for the previous range of payments
            previous_payments = parse_qs(
                urlparse(payments["_links"]["previous"]["href"]).query
            )["from"][0]

        if payments["_links"]["next"] is None:
            next_payments = None
        else:
            next_payments = parse_qs(
                urlparse(payments["_links"]["next"]["href"]).query
            )["from"][0]

        return payments, previous_payments, next_payments, "success"
    except MollieError as e:
        return [], _("Api call failed: %s" % e)


def find_transaction_for_mollie_id(mollie_id: str) -> Optional[Transaction]:
    return transaction_repository.find_transaction_by_mollie_id(mollie_id)
