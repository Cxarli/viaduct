import datetime
from typing import List, Optional, Tuple

from app.exceptions.base import ResourceNotFoundException
from app.models.meeting import Meeting
from app.models.user import User
from app.repository import meeting_repository
from app.service import group_service


def add(
    group_id: int,
    name: str,
    cancelled: bool,
    start_time: datetime.datetime,
    end_time: datetime.datetime,
    location: str = "",
    description: str = "",
) -> Meeting:
    start_time, end_time = configure_end_time(start_time, end_time)

    m = Meeting()
    m.group_id = group_id
    m.name = name
    m.cancelled = cancelled
    m.start_time = start_time
    m.end_time = end_time
    m.location = location
    m.description = description

    meeting_repository.add_meeting(m)

    return m


def update(
    m: Meeting,
    group_id: int = None,
    name: str = None,
    cancelled: bool = None,
    start_time: datetime.datetime = None,
    end_time: datetime.datetime = None,
    location: str = None,
    description: str = None,
) -> None:
    start_time, end_time = configure_end_time(
        start_time or m.start_time, end_time or m.end_time
    )

    if group_id is not None:
        m.group_id = group_id
    if name is not None:
        m.name = name
    if cancelled is not None:
        m.cancelled = cancelled
    if start_time is not None:
        m.start_time = start_time
    if end_time is not None:
        m.end_time = end_time
    if location is not None:
        m.location = location
    if description is not None:
        m.description = description

    meeting_repository.update_meeting()


def configure_end_time(
    start_time: datetime.datetime, end_time: Optional[datetime.datetime]
) -> Tuple[datetime.datetime, datetime.datetime]:
    if start_time == end_time or end_time is None:
        end_time = start_time + datetime.timedelta(hours=1)

    return start_time, end_time


def get_by_id(meeting_id: int) -> Meeting:
    """
    Get single meeting by id

    :param meeting_id:
    :return: Meeting
    """
    meeting = meeting_repository.get_meeting_by_id(meeting_id)

    if not meeting:
        raise ResourceNotFoundException("meeting", meeting_id)

    return meeting


def get_by_user(user: User, future: bool = None) -> List[Meeting]:
    """
    Get all meetings of groups where user is part of

    :param future: get only future meetings if true, past if false and if
    None get all meetings
    :param user: User object
    :return: list of meetings
    """
    if future is True:
        return meeting_repository.get_upcoming_for_user(user)
    elif future is False:
        return meeting_repository.get_past_for_user(user)
    else:
        return meeting_repository.get_all_for_user(user)


def remove_by_id(meeting_id: int) -> None:
    """
    Remove a single meeting by id

    :param meeting_id:
    :return: None
    """
    meeting_repository.remove_meeting_by_id(meeting_id)


def find_all() -> List[Meeting]:
    """
    Return all meetings, past and future.

    :return: List of Meetings
    """
    groups = [g.id for g in group_service.find_groups()]

    return [m for m in meeting_repository.get_meetings_by_group_ids(groups)]
