import logging
from collections import defaultdict
from typing import Dict, List, Literal, Optional, Tuple

from flask import url_for
from werkzeug.exceptions import Gone

from app import db
from app.exceptions.base import (
    BusinessRuleException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.navigation import NavigationEntry
from app.models.page import Page
from app.models.user import User
from app.repository import model_service, navigation_repository
from app.service import page_service

_logger = logging.getLogger(__name__)


def get_stripped_path(input_path: str, depth: int) -> str:
    path = input_path.rstrip("0123456789")
    path = path.rstrip("/")
    path = path.lstrip("/")

    for _ in range(depth):
        try:
            # Try to parse a parent path
            path = path.rsplit("/", 1)[0]
        except IndexError:
            # Path does not contain any more slashes
            break
    _logger.debug(f"get_stripped_path {input_path=} {path=}")
    return path


# @cache.memoize()
def get_navigation_top_entries():
    """
    Retrieve all navigation entries, and preload the children

    Return the top level entries.
    :return:
    """
    _logger.debug("Loading navigation top entries.")

    return navigation_repository.get_root_entries(children_depth=2)


def get_navigation_root_entries(children_depth) -> List[NavigationEntry]:
    return navigation_repository.get_root_entries(children_depth)


def get_navigation_backtrack(path) -> List[NavigationEntry]:
    """Return a list of entries to be used as backtrack menu."""
    _logger.debug("Loading navigation backtrack.")
    backtrack = []
    path = get_stripped_path(path, depth=0)
    entry = find_entry_by_path(path)
    while entry:
        backtrack.append(entry)
        entry = navigation_repository.get_parent(entry, children_depth=0)

    backtrack.reverse()
    return backtrack


def get_navigation_side_entries(path: str, user: User):
    _logger.debug("Loading navigation side entries.")

    path = path.rstrip("/")

    entry = find_entry_by_path(get_stripped_path(path, depth=0))
    parent = None

    if entry:
        parent = navigation_repository.get_parent(entry, children_depth=1)
    else:
        _logger.debug("Could not find entry for '%s', attempting parent path", path)
        entry = find_entry_by_path(get_stripped_path(path, depth=1))
        if entry and entry.parent_id:
            parent = navigation_repository.get_parent(entry, children_depth=1)

    if parent:
        entries = get_children(parent, authorized_user=user)
    elif entry:
        entries = [entry]
    else:
        entries = []

    return entries, entry


# Dictionary of parent_id to children where parent_id == None are root entries.
NavigationChildren = Dict[Optional[int], List[NavigationEntry]]


def get_navigation_ordering() -> Tuple[List[NavigationEntry], NavigationChildren]:
    entries = model_service.get_all(NavigationEntry)
    entries_by_id = dict()
    root = navigation_repository.get_root_entries(children_depth=0)
    ordering = defaultdict(list)
    for entry in entries:
        entries_by_id[entry.id] = entry
        ordering[entry.parent_id].append(entry)

    # Apply the actual order of the subitems.
    for k, v in ordering.items():
        if k and entries_by_id[k].order_children_alphabetically:
            ordering[k] = sorted(v, key=lambda x: x.title)
        else:
            ordering[k] = sorted(v, key=lambda x: x.position)

    return root, ordering


def find_entry_by_path(path: str) -> Optional[NavigationEntry]:
    try:
        page = page_service.get_page_by_path(path)
        entry = navigation_repository.find_entry_by_page(page)
        if entry:
            return entry
    except (ResourceNotFoundException, Gone):
        pass

    return navigation_repository.find_entry_by_path(path)


def get_entry_by_id(entry_id: int) -> NavigationEntry:
    entry = navigation_repository.find_entry_by_id(entry_id, children_depth=1)
    if not entry:
        raise ResourceNotFoundException("navigation", entry_id)
    return entry


def get_children(
    entry: NavigationEntry, authorized_user: Optional[User]
) -> List[NavigationEntry]:
    """
    Load the children and filter/order them accordingly.

    Note: entry.children raises by default,
    """
    children: List[NavigationEntry] = entry.children

    if entry.order_children_alphabetically:
        children = sorted(children, key=lambda x: x.title)
    else:
        children = sorted(children, key=lambda x: x.position)

    if authorized_user is not None:
        children = remove_unauthorized(children, authorized_user)
    return children


def can_view_navigation_entry(entry: NavigationEntry, user) -> bool:
    """
    Check whether the current user can view the entry.

    Note: currently only works with pages.
    """
    if entry.external or entry.activity_list or not entry.page:
        return True
    return page_service.can_user_read_page(entry.page, user)


def remove_unauthorized(
    entries: List[NavigationEntry], user: User
) -> List[NavigationEntry]:
    authorized_entries = list(entries)
    for entry in entries:
        if not can_view_navigation_entry(entry, user):
            authorized_entries.remove(entry)

    return authorized_entries


def save_navigation_entries(entries: Dict, parent: Optional[NavigationEntry]):
    position = 1

    for dict_entry in entries:
        entry_id = dict_entry["id"]
        entry = model_service.find_by_id(NavigationEntry, entry_id)
        if entry is None:
            _logger.error("Could not save NavigationOrderEntry with id %d", entry_id)
            continue
        entry.parent_id = parent.id if parent else None
        entry.position = position

        save_navigation_entries(dict_entry["children_data"], entry)

        position += 1

        db.session.add(entry)
        db.session.commit()


def delete_navigation_entry(entry: NavigationEntry, remove_page: bool):
    if entry.children:
        raise BusinessRuleException("This entry still has children")

    if remove_page:
        if entry.external or entry.activity_list or not entry.page_id:
            raise BusinessRuleException("This entry does no refer to a page.")

        page_service.delete_page(entry.page)

    navigation_repository.remove_navigation_entry(entry)
    model_service.delete(entry)


def set_entry_type(
    entry: NavigationEntry,
    type: Literal["page", "url", "activities"],
    page_id: Optional[int],
    url: Optional[str],
    external: Optional[bool],
) -> None:
    if type == "page":
        if not page_id:
            raise ValidationException("Missing page id for navigation of type page")
        page = model_service.get_by_id(Page, page_id)
        entry.page_id = page.id
        entry.external = False
        entry.activity_list = False
    elif type == "url":
        if not url:
            raise ValidationException("Missing url for navigation of type url")
        entry.url = get_stripped_path(url, depth=0)
        entry.external = external
        entry.activity_list = False
        entry.page_id = None
    elif type == "activities":
        entry.external = False
        entry.activity_list = True
        entry.order_children_alphabetically = False
        entry.url = get_stripped_path(url_for("activity.view"), depth=0)
        entry.page_id = None


def create_entry(
    # "page" is for site-created pages, "url" for programmed pages,
    # and "activities" for pages whose children are activities.
    type: Literal["page", "url", "activities"],
    nl_title: str,
    en_title: str,
    parent_id: int = None,
    page_id: int = None,
    url: str = None,
    external: bool = False,
    order_children_alphabetically: bool = False,
):
    entry = NavigationEntry()
    entry.parent_id = parent_id
    entry.nl_title = nl_title
    entry.en_title = en_title
    entry.order_children_alphabetically = order_children_alphabetically

    set_entry_type(entry, type, page_id, url, external)

    if parent_id:
        parent = get_entry_by_id(parent_id)

        # Do not use the get_children method, as we do not want to filter any entries.
        entry.position = len(parent.children) + 1
    else:
        entry.position = len(get_navigation_root_entries(children_depth=0)) + 1

    navigation_repository.save(entry)
    return entry


def update_entry(
    entry: NavigationEntry,
    type: Literal["page", "url", "activities"],
    nl_title: str,
    en_title: str,
    page_id: int = None,
    url: str = None,
    external: bool = False,
    order_children_alphabetically: bool = False,
):
    entry.nl_title = nl_title
    entry.en_title = en_title
    entry.order_children_alphabetically = order_children_alphabetically

    set_entry_type(entry, type, page_id, url, external)

    navigation_repository.save(entry)
    return entry
