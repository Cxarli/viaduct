from typing import Sequence, Union

import bleach
from bleach_allowlist import generally_xss_safe, all_styles, print_attrs, markdown_attrs
from markdown import markdown
from markdown.extensions import Extension
from app.utils.markdown_summary import MarkdownSummaryExtension, SummaryOptions
from app.utils.markdown_youtube import MarkdownYoutubeExtension
from app.utils.markdown_blockquotes import MarkdownBlockquotesExtension

markdown_extensions: Sequence[Union[str, Extension]] = [
    "toc",
    "tables",
    MarkdownYoutubeExtension(),
    MarkdownBlockquotesExtension(),
]

all_allowed_attrs = {**print_attrs, **markdown_attrs}


def render_markdown_safely(markdown_content: str):
    """Renders markdown as escaped HTML.

    E.g. **via** --> <strong>via</strong>
         <script> ... </script> --> &lt;script&gt; ... &lt;/script&gt;"""
    return markdown(_clean_html(markdown_content), extensions=markdown_extensions)


def render_markdown_summarized_safely(
    markdown_content: str,
    options: SummaryOptions,
):
    return markdown(
        _clean_html(markdown_content), extensions=[MarkdownSummaryExtension(options)]
    )


def _clean_html(data: str):
    return bleach.clean(
        data,
        tags=generally_xss_safe,
        attributes=all_allowed_attrs,
        styles=all_styles,
    )
