import hashlib
from datetime import datetime, time
from typing import List, Tuple

import pytz
from flask import url_for
from icalendar import Calendar, Event, vCalAddress, vGeo, vText
from pytz import timezone

from app import app, constants
from app.models.activity import Activity
from app.models.alv_model import Alv
from app.models.group import Group
from app.models.meeting import Meeting
from app.models.user import User
from app.repository import model_service
from app.service import (
    activity_service,
    alv_service,
    group_service,
    meeting_service,
    pretix_service,
)
from app.service.pretix_service import PretixPaymentStatus


class CalendarEvent(object):
    def __init__(
        self,
        event_id: int,
        summary: str,
        created: datetime,
        last_modified: datetime,
        dtstart: datetime,
        dtend: datetime,
        organizer: Group = None,
        sequence: int = 0,
    ):
        # data of data from function from a model
        self.id = event_id
        self.created: datetime = created
        self.last_modified: datetime = last_modified
        self.sequence = sequence

        if organizer:
            self.organizer = vCalAddress(f"MAILTO:" f"{organizer.maillist}@svia.nl")
            self.organizer.params["cn"] = vText(organizer.name)
        else:
            self.organizer = vCalAddress("MAILTO:bestuur@svia.nl")
            self.organizer.params["cn"] = vText("Bestuur via")

        # user input below
        self.summary = summary
        # In icalendar/prop.py:304 a specific check for pytz.utc is done.
        # Therefore replace our instances of tzinfo (assumed utc) with pytz.utc
        self.dtstart: datetime = dtstart.replace(tzinfo=pytz.utc)
        self.dtend: datetime = dtend.replace(tzinfo=pytz.utc)

        # below are most of the optional things
        self.classification: str = "Public"
        self.description: str = ""
        self.location: str = ""
        self.priority: int = 0
        self.status: str = "TENTATIVE"
        self.transp: str = "OPAQUE"
        self.url: str = ""

        # dtstamp is the same as last_modified because we don't use METHOD
        self.dtstamp: datetime = last_modified

        # default coördinates of via-room
        self.geo: Tuple[float, float] = (52.3545425, 4.9553998)

    def set_classification(self, classification: str) -> None:
        """
        Func to set classification of event

        :param classification: str which can be "PUBLIC", "PRIVATE" or
        "CONFIDENTIAL" :return: None
        """
        if classification.upper() in ["PUBLIC", "PRIVATE", "CONFIDENTIAL"]:
            self.classification = classification.upper()
        else:
            raise ValueError("Invalid classification of event")

    def set_geo(self, geo: Tuple[float, float]) -> None:
        """
        Func to set geographical coördinates of location

        :param geo: List of floats with coördinates
        :return: None
        """
        if type(geo) is tuple and type(geo[0]) is float and type(geo[1]) is float:
            self.geo = vGeo(geo)
        else:
            raise TypeError("Geo types are incorrect")

    def set_location(self, location: str) -> None:
        """
        Func to set textual location of event

        :param location: name of location
        :return: None
        """

        # TODO auto set geo location data (https://pypi.org/project/geopy/)
        if type(location) is str:
            self.location = location
        else:
            raise TypeError("Location type is incorrect")

    def set_transp(self, transp: str) -> None:
        """
        Set transparity of event for search in free/busy searches

        :param transp: str that must be:
            "OPAQUE" (blocks or opaque on busy time searches) or
            "TRANSPARENT" (transparent on busy time searches)
        :return: None
        """
        if transp.upper() in ["OPAQUE", "TRANSPARENT"]:
            self.transp = transp.upper()
        else:
            raise ValueError("transp must be OPAQUE or TRANSPARENT")

    def set_status(self, status: str) -> None:
        """
        Set status of event

        :param status: str that must be:
            "TENTATIVE" (indicates event is tentative) or
            "CONFIRMED" (indicates event is definite) or
            "CANCELLED" (indicates event was cancelled)
        :return: None
        """
        if status.upper() in ["TENTATIVE", "CONFIRMED", "CANCELLED"]:
            self.status = status.upper()
        else:
            raise ValueError("status must be TENTATIVE, CONFIRMED or CANCELLED")

    def set_description(self, description: str) -> None:
        """
        Func to set description of event

        :param description: description of event.
        :return: None
        """
        self.description = description

    def set_url(self, url: str) -> None:
        """
        Func to set URL of event

        :param url: str of URL
        :return: None
        """

        if type(url) is str:
            self.url = url
        else:
            raise ValueError("url is not a string")

    def set_priority(self, priority: int) -> None:
        """
        Function set priority of event

        :param priority: integer that must be from 0 (high) to 9 (low)
        :return: None
        """
        if 0 <= priority <= 9:
            self.priority = priority
        else:
            raise ValueError("priority is lower than 0 or higher than 9")

    def build(self) -> Event:
        """
        Function that builds icalendar event with parameters of the class.

        :return: iCalendar Event
        """
        e = Event()

        e.add("uid", f"{datetime.timestamp(self.created)}+{self.id}@svia.nl")
        e.add("created", self.created)
        e.add("last-modified", self.last_modified)
        e.add("sequence", self.sequence)
        e.add("organizer", self.organizer)
        e.add("summary", self.summary)
        e.add("dtstart", self.dtstart)
        e.add("dtend", self.dtend)
        e.add("classification", self.classification)
        e.add("description", self.description)
        e.add("location", self.location)
        e.add("priority", self.priority)
        e.add("status", self.status)
        e.add("transp", self.transp)
        e.add("url", self.url)
        e.add("dtstamp", self.dtstamp)
        e.add("geo", self.geo)

        return e


class CalendarBuilder:
    def __init__(self):
        self.user = None
        self.calendar: Calendar = Calendar()

    @staticmethod
    def _meeting(m: Meeting) -> Event:
        """
        Function to fill in (Calendar)Event object to return an Event object.

        :param m: Meeting object
        :return: iCalendar Event object
        """
        ce = CalendarEvent(
            m.id,
            m.name,
            m.created,
            m.modified,
            m.start_time,
            m.end_time,
            group_service.find_group_by_id(m.group_id),
            m.sequence,
        )
        ce.set_description(m.description if m.description else "")
        ce.set_priority(0)
        ce.set_transp("OPAQUE")
        if m.location:
            ce.set_location(m.location)
        ce.set_classification("PRIVATE")

        if m.cancelled:
            ce.set_status("CANCELLED")
        elif not m.cancelled and datetime.now(pytz.utc) > m.start_time:
            ce.set_status("CONFIRMED")
        else:
            ce.set_status("TENTATIVE")

        return ce.build()

    @staticmethod
    def _activity(a: Activity, locale="nl") -> Event:
        if locale == "nl":
            name = a.nl_name or ""
            description = a.nl_description or ""
        else:
            name = a.en_name or ""
            description = a.en_description or ""

        ce = CalendarEvent(
            a.id, name, a.created, a.modified, a.start_time, a.end_time, None
        )
        ce.set_description(description)
        ce.set_location(a.location)
        ce.set_classification("PUBLIC")
        ce.set_transp("OPAQUE")
        ce.set_url(url_for("activity.get_activity", activity=a, _external=True))
        ce.set_status("TENTATIVE")

        return ce.build()

    @staticmethod
    def _alv(alv: Alv, locale="nl"):
        if locale == "nl":
            name = alv.nl_name
        else:
            name = alv.en_name

        tz_ams = timezone("Europe/Amsterdam")
        start_time = datetime.combine(alv.date, time(18))
        start_time = tz_ams.localize(start_time, is_dst=None)
        start_time = start_time.astimezone(pytz.utc)
        end_time = datetime.combine(alv.date, time(21, 30))
        end_time = tz_ams.localize(end_time, is_dst=None)
        end_time = end_time.astimezone(pytz.utc)

        ce = CalendarEvent(
            alv.id, name, alv.created, alv.modified, start_time, end_time
        )

        ce.set_status("TENTATIVE")

        if alv.activity:
            ce.set_location(alv.activity.location)
            ce.dtstart = alv.activity.start_time
            ce.dtend = alv.activity.end_time
            ce.set_status("CONFIRMED")

        ce.set_classification("PUBLIC")
        ce.set_transp("OPAQUE")
        ce.set_url(url_for("alv.view", alv=alv, _external=True))

        return ce.build()

    def set_user(self, u: User) -> None:
        """
        Set the user to get personalized activities and meetings

        :param u: User object
        :return: None
        """
        self.user = u

    def _meetings(self, meetings: List[Meeting]) -> None:
        for m in meetings:
            self.calendar.add_component(self._meeting(m))

    def _activities(self, activities: List[Activity], locale: str) -> None:
        for a in activities:
            self.calendar.add_component(self._activity(a, locale))

    def _alvs(self, alvs: List[Alv], locale: str) -> None:
        for a in alvs:
            self.calendar.add_component(self._alv(a, locale))

    def build_user(self, types: List[str]) -> Calendar:
        if self.user is None:
            raise ValueError("Unknown for which user the calendar should be")

        event_slugs = pretix_service.get_user_orders(
            self.user, PretixPaymentStatus.active
        )
        user_pretix_activities = set(
            activity_service.get_by_pretix_slugs(list(event_slugs.keys()))
        )

        alvs = alv_service.find_all_alv()
        alv_activities = {a.activity for a in alvs if a.activity}

        if "activity" in types:
            self._activities(
                list(user_pretix_activities - alv_activities), self.user.locale
            )
        if "alv" in types:
            self._alvs(alvs, self.user.locale)
        if "meeting" in types:
            self._meetings(meeting_service.get_by_user(self.user))

        self.calendar.add("version", "2.0")
        self.calendar.add("prodid", "-//svia.nl/personalcalendar//NONSGML v1.0//NL")
        self.calendar.add("name", "via calendar")
        self.calendar.add("X-WR-CALNAME", "via calendar")
        return self.calendar.to_ical()

    def build_public(self, types: List[str], locale: str = None) -> Calendar:
        """
        Function to create public ics's
        """

        if locale not in constants.LANGUAGES.keys() or not locale:
            locale = "en"

        if "meeting" in types:
            self._meetings(model_service.get_all(Meeting))

        if "alv" in types:
            self._alvs(model_service.get_all(Alv), locale)

        if "activity" in types:
            self._activities(model_service.get_all(Activity), locale)

        prod_id = "-".join(types)

        self.calendar.add("version", "2.0")
        self.calendar.add(
            "prodid",
            f"-//svia.nl/publiccalendar//{prod_id}//NONSGML "
            f"v1.0//{locale.capitalize()}",
        )
        self.calendar.add("name", "via public calendar")
        self.calendar.add("X-WR-CALNAME", "via public calendar")

        return self.calendar.to_ical()


def get_calendar_hash(user_id: int) -> str:
    s = bytes(str(app.secret_key), "utf-8")
    u = bytes(str(user_id), "utf-8")

    return hashlib.sha512(s + u).hexdigest()
