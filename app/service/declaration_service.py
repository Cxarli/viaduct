import mimetypes
import os
from typing import Tuple, Iterable
from babel.numbers import format_decimal
from flask_login import current_user
from app.service import mail_service
from app.task.mail import MailCommand
from app.repository import declaration_repository
from app.models.declaration import Declaration


def send_declaration(
    file_data: Iterable[Tuple[str, str]],
    reason: str = None,
    committee: str = None,
    amount: float = None,
    iban: str = None,
):
    new_amount = format_decimal(amount, locale="nl_NL")
    user = current_user
    command = MailCommand("penningmeester@svia.nl")
    command.reply_to = user.email
    command.with_template(
        "declaration",
        locale="en",
        user=user,
        reason=reason,
        committee=committee,
        amount=new_amount,
        iban=iban,
    )

    for i, (file_location, content_type) in enumerate(file_data):
        extension = mimetypes.guess_extension(content_type) or ""

        with open(file_location, "rb") as fp:
            command.with_loaded_attachment(
                "File" + str(i + 1) + extension, fp.read(), content_type
            )
        os.remove(file_location)

    mail_service.send_mail(command)
    declaration = Declaration(
        committee=committee, amount=amount, reason=reason, user_id=user.id
    )
    declaration_repository.save(declaration)


def get_all_declarations_by_user_id(user_id: int):
    return declaration_repository.find_all_declarations_by_user_id(user_id)
