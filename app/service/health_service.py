import logging
from typing import Dict

from celery import Celery
from celery.exceptions import CeleryError

from app.repository import health_repository

_logger = logging.getLogger(__name__)


def get_system_health(worker: Celery) -> Dict[str, bool]:
    subsystems = list()

    db_status = health_repository.check_database_status()
    subsystems.append({"name": "database", "healthy": db_status})

    try:
        workers = worker.control.inspect().active()
        if workers and len(workers) > 0:
            worker_status = True
        else:
            worker_status = False
    except CeleryError as e:
        _logger.error("Celery health check failed: %s", e)
        worker_status = False

    subsystems.append({"name": "worker", "healthy": worker_status})

    status = dict()
    status["healthy"] = all([db_status, worker_status])
    status["worker"] = worker_status
    status["database"] = db_status

    return status
