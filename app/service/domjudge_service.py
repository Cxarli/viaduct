import csv
import datetime as dt
import re
from io import StringIO
from typing import Any, List, NamedTuple, Optional, Dict, Tuple
from urllib.parse import urljoin
from pathlib import PurePath
from base64 import b64encode

import requests
from marshmallow import EXCLUDE
from werkzeug.datastructures import FileStorage

from app import app
from app.enums import FileCategory
from app.models.domjudge import DOMjudgeContestSettings
from app.models.file import File
from app.repository import domjudge_repository
from config import Config
from app.networking import DefaultResponseAdapter
from app.exceptions.base import (
    ApplicationException,
    AuthorizationException,
    ResourceNotFoundException,
)
from app.exceptions.domjudge import (
    ProblemTextNotFoundException,
    ContestNotActiveException,
)
from app.models.user import User
from app.service import user_service, file_service
from app.utils.marshmallow import create_schema_from_named_tuple

API_BASE_URL = "/api/v4"
VIADUCT_GROUP_NAME = "viaduct_user"
VIADUCT_CONTEST_SHORTNAME_PREFIX = "viaduct-"
VIA_USER_TEAM_REGEX = re.compile(r"^via_user_team_(?P<user_id>\d+).*$")
SUBMIT_FILENAME_ILLEGAL_CHARS_REGEX = re.compile(r"[^a-zA-Z0-9+_.-]")
SUBMIT_FILENAME_ALLOWED_FIRST_CHAR_REGEX = re.compile(r"[a-zA-Z]")

HEX_COLOR_REGEX = re.compile(r"^#([a-fA-F0-9]{3}|[a-fA-F0-9]{6})$")

DOMJUDGE_SESSION = requests.Session()


@Config.post_load
def _domjudge_config(config: Config):
    global DOMJUDGE_SESSION
    DOMJUDGE_SESSION.mount(config.DOMJUDGE_URL, DefaultResponseAdapter("DOMJudge"))


class Contest(NamedTuple):
    id: int
    name: str
    shortname: str
    start_time: dt.datetime
    end_time: dt.datetime


class ScoreboardTeamScore(NamedTuple):
    num_solved: int
    total_time: int


class ScoreboardProblem(NamedTuple):
    label: str
    problem_id: int
    num_judged: int
    num_pending: int
    solved: bool
    first_to_solve: bool
    time: Optional[int] = None


class ScoreboardRow(NamedTuple):
    rank: int
    team_id: int
    score: ScoreboardTeamScore
    problems: List[ScoreboardProblem]


Scoreboard = List[ScoreboardRow]


class Problem(NamedTuple):
    id: int
    name: str
    label: str
    short_name: str
    rgb: str = "#FFFFFF"


class Group(NamedTuple):
    id: int
    name: str


class Team(NamedTuple):
    id: int
    name: str
    group_ids: List[int]
    via_user: Optional[User] = None


class Language(NamedTuple):
    id: str
    name: str
    extensions: List[str]
    filter_compiler_files: bool


class Judgement(NamedTuple):
    id: int
    submission_id: int
    judgement_type_id: Optional[str]
    valid: bool


class Submission(NamedTuple):
    id: int
    time: dt.datetime
    team_id: int
    problem_id: int
    language_id: str
    judgement_type_id: Optional[str] = None


class CurrentUserInfo(NamedTuple):
    id: int
    username: str
    enabled: bool
    roles: List[str]


class ProblemText(NamedTuple):
    data: bytes
    content_type: Optional[str]
    content_disposition: Optional[str]


class ServerCheckResult(NamedTuple):
    can_reach_api: bool
    domjudge_version: Optional[str]
    can_login_with_admin_user: bool
    can_login_with_admin_user_nonapi: bool
    admin_user_has_admin_role: bool
    num_contests: int
    viaduct_user_group_exists: bool
    viaduct_user_group_id: Optional[int]


class ContestCheckResult(NamedTuple):
    contest: Contest
    problems: List[Problem]
    problems_without_problem_text: List[Problem]
    problems_without_color: List[Problem]


ContestSchema = create_schema_from_named_tuple(Contest)
ProblemSchema = create_schema_from_named_tuple(Problem)
ScoreboardRowSchema = create_schema_from_named_tuple(ScoreboardRow)
GroupSchema = create_schema_from_named_tuple(Group)
TeamSchema = create_schema_from_named_tuple(Team, ignore_fields=["via_user"])
LanguageSchema = create_schema_from_named_tuple(Language)
JudgementSchema = create_schema_from_named_tuple(Judgement)
SubmissionSchema = create_schema_from_named_tuple(
    Submission, ignore_fields=["judgement_type_id"]
)
CurrentUserInfoSchema = create_schema_from_named_tuple(CurrentUserInfo)


def _execute_request(
    method: str,
    url: str,
    session: Optional[requests.Session] = None,
    use_api_base_url: bool = True,
    **kwargs: Any,
) -> requests.Response:

    if use_api_base_url:
        relative_url = API_BASE_URL + url
    else:
        relative_url = url

    full_url = urljoin(app.config["DOMJUDGE_URL"], relative_url)

    if session:
        response = session.request(method, full_url, **kwargs)
    else:
        response = DOMJUDGE_SESSION.request(method, full_url, **kwargs)

    if response.status_code == 404:
        raise ResourceNotFoundException(url, "")
    elif response.status_code == 401:
        raise AuthorizationException("DOMjudge API response status code was 401")
    elif response.status_code >= 400:
        raise ApplicationException(response=response)

    return response


def _execute_get_request(
    url: str,
    session: Optional[requests.Session] = None,
    use_api_base_url: bool = True,
    **kwargs: Any,
) -> requests.Response:
    return _execute_request(
        "GET", url, session=session, use_api_base_url=use_api_base_url, **kwargs
    )


def _execute_post_request(
    url: str,
    session: Optional[requests.Session] = None,
    use_api_base_url: bool = True,
    **kwargs: Any,
) -> requests.Response:
    return _execute_request(
        "POST", url, session=session, use_api_base_url=use_api_base_url, **kwargs
    )


def get_domjudge_team_id(user: User) -> int:
    return user.id + int(10e5)


def _get_domjudge_username(user: User) -> str:
    return f"via_user_{get_domjudge_team_id(user)}"


def _get_domjudge_team_name(user: User) -> str:
    name_underscores = user.name.replace(" ", "_")
    return f"via_user_team_{user.id}_{name_underscores}"


def _create_session(
    username: str, password: str, nonapi: bool = False
) -> requests.Session:
    session = requests.Session()

    if nonapi:
        # Non-API requests cannot be authenticated using basic auth
        # TODO: maybe cache session object with cookie
        #  so we don't have to do this for every request?
        response = _execute_post_request(
            "/login",
            session=session,
            use_api_base_url=False,
            headers={
                "X-DOMjudge-Login": username,
                "X-DOMjudge-Pass": b64encode(password.encode("utf-8")).decode("ascii"),
            },
            data={"loginmethod": "xheaders"},
        )

        # Having this header means that we are redirected to the login page
        # and that the authentication has failed.
        if "X-Login-Page" in response.headers.keys():
            raise AuthorizationException(
                "DOMjudge authentication using X-Headers failed."
            )
    else:
        session.auth = (username, password)

    session.mount(app.config["DOMJUDGE_URL"], DefaultResponseAdapter("DOMJudge"))
    return session


def _create_admin_session(nonapi: bool = False) -> requests.Session:
    return _create_session(
        app.config["DOMJUDGE_ADMIN_USERNAME"],
        app.config["DOMJUDGE_ADMIN_PASSWORD"],
        nonapi=nonapi,
    )


def _create_user_session(user: User) -> requests.Session:
    return _create_session(
        _get_domjudge_username(user), app.config["DOMJUDGE_USER_PASSWORD"]
    )


def _get_viaduct_group_id(contest_id: int) -> int:
    with _create_admin_session() as session:
        result = _execute_get_request(f"/contests/{contest_id}/groups", session=session)

    groups: List[Group] = GroupSchema(many=True, unknown=EXCLUDE).loads(result.text)
    for group in groups:
        if group.name == VIADUCT_GROUP_NAME:
            return group.id

    raise ResourceNotFoundException("domjudge_group", VIADUCT_GROUP_NAME)


def _get_current_user_info(session: requests.Session) -> CurrentUserInfo:
    result = _execute_get_request("/user", session=session)
    user_info: CurrentUserInfo = CurrentUserInfoSchema(unknown=EXCLUDE).loads(
        result.text
    )

    return user_info


def domjudge_integration_enabled() -> bool:
    return app.config["DOMJUDGE_ENABLED"]


def check_server_communication() -> ServerCheckResult:
    can_reach_api = False
    domjudge_version: Optional[str] = None
    can_login_with_admin_user = False
    can_login_with_admin_user_nonapi = False
    admin_user_has_admin_role = False
    num_contests: int = 0
    viaduct_user_group_exists = False
    viaduct_user_group_id: Optional[int] = None

    try:
        result_info = _execute_get_request("/info")

        can_reach_api = True
        domjudge_version = result_info.json()["domjudge_version"]

        try:
            with _create_admin_session() as admin_session:
                admin_info = _get_current_user_info(admin_session)
                can_login_with_admin_user = True
                admin_user_has_admin_role = "admin" in admin_info.roles
        except AuthorizationException:
            pass

        try:
            _create_admin_session(nonapi=True)
            can_login_with_admin_user_nonapi = True
        except AuthorizationException:
            pass

        contests = get_all_contests()
        num_contests = len(contests)

        if num_contests > 0:
            try:
                viaduct_user_group_id = _get_viaduct_group_id(contests[0].id)
                viaduct_user_group_exists = True
            except ResourceNotFoundException:
                pass
    except (ApplicationException, requests.RequestException):
        pass

    return ServerCheckResult(
        can_reach_api=can_reach_api,
        domjudge_version=domjudge_version,
        can_login_with_admin_user=can_login_with_admin_user,
        can_login_with_admin_user_nonapi=can_login_with_admin_user_nonapi,
        admin_user_has_admin_role=admin_user_has_admin_role,
        num_contests=num_contests,
        viaduct_user_group_exists=viaduct_user_group_exists,
        viaduct_user_group_id=viaduct_user_group_id,
    )


def get_all_contests() -> List[Contest]:
    with _create_admin_session() as session:
        result = _execute_get_request("/contests", session=session)

    contests: List[Contest] = ContestSchema(many=True, unknown=EXCLUDE).loads(
        result.text
    )

    contests.sort(key=lambda c: c.start_time, reverse=True)
    return [
        contest
        for contest in contests
        if contest.shortname.startswith(VIADUCT_CONTEST_SHORTNAME_PREFIX)
    ]


def get_all_contest_settings() -> Dict[int, DOMjudgeContestSettings]:
    contest_settings = domjudge_repository.find_all_contest_settings()
    return {settings.contest_id: settings for settings in contest_settings}


def get_contest_by_id(contest_id: int) -> Contest:
    with _create_admin_session() as session:
        result = _execute_get_request(f"/contests/{contest_id}", session=session)

    contest: Contest = ContestSchema(unknown=EXCLUDE).loads(result.text)
    if not contest.shortname.startswith(VIADUCT_CONTEST_SHORTNAME_PREFIX):
        raise ResourceNotFoundException("domjudge_contest", contest_id)

    return contest


def contest_has_started(contest: Contest) -> bool:
    return contest.start_time <= dt.datetime.now(tz=dt.timezone.utc)


def get_contest_teams(contest: Contest) -> List[Team]:
    with _create_admin_session() as session:
        result = _execute_get_request(f"/contests/{contest.id}/teams", session=session)

    teams_result: List[Team] = TeamSchema(many=True, unknown=EXCLUDE).loads(result.text)

    teams = []
    for team in teams_result:
        team_name = team.name
        match = VIA_USER_TEAM_REGEX.match(team_name)
        if match:
            user = user_service.find_by_id(int(match.group("user_id")))
            if user:
                teams.append(Team(team.id, user.name, team.group_ids, user))
            else:
                teams.append(team)
        else:
            teams.append(team)

    return teams


def get_contest_scoreboard(
    contest: Contest,
    public: bool = True,
    include_rows_without_submissions: bool = True,
) -> Scoreboard:
    if public and not contest_has_started(contest):
        return []

    viaduct_group_id = _get_viaduct_group_id(contest.id)

    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/scoreboard",
            params=dict(public=True, category=viaduct_group_id),
            session=session,
        )

    scoreboard: Scoreboard = ScoreboardRowSchema(many=True, unknown=EXCLUDE).load(
        result.json()["rows"]
    )

    if not include_rows_without_submissions:
        return [
            row
            for row in scoreboard
            if any(
                problem.num_pending + problem.num_judged > 0 for problem in row.problems
            )
        ]

    return scoreboard


def get_contest_problems(
    contest: Contest,
    public: bool = True,
) -> List[Problem]:
    if public and not contest_has_started(contest):
        return []

    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/problems", session=session
        )

    problems: List[Problem] = ProblemSchema(many=True, unknown=EXCLUDE).loads(
        result.text
    )
    problems.sort(key=lambda p: p.label)

    return problems


def get_contest_languages(contest: Contest) -> List[Language]:
    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/languages", session=session
        )

    return LanguageSchema(many=True, unknown=EXCLUDE).loads(result.text)


def get_contest_language_by_id(contest: Contest, language_id: str) -> Language:
    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/languages/{language_id}", session=session
        )
    return LanguageSchema(unknown=EXCLUDE).loads(result.text)


def get_contest_submissions(contest: Contest) -> List[Submission]:
    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/submissions", session=session
        )
        submissions: List[Submission] = SubmissionSchema(
            many=True, unknown=EXCLUDE
        ).loads(result.text)

        result = _execute_get_request(
            f"/contests/{contest.id}/judgements", session=session
        )
        judgements_result: List[Judgement] = JudgementSchema(
            many=True, unknown=EXCLUDE
        ).loads(result.text)

    judgements = {
        j.submission_id: j.judgement_type_id for j in judgements_result if j.valid
    }

    submissions = [
        Submission(
            s.id, s.time, s.team_id, s.problem_id, s.language_id, judgements.get(s.id)
        )
        for s in submissions
    ]

    return submissions


def get_contest_settings(contest: Contest) -> Optional[DOMjudgeContestSettings]:
    return domjudge_repository.find_contest_settings_by_contest_id(contest.id)


def _ensure_settings_for_contest(contest: Contest) -> DOMjudgeContestSettings:
    if settings := get_contest_settings(contest):
        return settings
    else:
        return domjudge_repository.create_contest_settings_for_contest(contest.id)


def set_contest_banner(contest: Contest, file: FileStorage):
    settings = _ensure_settings_for_contest(contest)
    old_banner_file: Optional[File] = None

    if settings.banner_file:
        old_banner_file = settings.banner_file

    banner_file = file_service.add_file(FileCategory.DOMJUDGE_BANNER, file)
    settings.banner_file = banner_file

    domjudge_repository.save_contest_settings(settings)

    if old_banner_file:
        file_service.delete_file(old_banner_file)


def delete_contest_banner(contest: Contest):
    settings = get_contest_settings(contest)

    if settings and settings.banner_file:
        banner_file = settings.banner_file
        settings.banner_file_id = None
        domjudge_repository.save_contest_settings(settings)
        file_service.delete_file(banner_file)


def save_contest_settings(contest: Contest, banner_url: Optional[str] = None):
    settings = _ensure_settings_for_contest(contest)

    if banner_url == "":
        banner_url = None
    settings.banner_url = banner_url
    domjudge_repository.save_contest_settings(settings)


def get_problem_by_id(
    contest: Contest,
    problem_id: int,
    public: bool = True,
) -> Problem:
    if public and not contest_has_started(contest):
        raise ResourceNotFoundException("problem", problem_id)

    # Need to use authentication due to a bug in DOMjudge.
    # This route without authentication returns status code 500.
    with _create_admin_session() as session:
        result = _execute_get_request(
            f"/contests/{contest.id}/problems/{problem_id}", session=session
        )
        problem: Problem = ProblemSchema(unknown=EXCLUDE).loads(result.text)

        return problem


def get_problem_text(problem: Problem) -> ProblemText:
    with _create_admin_session(nonapi=True) as session:
        return _get_problem_text_internal(problem.id, session)


def _get_problem_text_internal(
    problem_id: int, session: requests.Session
) -> ProblemText:
    url = f"/jury/problems/{problem_id}/text"

    try:
        result = _execute_get_request(
            url, use_api_base_url=False, allow_redirects=False, session=session
        )
    except ApplicationException as ex:
        response: requests.Response = ex.data["response"]
        if response.status_code == 400 and "text has unknown type" in response.text:
            raise ProblemTextNotFoundException(url, "")

        raise

    return ProblemText(
        data=result.content,
        content_type=result.headers.get("Content-Type"),
        content_disposition=result.headers.get("Content-Disposition"),
    )


def _ensure_team_for_user(user: User, contest: Contest):
    with _create_user_session(user) as session:
        try:
            _get_current_user_info(session)

        except AuthorizationException:
            teams_tsv_buffer = StringIO()
            accounts_tsv_buffer = StringIO()

            teams_writer = csv.writer(teams_tsv_buffer, delimiter="\t")

            accounts_writer = csv.writer(accounts_tsv_buffer, delimiter="\t")

            viaduct_group_id = _get_viaduct_group_id(contest.id)

            teams_writer.writerow(["teams", 2])
            teams_writer.writerow(
                [
                    get_domjudge_team_id(user),  # Team number
                    user.id,  # External ID
                    viaduct_group_id,  # Group ID
                    _get_domjudge_team_name(user),  # Team name
                    "University of Amsterdam",  # Institution name
                    "UvA",  # Institution short name
                    "NLD",  # Country code
                    "UvA",  # Institution code - Needed since otherwise
                    # a new affiliation is created for every new user
                ]
            )
            teams_tsv = teams_tsv_buffer.getvalue()

            accounts_writer.writerow(["accounts", 1])
            accounts_writer.writerow(
                [
                    "team",  # Account type
                    user.name,  # Name
                    _get_domjudge_username(user),  # Username
                    app.config["DOMJUDGE_USER_PASSWORD"],  # Password
                ]
            )
            accounts_tsv = accounts_tsv_buffer.getvalue()

            with _create_admin_session() as admin_session:
                _execute_post_request(
                    "/users/teams", files={"tsv": teams_tsv}, session=admin_session
                )
                _execute_post_request(
                    "/users/accounts",
                    files={"tsv": accounts_tsv},
                    session=admin_session,
                )


def submission_filename_has_correct_extension(
    filename: str, language: Language
) -> bool:
    if language.filter_compiler_files:
        extension = PurePath(filename).suffix
        if extension == "" or extension[1:] not in language.extensions:
            return False

    return True


def submit_problem(
    user: User,
    contest: Contest,
    problem: Problem,
    language: Language,
    file: FileStorage,
):
    _ensure_team_for_user(user, contest)

    filename: str
    if file.filename:

        # Sanitize filename to match DOMjudge's allowed format
        filename = SUBMIT_FILENAME_ILLEGAL_CHARS_REGEX.sub("_", file.filename)
        if not SUBMIT_FILENAME_ALLOWED_FIRST_CHAR_REGEX.match(filename[0]):
            filename = "S_" + filename
    else:
        filename = f"submission.{language.extensions[0]}"

    content = file.read()

    with _create_user_session(user) as session:
        try:
            _execute_post_request(
                f"/contests/{contest.id}/submissions",
                data={"problem": problem.id, "language": language.id},
                files={"code[]": (filename, content)},
                session=session,
            )
        except ResourceNotFoundException:
            # 404 means that the contest is not currently active
            raise ContestNotActiveException(f"Contest {contest.id} is not active")


def check_contest(contest: Contest) -> ContestCheckResult:
    problems = get_contest_problems(contest)

    problems_without_text = []
    problems_without_color = []

    with _create_admin_session() as session:
        problems_raw = _execute_get_request(
            f"/contests/{contest.id}/problems", session=session
        ).json()

    problem_ids_without_color = {
        int(p["id"]) for p in problems_raw if "rgb" not in p.keys()
    }

    with _create_admin_session(nonapi=True) as session:
        for problem in problems:
            try:
                _get_problem_text_internal(problem.id, session)
            except ProblemTextNotFoundException:
                problems_without_text.append(problem)

            if problem.id in problem_ids_without_color:
                problems_without_color.append(problem)

        return ContestCheckResult(
            contest, problems, problems_without_text, problems_without_color
        )


def color_to_rgb(hex_color: Optional[str]) -> Optional[Tuple[int, int, int]]:
    if not hex_color:
        return None

    match = HEX_COLOR_REGEX.match(hex_color)
    if not match:
        return None

    digits = match.group(1)
    if len(digits) == 3:
        digits = "".join(2 * digit for digit in digits)

    return int(digits[0:2], 16), int(digits[2:4], 16), int(digits[4:], 16)
