from datetime import date
from typing import List

from werkzeug.datastructures import FileStorage

from app.enums import FileCategory
from app.exceptions.base import BusinessRuleException
from app.models.course import Course
from app.models.examination import Examination
from app.repository import examination_repository, model_service
from app.service import course_service, file_service


def find_all_examinations_by_course_id(course_id: int) -> List[Examination]:
    return examination_repository.get_all_examinations_by_course_id(course_id)


def add_examination(
    exam_date: date,
    comment: str,
    course_id: int,
    test_type,
) -> Examination:
    course = model_service.get_by_id(Course, course_id)

    if not course_service.course_available(course, exam_date):
        raise BusinessRuleException(f"Course unavailable on {exam_date.isoformat()}")

    exam = examination_repository.create_examination()
    exam.date = exam_date
    exam.comment = comment
    exam.course = course
    exam.test_type = test_type

    model_service.save(exam)

    return exam


def update_examination(
    exam: Examination,
    exam_date: date,
    comment: str,
    course_id: int,
    test_type: str,
) -> Examination:
    course = model_service.get_by_id(Course, course_id)
    if not course_service.course_available(course, exam_date):
        raise BusinessRuleException(f"Course unavailable on {exam_date.isoformat()}")

    exam.date = exam_date
    exam.comment = comment
    exam.course = course
    exam.test_type = test_type

    model_service.save(exam)

    return exam


def delete_examination(examination: Examination):
    model_service.delete(examination)

    if examination.examination_file is not None:
        file_service.delete_file(examination.examination_file)
    if examination.answers_file is not None:
        file_service.delete_file(examination.answers_file)


def count_examinations_by_course(course: Course) -> int:
    exams = examination_repository.get_all_examinations_by_course_id(course.id)
    return len(exams)


def save_exam(exam: Examination, exam_filestorage: FileStorage):
    old_exam_file = exam.examination_file

    exam_file = file_service.add_file(FileCategory.EXAMINATION, exam_filestorage)

    exam.examination_file = exam_file
    model_service.save(exam)

    if old_exam_file is not None:
        file_service.delete_file(old_exam_file)


def save_answers(exam: Examination, exam_filestorage: FileStorage):
    old_answers_file = exam.answers_file

    exam_file = file_service.add_file(FileCategory.EXAMINATION, exam_filestorage)

    exam.answers_file = exam_file
    model_service.save(exam)

    if old_answers_file is not None:
        file_service.delete_file(old_answers_file)
