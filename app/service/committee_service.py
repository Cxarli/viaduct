from typing import Optional, List, Tuple

from werkzeug.datastructures import FileStorage

from app.api.schema import MultilangStringDict, PageSearchParameters
from app.enums import FileCategory
from app.exceptions.base import (
    ResourceNotFoundException,
    BusinessRuleException,
    ValidationException,
)
from app.models.committee import Committee, CommitteeTag
from app.models.page import Page, PageRevision
from app.models.user import User
from app.repository import (
    committee_repository,
    user_repository,
    group_repository,
    page_repository,
)
from app.service import file_service
from app.utils.pagination import Pagination


def find_committee_by_page(page: Page) -> Optional[Committee]:
    return committee_repository.find_committee_by_page_id(page.id)


def find_committee_members(committee: Committee) -> List[User]:
    return list(committee.group.users)


def create_committee(
    name: MultilangStringDict,
    coordinator_id: int,
    group_id: int,
    page_id: int,
    coordinator_interim: bool,
    open_new_members: bool,
    tags: List[dict],
    pressure: int,
):
    _check_params(coordinator_id, group_id, page_id, pressure)

    if committee_repository.find_committee_by_page_id(page_id):
        raise BusinessRuleException("A committee is already linked to the page")

    return committee_repository.create(
        name,
        coordinator_id,
        group_id,
        page_id,
        coordinator_interim,
        open_new_members,
        tags,
        pressure,
    )


def edit_committee(
    committee: Committee,
    name: MultilangStringDict,
    coordinator_id: int,
    group_id: int,
    page_id: int,
    coordinator_interim: bool,
    open_new_members: bool,
    tags: List[dict],
    pressure: int,
    description: MultilangStringDict,
):
    _check_params(coordinator_id, group_id, page_id, pressure)

    committee_using_page = committee_repository.find_committee_by_page_id(page_id)
    if committee_using_page and committee_using_page.id != committee.id:
        raise BusinessRuleException("Another committee is already linked to that page")

    committee_repository.edit(
        committee,
        name,
        coordinator_id,
        group_id,
        page_id,
        coordinator_interim,
        open_new_members,
        tags,
        pressure,
        description,
    )


def _check_params(coordinator_id, group_id, page_id, pressure):
    if not user_repository.find_by_id(coordinator_id):
        raise ResourceNotFoundException("User", coordinator_id)
    if not group_repository.find_by_id(group_id):
        raise ResourceNotFoundException("Group", group_id)
    if not page_repository.find_page_by_id(page_id):
        raise ResourceNotFoundException("Page", page_id)
    if pressure < 0 or pressure > 5:
        raise BusinessRuleException("Pressure not between 0 and 5")


def get_all_committees_with_page_revisions() -> List[Tuple[Committee, PageRevision]]:
    return committee_repository.get_all_committees_with_page_revisions()


def get_all_committees() -> List[Committee]:
    return committee_repository.get_all_committees()


def paginated_search_all_committees(pagination: PageSearchParameters) -> Pagination:
    return committee_repository.paginated_search_all_committees(pagination)


def delete_committee(committee: Committee):
    committee_repository.delete_committee(committee)


def set_committee_picture(committee: Committee, file: FileStorage):
    if not file.mimetype.startswith("image/"):
        raise ValidationException("Invalid filetype for committee picture")

    picture_file = file_service.add_file(FileCategory.COMMITTEE_PICTURE, file)

    old_picture_id = committee.picture_file_id

    committee_repository.set_committee_picture(committee, picture_file.id)

    if old_picture_id:
        old_picture = file_service.get_file_by_id(old_picture_id)
        file_service.delete_file(old_picture)


def get_all_tags() -> List[CommitteeTag]:
    return committee_repository.get_all_tags()
