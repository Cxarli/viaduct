import hashlib
import urllib.parse
from datetime import date, datetime, timedelta
from pytz import timezone
from typing import Any, Dict, List, Optional, Tuple

import bcrypt
from flask import url_for
from flask_sqlalchemy import Pagination

from app import db
from app.api.schema import PageSearchParameters
from app.enums import FileCategory, ProgrammeType
from app.exceptions.base import (
    AuthorizationException,
    BusinessRuleException,
    RateLimitedException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.education import Education
from app.models.user import User
from app.repository import page_repository, user_repository
from app.service import (
    copernica_service,
    file_service,
    google_service,
    mail_service,
    mailinglist_service,
    oauth_service,
    user_mailing_service,
)
from app.service.membership_service import set_membership_status
from app.task import copernica
from app.task.mail import MailCommand

LOGIN_RETRY_TIMEOUT = timedelta(milliseconds=500)


def set_password(user_id, password):
    """Set the new password for user with id."""
    password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
    user = get_user_by_id(user_id)
    user.password = password
    user_repository.save(user)

    oauth_service.revoke_user_tokens_by_user_id(user_id)
    return user


def find_user_by_email(email):
    """Retrieve the user by email or return None."""
    return user_repository.find_user_by_email(email)


def get_user_by_email(email):
    """Retrieve the user by email, throw error if not found."""
    user = find_user_by_email(email)
    if not user:
        raise ResourceNotFoundException("user", email)
    return user


def get_user_by_id(user_id: int) -> User:
    """Retrieve the user by id, throw error if not found."""
    user = find_by_id(user_id)
    if not user:
        raise ResourceNotFoundException("user", user_id)
    return user


def get_users_by_ids(user_ids: List[int]) -> List[User]:
    return user_repository.find_by_ids(user_ids)


def find_by_id(user_id: int) -> Optional[User]:
    """Retrieve the user or return None."""
    return user_repository.find_by_id(user_id)


def find_user_by_student_id(student_id, needs_confirmed=True):
    """Retrieve the user or return None."""
    return user_repository.find_user_by_student_id(student_id, needs_confirmed)


def get_user_by_student_id(student_id, needs_confirmed=True):
    """Retrieve the user by student id, throw error if not found."""
    user = find_user_by_student_id(student_id, needs_confirmed)
    if not user:
        raise ResourceNotFoundException("user", student_id)

    return user


def find_by_copernica_id(copernica_id: int) -> User:
    return user_repository.find_user_by_copernica_id(copernica_id)


def get_all_users_with_unconfirmed_student_id(student_id):
    """
    Retrieve all users with an unconfirmed student id.

    throw error if not found.
    """

    users = user_repository.find_all_users_with_unconfirmed_student_id(student_id)

    if not users:
        raise ResourceNotFoundException("users", student_id)

    return users


def set_confirmed_student_id(user, student_id):
    # Check if there is not another account with the same student ID
    # that is already confirmed

    other_user = find_user_by_student_id(student_id)
    if other_user is not None and other_user != user:
        raise BusinessRuleException("Student ID already linked to other user.")

    # Find all users with the same student ID (unconfirmed)
    # and set it to None
    other_users = user_repository.find_all_users_with_unconfirmed_student_id(student_id)

    for other_user in other_users:
        remove_student_id(other_user)

    # Set the confirmed student ID of the user
    user.student_id = student_id
    user.student_id_confirmed = True

    # Save everything at once
    user_repository.save_all(other_users + [user])


def set_unconfirmed_student_id(user, student_id):
    user.student_id = student_id
    user.student_id_confirmed = False

    user_repository.save(user)


def remove_student_id(user):
    user.student_id = ""
    user.student_id_confirmed = False

    user_repository.save(user)


def set_user_email(user: User, email: str):
    if user.email == email:
        return

    existing_user = user_repository.find_user_by_email(email)
    if existing_user:
        raise BusinessRuleException("Another user is using this e-mail.")

    old_email = user.email

    for group in user.groups:
        if not group.maillist:
            continue

        google_service.remove_email_from_google_group(group, old_email)
        google_service.add_email_to_google_group(group, email)

    user.email = email
    user_repository.save(user)

    # TODO: allow having users specify their timezone
    tz = timezone('Europe/Amsterdam')
    date = datetime.now().astimezone(tz).strftime("%d/%m/%Y %H:%M:%S")

    # send change notification to old address
    command = MailCommand(old_email)
    command.with_template(
        "email_changed",
        locale="en",  # TODO: user.locale ?
        old_email=old_email,
        new_email=email,
        date=date,
    )
    mail_service.send_mail(command)

    # send change notification to new address
    command = MailCommand(email)
    command.with_template(
        "email_changed",
        locale="en",  # TODO: user.locale ?
        old_email=old_email,
        new_email=email,
        date=date,
    )
    mail_service.send_mail(command)


def set_user_properties(
    user: User,
    email: Optional[str] = None,
    first_name: Optional[str] = None,
    last_name: Optional[str] = None,
    phone_nr: Optional[str] = None,
    city: Optional[str] = None,
    zip: Optional[str] = None,
    address: Optional[str] = None,
    iban: Optional[str] = None,
) -> None:
    if email is not None:
        set_user_email(user, email)
    if first_name is not None:
        user.first_name = first_name
    if last_name is not None:
        user.last_name = last_name
    if phone_nr is not None:
        user.phone_nr = phone_nr
    if city is not None:
        user.city = city
    if zip is not None:
        user.zip = zip
    if address is not None:
        user.address = user.address
    if iban is not None:
        user.iban = iban

    user_repository.save(user)
    return None


def set_admin_properties(
    user: User,
    member_of_merit: Tuple[Optional[date], bool],
    has_paid: Optional[bool] = None,
    favourer: Optional[bool] = None,
    disabled: Optional[bool] = None,
) -> None:
    """
    Update properties of a user that can only be set by admins.

    Since member_of_merit has None as valid value, is uses a tuple to
    indicate if updates are necessary
    """

    if has_paid is not None:
        set_membership_status(user, has_paid)

    member_of_merit_value, update_member_of_merit = member_of_merit
    if update_member_of_merit:
        user.member_of_merit_date = member_of_merit_value

    if favourer is not None:
        user.favourer = favourer

    if disabled is not None:
        user.disabled = disabled

    user_repository.save(user)


def find_members():
    """Find all users which are marked as member."""
    return user_repository.find_members()


def find_member_of_merit() -> List[Dict[str, Any]]:
    rv = []
    users = user_repository.find_members_of_merit(db.session)
    for row in users:
        quoted_name = urllib.parse.quote_plus(row.name)
        page = page_repository.find_page_by_path(f"ereleden/{quoted_name}")
        rv.append(
            {
                "id": row.id,
                "first_name": row.first_name,
                "last_name": row.last_name,
                "member_of_merit_date": row.member_of_merit_date,
                "path": page.path if page else None,
            }
        )
    return rv


def find_all_user_ids() -> List[int]:
    return user_repository.find_all_user_ids()


def paginated_search_all_users(pagination: PageSearchParameters) -> Pagination:
    """Get all users with search and pagination."""
    return user_repository.paginated_search_all_users(pagination)


def get_user_by_login(email, password):
    user = user_repository.find_user_by_email(email)
    if not user:
        raise ResourceNotFoundException("user", email)

    if user.disabled:
        raise AuthorizationException("User is disabled.")

    if (
        user.last_login_attempt is not None
        and datetime.now() - user.last_login_attempt < LOGIN_RETRY_TIMEOUT
    ):
        raise RateLimitedException("Cannot login at this time.")

    if not validate_password(user, password):
        user.last_login_attempt = datetime.now()
        user_repository.save(user)

        raise ValidationException("Invalid password.")

    return user


def validate_password(user: User, password: str) -> bool:
    if user.password is None:
        return False

    return bcrypt.checkpw(password, user.password)


def user_has_avatar(user: User):
    if user.is_anonymous:
        return False

    return user.avatar_file_id is not None


def remove_avatar(user_id):
    user = get_user_by_id(user_id)

    _file = file_service.get_file_by_id(user.avatar_file_id)
    user.avatar_file_id = None

    user_repository.save(user)

    file_service.delete_file(_file)


def set_avatar(user_id, file_data):
    """
    Upload the new avatar.

    Checks if the file type is allowed if so removes any
    previous uploaded avatars.
    """
    user = get_user_by_id(user_id)

    # Remove old avatar
    if user.avatar_file_id is not None:
        old_file = file_service.get_file_by_id(user.avatar_file_id)
        user.avatar_file_id = None
    else:
        old_file = None

    _file = file_service.add_file(
        FileCategory.USER_AVATAR,
        file_data,
        thumbnail=True,
    )

    user.avatar_file_id = _file.id

    if old_file:
        file_service.delete_file(old_file)

    user_repository.save(user)


def register_new_user(
    email,
    first_name,
    last_name,
    student_id,
    educations,
    birth_date,
    study_start,
    phone_nr,
    address,
    zip_,
    city,
    country,
    locale,
    password=None,
    link_student_id=False,
):
    if find_user_by_email(email) is not None:
        raise BusinessRuleException(
            "A user with the same email address already exists."
        )

    user = user_repository.create_user()

    user.email = email

    # Some users are registered without a password.
    if password is not None:
        user.password = bcrypt.hashpw(password, bcrypt.gensalt())

    user.first_name = first_name
    user.last_name = last_name
    user.student_id = student_id
    user.educations = educations
    user.birth_date = birth_date
    user.study_start = study_start
    user.mailinglists = mailinglist_service.get_default_mailinglists()
    user.phone_nr = phone_nr
    user.address = address
    user.zip = zip_
    user.city = city
    user.country = country
    user.locale = locale

    users = [user]

    if link_student_id:
        user.student_id_confirmed = True
        unconfirmed_users = user_repository.find_all_users_with_unconfirmed_student_id(
            user.student_id
        )

        for u in unconfirmed_users:
            remove_student_id(u)
            users.append(u)

    user_repository.save_all(users)

    copernica.create_user.delay(user.id)

    return user


def user_is_master_student(user):
    """
    Returns whether the user is a master student.

    Check whether at least one of his/her study programmes is
    of type ProgrammeType.MASTER.

    :param user: The user
    :return: True if he/she is a master student, False otherwise
    """

    for education in user.educations:
        if education.programme_type == ProgrammeType.MASTER:
            return True

    return False


def get_user_sorted_study_programmes(user: User) -> List[Education]:
    """
    Return study programmes of a user in the order Master -> Bachelor -> Minor.

    :param user: The user to retrieve sorted study programmes from
    :return: A list of Educations sorted by the priority as specified.
    """

    study_programme_order = {
        ProgrammeType.MASTER: 0,
        ProgrammeType.BACHELOR: 1,
        ProgrammeType.MINOR: 2,
        ProgrammeType.OTHER: 3,
    }

    return sorted(
        user.educations, key=lambda e: study_programme_order[e.programme_type]
    )


def get_user_gravatar_url(user: User) -> str:
    if user.is_anonymous:
        email = ""
    else:
        email = user.email

    # Set default values gravatar
    default = "identicon"
    size = 100

    # Construct the url
    return (
        "https://www.gravatar.com/avatar/"
        + hashlib.md5(email.lower().encode("utf-8")).hexdigest()
        + "?"
        + urllib.parse.urlencode({"d": default, "s": str(size)})
    )


def anonymize(user: User):
    # Prepare the mail command, as after anonymizing the user it will not be
    # available.
    command = MailCommand("bestuur@svia.nl")
    command.with_template(
        "anonymize_user",
        locale="en",
        url=url_for("user.view_single_user", user=user),
        anonymized_user=user,
        date=datetime.today(),
    )

    user.disabled = True
    user.password = None
    user.email = f"{user.id}@anonymized.svia.nl"

    user.first_name = "Anonymized"
    user.last_name = "User"

    user.address = ""
    user.zip = ""
    user.city = ""
    user.country = ""

    user.phone_nr = ""

    user.birth_date = None
    user.study_start = None

    remove_student_id(user)

    user.educations = []
    user_mailing_service.set_mailing_list_subscriptions(user, [], False)
    user.copernica_id = None

    user.paid_date = None
    user.member_of_merit_date = None
    user.favourer = False

    for group in user.groups:
        google_service.remove_email_from_google_group(group, user.email)

    user.groups = []

    if user_has_avatar(user):
        remove_avatar(user.id)

    if user.copernica_id:
        # TODO Move to worker task.
        copernica_service.delete_profile(user.copernica_id)
        user.copernica_id = None

    oauth_service.revoke_user_tokens_by_user_id(user.id)

    user_repository.save(user)

    mail_service.send_mail(command)

    # TODO Create API call to remove via user in Pretix (update anonymize.htm)
    # https://gitlab.com/studieverenigingvia/ict/pretix-via/issues/18
