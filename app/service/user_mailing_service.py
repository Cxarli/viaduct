import logging
import celery.result
from typing import Optional
from app.models.user import User
from app.models.mailinglist_model import MailingList
from app.repository import mailinglist_repository, user_repository
from typing import Iterable, List, overload, Literal, Set
from app.task import copernica

_logger = logging.getLogger(__name__)


@overload
def set_mailing_list_subscriptions(
    user: User, subscribed_mailing_lists: Iterable[MailingList]
) -> celery.result.AsyncResult:
    """Typing definition for dependent return type, see implementation"""
    ...


@overload
def set_mailing_list_subscriptions(
    user: User,
    subscribed_mailing_lists: Iterable[MailingList],
    update_copernica: Literal[True],
) -> celery.result.AsyncResult:
    """Typing definition for dependent return type, see implementation"""
    ...


@overload
def set_mailing_list_subscriptions(
    user: User,
    subscribed_mailing_lists: Iterable[MailingList],
    update_copernica: Literal[False],
) -> None:
    """Typing definition for dependent return type, see implementation"""
    ...


def set_mailing_list_subscriptions(
    user: User,
    subscribed_mailing_lists: Iterable[MailingList],
    update_copernica: bool = True,
) -> Optional[celery.result.AsyncResult]:
    """
    Set users mailinglist and optionally update Copernica using async task.

    Only returns an AsyncResult when the task is queued. The @overload
    functions above are to correctly encode the dependent return type for mypy.
    """

    user.mailinglists = list(subscribed_mailing_lists)

    user_repository.save(user)

    if update_copernica:
        return copernica.update_user.delay(user.id)
    return None


def set_mailing_list_subscribed(
    user: User,
    mailing_list: MailingList,
    subscribed: bool,
) -> celery.result.AsyncResult:
    subscribed_mailing_lists: Set[MailingList] = set(user.mailinglists)
    if subscribed:
        subscribed_mailing_lists.add(mailing_list)
    else:
        subscribed_mailing_lists.remove(mailing_list)

    return set_mailing_list_subscriptions(user, subscribed_mailing_lists)


def get_user_subscribed_mailing_lists(user: User) -> List[MailingList]:
    return mailinglist_repository.find_user_subscribed_mailinglists(user)
