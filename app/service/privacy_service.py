import json
from typing import NamedTuple

from app.models.setting_model import Setting
from app.repository import setting_repository

SETTING_KEY = "PRIVACY_POLICY"


class PrivacyPolicies(NamedTuple):
    url_nl: str
    url_en: str


def find_policies() -> PrivacyPolicies:
    s = setting_repository.find_by_key(SETTING_KEY)
    if not s:
        return PrivacyPolicies(
            url_nl="/static/via_privacy_policy_nl.pdf",
            url_en="/static/via_privacy_policy_en.pdf",
        )
    dict = json.loads(s.value)
    return PrivacyPolicies(dict["url_nl"], dict["url_en"])


def set_privacy_policy(policies: PrivacyPolicies):
    s = setting_repository.find_by_key(SETTING_KEY)
    if not s:
        s = Setting()
        s.key = SETTING_KEY
    s.value = json.dumps({"url_nl": policies.url_nl, "url_en": policies.url_en})
    setting_repository.save(s)
