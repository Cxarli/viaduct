from functools import wraps
from typing import Any, Callable, Dict, Optional
from urllib.parse import urljoin

from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required
from werkzeug.datastructures import FileStorage

from app import app
from app.decorators import require_role
from app.exceptions.domjudge import (
    ProblemTextNotFoundException,
    ContestNotActiveException,
)
from app.roles import Roles
from app.service import domjudge_service, role_service
from app.service.domjudge_service import Language, Problem, ScoreboardProblem, Team
from app.views.file import send_picture

blueprint = Blueprint("domjudge", __name__, url_prefix="/domjudge")


def domjudge_enabled(f: Callable[..., Any]) -> Callable[..., Any]:
    @wraps(f)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        if not domjudge_service.domjudge_integration_enabled():
            flash(
                _(
                    "The DOMjudge module is currently disabled. It will be "
                    "enabled during an event which makes use the module."
                ),
                "danger",
            )
            return abort(403)

        return f(*args, **kwargs)

    return wrapper


@blueprint.route("/")
@domjudge_enabled
def contest_list():
    contests = domjudge_service.get_all_contests()

    return render_template("domjudge/list.htm", contests=contests)


@blueprint.route("/contest/<int:contest_id>/")
@domjudge_enabled
def contest_view(contest_id: int):
    admin = role_service.user_has_role(current_user, Roles.DOMJUDGE_ADMIN)

    fullscreen = "fullscreen" in request.args
    embed = "embed" in request.args

    contest = domjudge_service.get_contest_by_id(contest_id)
    contest_settings = domjudge_service.get_contest_settings(contest)

    scoreboard = domjudge_service.get_contest_scoreboard(
        contest,
        public=not admin,
        include_rows_without_submissions=False,
    )
    problems = domjudge_service.get_contest_problems(
        contest,
        public=not admin,
    )
    teams = domjudge_service.get_contest_teams(contest)
    teams_dict: Dict[int, Team] = {team.id: team for team in teams}

    problems_classes: Dict[ScoreboardProblem, str] = {}

    for team in scoreboard:
        team.problems.sort(key=lambda p: p.label)

        for problem in team.problems:
            if problem.solved:
                if problem.first_to_solve:
                    _class = "domjudge-problem-solved-first-cell"
                else:
                    _class = "domjudge-problem-solved-cell"
            else:
                if problem.num_pending > 0:
                    _class = "domjudge-problem-judging-cell"
                elif problem.num_judged > 0:
                    _class = "domjudge-problem-incorrect-cell"
                else:
                    _class = "domjudge-problem-untried-cell"

            problems_classes[problem] = _class

    return render_template(
        "domjudge/view.htm",
        total_problems_amount=len(problems),
        link=admin,
        contest=contest,
        contest_settings=contest_settings,
        teams=teams_dict,
        problems=problems,
        scoreboard=scoreboard,
        problems_classes=problems_classes,
        fullscreen=fullscreen,
        embed=embed,
    )


@blueprint.route("/contest/<int:contest_id>/edit/")
@domjudge_enabled
@require_role(Roles.DOMJUDGE_ADMIN)
def contest_edit(contest_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)

    return render_template("vue_content.htm", title=f"Edit {contest.name}")


@blueprint.route("/contest/<int:contest_id>/users/")
@domjudge_enabled
@require_role(Roles.DOMJUDGE_ADMIN)
def contest_users(contest_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)

    return render_template("vue_content.htm", title=f"Contest {contest.name} users")


@blueprint.route("/contest/<int:contest_id>/problems/")
@domjudge_enabled
def contest_problems_list(contest_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)

    admin = role_service.user_has_role(current_user, Roles.DOMJUDGE_ADMIN)
    problems = domjudge_service.get_contest_problems(contest, public=not admin)

    return render_template(
        "domjudge/problem/list.htm",
        contest=contest,
        problems=problems,
    )


@blueprint.route("/contest/<int:contest_id>/check/")
@require_role(Roles.DOMJUDGE_ADMIN)
def contest_check(contest_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)
    contest_check_result = domjudge_service.check_contest(contest)
    return render_template("domjudge/contest_check.htm", result=contest_check_result)


@blueprint.route("contest/<int:contest_id>/problem/<int:problem_id>/")
@domjudge_enabled
def contest_problem_view(contest_id: int, problem_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)
    admin = role_service.user_has_role(current_user, Roles.DOMJUDGE_ADMIN)

    # Check if problem exists and belongs to the specified contest
    problem = domjudge_service.get_problem_by_id(contest, problem_id, public=not admin)

    try:
        problem_text = domjudge_service.get_problem_text(problem)
    except ProblemTextNotFoundException:

        flash(
            _(
                "This problem does not have an associated problem description. "
                "This is probably a mistake."
            ),
            "danger",
        )
        raise

    headers = {"Content-Type": problem_text.content_type}
    if problem_text.content_disposition:
        headers["Content-Disposition"] = problem_text.content_disposition

    return problem_text.data, headers


@blueprint.route(
    "/contest/<int:contest_id>/problem/<int:problem_id>/submit/",
    methods=["GET", "POST"],
)
@login_required
@domjudge_enabled
def contest_problem_submit(contest_id: int, problem_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)
    admin = role_service.user_has_role(current_user, Roles.DOMJUDGE_ADMIN)

    languages = domjudge_service.get_contest_languages(contest)
    problem = domjudge_service.get_problem_by_id(contest, problem_id, public=not admin)

    def show_error(message: str) -> str:
        flash(message, "danger")
        return render_template(
            "domjudge/problem/submit.htm",
            problem=problem,
            contest=contest,
            languages=languages,
        )

    if request.method == "POST":
        file: Optional[FileStorage] = request.files.get("file", None)
        language_id: Optional[str] = request.form.get("language", None)

        if file is None or not file.filename or file.filename == "":
            return show_error(_("No file uploaded."))

        if language_id is None:
            return show_error(_("Invalid language."))
        else:
            language: Optional[Language] = None

            for lang in languages:
                if lang.id == language_id:
                    language = lang
                    break

            if not language:
                return show_error(_("Invalid language."))

            if not domjudge_service.submission_filename_has_correct_extension(
                file.filename, language
            ):
                return show_error(
                    _(
                        "Invalid filename extension for language {}. "
                        + "Allowed extensions are: {}"
                    ).format(
                        language.name, ", ".join([f".{e}" for e in language.extensions])
                    )
                )

        try:
            domjudge_service.submit_problem(
                current_user, contest, problem, language, file
            )
        except ContestNotActiveException:
            return show_error(
                _(
                    "This contest is not active. "
                    "Submitting solutions is currently not possible."
                )
            )

        flash(_("Submission successful."), "success")

        return redirect(
            url_for("domjudge.contest_submissions_view_self", contest_id=contest_id)
        )
    else:
        return render_template(
            "domjudge/problem/submit.htm",
            problem=problem,
            contest=contest,
            languages=languages,
        )


@blueprint.route("/contest/<int:contest_id>/submissions/")
@login_required
@domjudge_enabled
def contest_submissions_view_self(contest_id: int):
    admin = role_service.user_has_role(current_user, Roles.DOMJUDGE_ADMIN)
    return render_contest_submissions_view(
        contest_id,
        team_id=domjudge_service.get_domjudge_team_id(current_user),
        admin=admin,
    )


@blueprint.route("/contest/<int:contest_id>/submissions/<int:team_id>/")
@login_required
@require_role(Roles.DOMJUDGE_ADMIN)
@domjudge_enabled
def contest_submissions_view(contest_id: int, team_id: int):
    return render_contest_submissions_view(contest_id, team_id=team_id, admin=True)


@blueprint.route("/contest/<int:contest_id>/submissions/all/")
@login_required
@require_role(Roles.DOMJUDGE_ADMIN)
@domjudge_enabled
def contest_submissions_view_all(contest_id: int):
    return render_contest_submissions_view(contest_id, view_all=True, admin=True)


def render_contest_submissions_view(
    contest_id: int,
    view_all: bool = False,
    team_id: Optional[int] = None,
    admin: bool = False,
):
    contest = domjudge_service.get_contest_by_id(contest_id)
    submissions = domjudge_service.get_contest_submissions(contest)
    languages = domjudge_service.get_contest_languages(contest)
    problems = domjudge_service.get_contest_problems(contest, public=not admin)
    teams = domjudge_service.get_contest_teams(contest)

    languages_dict: Dict[str, Language] = {lang.id: lang for lang in languages}
    problems_dict: Dict[int, Problem] = {problem.id: problem for problem in problems}
    teams_dict: Dict[int, Team] = {team.id: team for team in teams}

    submission_states = {
        "CE": _("Compiler Error"),
        "MLE": _("Memory limit"),
        "OLE": _("Output Limit"),
        "RTE": _("Run Error"),
        "TLE": _("Timelimit"),
        "WA": _("Wrong Answer"),
        "PE": _("Presentation error"),
        "NO": _("No Output"),
        "AC": _("Correct"),
    }

    submissions_data = []
    for s in sorted(submissions, key=lambda x: x.time, reverse=True):
        submission_team_id = s.team_id
        problem_id = s.problem_id

        team = teams_dict.get(submission_team_id)
        problem = problems_dict.get(problem_id)

        if not team or not problem:
            continue

        submission_info = {
            "id": s.id,
            "team": team.name,
            "problem": problem,
            "time": s.time,
            "team_id": submission_team_id,
            "language": languages_dict[s.language_id].name,
        }

        if not s.judgement_type_id:
            submission_info["result"] = _("Pending")
            submission_info["result_class"] = "domjudge-submission-blue"
        else:
            submission_info["result"] = submission_states[s.judgement_type_id]

            _class: str
            if s.judgement_type_id == "AC":
                _class = "green"
            elif s.judgement_type_id == "TLE" or s.judgement_type_id == "NO":
                _class = "orange"
            else:
                _class = "red"

            submission_info["result_class"] = f"domjudge-submission-{_class}"

        submissions_data.append(submission_info)

    return render_template(
        "domjudge/submissions.htm",
        view_all=view_all,
        urljoin=urljoin,
        team=team_id,
        domjudge_url=app.config["DOMJUDGE_URL"],
        admin=admin,
        contest=contest,
        submissions=submissions_data,
    )


@blueprint.route("/contest/<int:contest_id>/banner/")
def banner(contest_id: int):
    contest = domjudge_service.get_contest_by_id(contest_id)
    contest_settings = domjudge_service.get_contest_settings(contest)
    if not contest_settings:
        return abort(404)

    return send_picture(
        contest_settings.banner_file_id,
        f"domjudge_contest_{contest.id}_banner",
        "normal",
    )


@blueprint.route("/server-check/")
@require_role(Roles.DOMJUDGE_ADMIN)
def server_check():
    server_check_result = domjudge_service.check_server_communication()
    return render_template("domjudge/server_check.htm", result=server_check_result)
