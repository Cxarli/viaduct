from typing import Any, Optional

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_babel import _
from flask_login import current_user, login_required
from flask_wtf import FlaskForm

from app import db, get_locale
from app.decorators import require_role, response, response_headers
from app.exceptions.base import BusinessRuleException
from app.extensions import login_manager
from app.forms.user import (
    ChangePasswordForm,
    EditUserForm,
    EditUvALinkingForm,
    UnconfirmedStudentIDForm,
)
from app.models.user import User
from app.roles import Roles
from app.service import (
    activity_service,
    calendar_service,
    education_service,
    file_service,
    group_service,
    news_service,
    oauth_service,
    role_service,
    user_service,
)
from app.service.user_service import user_has_avatar
from app.task import copernica
from app.utils.google import HttpError
from app.views.calendar import MEETING_TYPES
from app.views.file import send_file_inline

blueprint = Blueprint("user", __name__)


@login_manager.user_loader
def load_user(user_id):
    # The hook used by the login manager to get the user from the database by
    # user ID.
    user = user_service.get_user_by_id(user_id)
    if user.disabled:
        return None
    return user


def view_profile_page(template: str, **kwargs: Any) -> str:
    return render_template(
        "user/profile/" + template, Roles=Roles, role_service=role_service, **kwargs
    )


@blueprint.route("/users/<user_self:user>/view/", methods=["GET"])
@login_required
def view_single_user(user: User):
    educations = map(
        lambda x: education_service.get_education_name(x, get_locale()),
        user.educations,
    )

    calendar_hash = calendar_service.get_calendar_hash(user.id)

    return view_profile_page(
        "view.htm", user=user, educations=educations, calendar_hash=calendar_hash
    )


@blueprint.route("/users/<user_self:user>/roles/", methods=["GET"])
@require_role(Roles.GROUP_PERMISSIONS)
def view_single_user_roles(user: User):
    user_roles_with_groups = role_service.user_roles_with_groups(user)
    user_groups_with_roles = role_service.user_groups_with_roles(user)
    return view_profile_page(
        "roles.htm",
        user=user,
        user_roles_with_groups=user_roles_with_groups,
        user_groups_with_roles=user_groups_with_roles,
    )


@blueprint.route("/users/<user_self:user>/avatar/remove/", methods=["DELETE"])
def remove_avatar(user: User):
    user_service.remove_avatar(user.id)
    return "", 200


@blueprint.route("/users/<user_self:user>/edit/", methods=["GET", "POST"])
def edit_user(user: User):
    """
    Edit for admins and users.

    User and form type are passed based on routes below.
    """
    form = EditUserForm(obj=user)
    student_id_form = UnconfirmedStudentIDForm(obj=user)

    user.has_avatar = user_service.user_has_avatar(user)

    def edit_page():
        is_admin = role_service.user_has_role(current_user, Roles.USER_WRITE)
        return view_profile_page(
            "personal.htm",
            title=_("Edit %s") % user.name if user.id else _("New user"),
            form=form,
            user=user,
            is_admin=is_admin,
            unconfirmed_student_id_form=student_id_form,
            get_education_name=education_service.get_education_name,
        )

    if form.validate_on_submit():
        try:
            user_service.set_user_email(user, form.email.data.strip())
        except BusinessRuleException:
            flash(_("A user with this e-mail address already exist."), "danger")
            return edit_page()
        except HttpError as e:
            if e.resp.status == 404:
                flash(
                    _(
                        "According to Google this email does not exist. "
                        "Please use an email that does."
                    ),
                    "danger",
                )
                return edit_page()
            raise e

        # Note: student id is updated separately.
        user.first_name = form.first_name.data.strip()
        user.last_name = form.last_name.data.strip()
        if form.locale.data:
            user.locale = form.locale.data
            if "lang" in session and user.id == current_user.id:
                del session["lang"]
        if role_service.user_has_role(current_user, Roles.USER_WRITE) and isinstance(
            form, EditUserForm
        ):
            user.alumnus = form.alumnus.data

        # Only update educations and student_id when user has not confirmed
        # their account using uva.
        if not user.student_id_confirmed:
            user.educations = student_id_form.educations.data
            user.student_id = student_id_form.student_id.data
        user.birth_date = form.birth_date.data
        user.study_start = form.study_start.data

        user.phone_nr = form.phone_nr.data.strip()
        user.address = form.address.data.strip()
        user.zip = form.zip.data.strip()
        user.city = form.city.data.strip()
        user.country = form.country.data.strip()

        db.session.add(user)
        db.session.commit()

        avatar = request.files.get("avatar")
        if avatar:
            user_service.set_avatar(user.id, avatar)

        copernica.update_user.delay(user.id)

        if user.id:
            flash(_("Profile succesfully updated"))
        else:
            flash(_("Profile succesfully created"))

        return redirect(url_for("user.edit_user", user=user))

    return edit_page()


@blueprint.route("/users/<user_self:user>/student-id-linking/", methods=["GET"])
def view_user_student_id_linking(user: User):
    can_write_users = role_service.user_has_role(current_user, Roles.USER_WRITE)

    return view_profile_page(
        "student_id_linking.htm",
        title=_("UvA account linking"),
        user=user,
        can_write_users=can_write_users,
        get_education_name=education_service.get_education_name,
    )


@blueprint.route(
    "/users/<user_self:user>/student-id-linking/edit/", methods=["GET", "POST"]
)
def edit_student_id_linking(user: User):
    form = EditUvALinkingForm(obj=user)

    # Fix student_id_confirmed not being set...
    if request.method == "GET":
        form.student_id_confirmed.data = user.student_id_confirmed

    def edit_page():
        return render_template("user/edit_student_id.htm", user=user, form=form)

    if form.validate_on_submit():
        if not form.student_id.data:
            user_service.remove_student_id(user)
        elif form.student_id_confirmed.data:
            other_user = user_service.find_user_by_student_id(form.student_id.data)

            if other_user is not None and other_user != user:
                error = _(
                    "The UvA account corresponding with this student ID "
                    "is already linked to another user "
                    "(%(name)s - %(email)s). Please unlink the account "
                    "from the other user first before linking it "
                    "to this user.",
                    name=other_user.name,
                    email=other_user.email,
                )

                form.student_id_confirmed.errors.append(error)

                return edit_page()

            user_service.set_confirmed_student_id(user, form.student_id.data)
        else:
            user_service.set_unconfirmed_student_id(user, form.student_id.data)

        flash(_("Student ID information saved."), "success")

        return redirect(url_for("user.view_user_student_id_linking", user=user))

    return edit_page()


@blueprint.route("/users/self/tfa/", methods=["GET"])
@login_required
def tfa():
    return view_profile_page("vue_content.htm", title=_("Two-factor authentication"))


@blueprint.route("/users/<user:user>/anonymize/", methods=["GET", "POST"])
@login_required
@require_role(Roles.USER_WRITE)
def anonymize(user: User):
    form = FlaskForm()
    if form.validate_on_submit():
        user_service.anonymize(user)
        return redirect(url_for("user.view_single_user", user=user))

    return render_template("user/anonymize.htm", user=user, form=form)


@blueprint.route("/users/<user_self:user>/password/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def change_password(user: User):
    form = ChangePasswordForm()

    if form.validate_on_submit():
        if user_service.validate_password(current_user, form.current_password.data):
            user_service.set_password(current_user.id, form.password.data)
            flash(_("Your password has successfully been changed."), "success")
        else:
            form.current_password.errors.append(
                _("Your current password does not match.")
            )
            return view_profile_page("password.htm", form=form, user=user), 400

    return view_profile_page("password.htm", form=form, user=user)


@blueprint.route("/users/", methods=["GET"])
@require_role(Roles.USER_WRITE)
def view():
    return render_template("vue_content.htm", title=_("Edit users"))


# We cannot use the UserSelfConverter here, as avatars have seperated permission checks.
# See test/views/test_avatar.py for response logic
@blueprint.route("/users/self/avatar/", methods=["GET"])
@blueprint.route("/users/<user:user>/avatar/", methods=["GET"])
def view_avatar(user: User = current_user):
    if not user_has_avatar(user):
        return redirect(user_service.get_user_gravatar_url(user))

    # User with news post has avatar publicly available on homepage.
    # Users with oauth clients have avatar publicly available in user applications.
    can_read = (
        any(
            news_service.get_news_by_user_id(user.id)
            + oauth_service.get_owned_clients_by_user_id(user.id)
        )
        or user.member_of_merit_date
    )

    # A user can always view his own avatar
    if current_user == user:
        can_read = True

    # group rights
    if role_service.user_has_role(
        current_user, Roles.USER_WRITE
    ) or role_service.user_has_role(current_user, Roles.ACTIVITY_WRITE):
        can_read = True

    if not can_read:
        return redirect(user_service.get_user_gravatar_url(user))

    avatar_file = file_service.get_file_by_id(user.avatar_file_id)

    filename = f"user_avatar_{user.id}.{avatar_file.extension}"

    return send_file_inline(avatar_file, filename)


@blueprint.route("/users/<user:user>/calendar/<string:calendar_hash>/", methods=["GET"])
@blueprint.route(
    "/users/<user:user>/calendar/<string:calendar_hash>/<meeting_type:types>/",
    methods=["GET"],
)
@response(content="calendar", name="via_calendar.ics")
def view_user_calendar(user: User, calendar_hash: str, types: Optional[list] = None):
    h = calendar_service.get_calendar_hash(user.id)
    if types is None:
        types = list(MEETING_TYPES)

    if h == calendar_hash:
        calendar = calendar_service.CalendarBuilder()
        calendar.set_user(user_service.get_user_by_id(user.id))
        return calendar.build_user(types)
    else:
        abort(403)


@blueprint.route("/users/<user_self:user>/mailinglist-subscriptions/", methods=["GET"])
def edit_user_mailinglist_subscriptions(user: User):
    return view_profile_page(
        "vue_content.htm", title=_("Mailinglist subscriptions"), user=user
    )


@blueprint.route("/users/<user_self:user>/activities/", methods=["GET"])
def view_user_activities(user: User):
    # Get all activity entrees from these forms, order by start_time of
    # activity.
    new_activities, old_activities = activity_service.get_upcoming_past_for_user(user)

    return view_profile_page(
        "activities.htm",
        user=user,
        new_activities=new_activities,
        old_activities=old_activities,
    )


@blueprint.route("/users/<user_self:user>/groups/", methods=["GET"])
def view_user_groups(user: User):
    groups = group_service.get_groups_for_user(user)

    can_write_groups = role_service.user_has_role(current_user, Roles.GROUP_WRITE)

    return view_profile_page(
        "groups.htm", user=user, groups=groups, can_write_groups=can_write_groups
    )


@blueprint.route("/users/<user_self:user>/admin/", methods=["GET"])
@login_required
@require_role(Roles.USER_WRITE)
def edit_user_admin(user: User):
    return view_profile_page(
        "vue_content.htm", title=_("User administrator properties"), user=user
    )


@blueprint.route("/users/<user_self:user>/applications/", methods=["GET"])
def view_user_applications(user: User):
    return view_profile_page(
        "vue_content.htm", title=_("Connected applications"), user=user
    )
