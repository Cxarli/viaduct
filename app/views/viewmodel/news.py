from flask import url_for
from flask_babel import gettext as _

from app.models.news import News


class NewsViewModel:
    def __init__(self, news_item: News) -> None:
        self.news_item = news_item

    @property
    def id(self):
        return self.news_item.id

    @property
    def title(self):
        return self.news_item.title

    @property
    def user_avatar(self):
        return url_for("user.view_avatar", user=self.news_item.user)

    @property
    def user_name(self):
        return self.news_item.user.name

    @property
    def publish_date(self):
        return self.news_item.publish_date

    @property
    def content(self):
        if not self.news_item.can_read():
            return _("Valid membership is required to read this news article")

        return self.news_item.content

    @property
    def read_more(self):
        url = url_for("news.view", news=self.news_item)
        return f"""
<p class="read-more">
  <small>
    <a href="{url}">({_("Read more")}...)</a>
  </small>
</p>
        """
