import json
import os
from functools import cached_property
from typing import Optional, Dict, Any
from datetime import datetime, timezone
from urllib.parse import unquote

import pytz
from babel.dates import format_datetime
from flask import render_template, url_for
from flask_babel import gettext as _

from app import app, constants, get_locale
from app.models.activity import Activity
from app.roles import Roles
from app.service import (
    pretix_service,
    role_service,
)
from app.service.pretix_service import PretixOrder, PretixPaymentStatus


class ActivityViewModel:
    def __init__(self, activity: Activity, user):
        self.activity = activity
        self.user = user
        self.timezone = pytz.timezone("Europe/Amsterdam")

    def render(self):
        return render_template("activity/view_single.htm", view_model=self)

    @property
    def activity_id(self):
        # For creating links to the activity.
        # TODO: create links here.
        return self.activity.id

    @property
    def activity_url(self):
        return url_for("activity.get_activity", activity=self.activity)

    @property
    def can_write(self):
        return role_service.user_has_role(self.user, Roles.ACTIVITY_WRITE)

    @property
    def skip_ssl_check(self):
        return "skip-ssl-check" if app.debug else ""

    @property
    def pretix_widget_url(self):
        return os.path.join(
            app.config["PRETIX_HOST"],
            app.config["PRETIX_ORGANIZER"],
            self.activity.pretix_event_slug,
        )

    @property
    def is_pretix_event(self):
        return bool(self.activity.pretix_event_slug)

    @cached_property
    def pretix_user_info_url(self):
        fernet_token = pretix_service.get_fernet_token_from_user(self.user)
        return url_for("api.pretix.user", fernet_token=fernet_token, _external=True)

    @property
    def pretix_user_order_url(self):
        return os.path.join(
            app.config["PRETIX_HOST"],
            app.config["PRETIX_ORGANIZER"],
            self.activity.pretix_event_slug,
            "order",
            self.pretix_order_code.code,
            self.pretix_order_code.secret,
        )

    @property
    def title(self):
        return self.activity.name

    @property
    def description(self):
        return self.activity.description

    @property
    def relative_time(self):
        return self.activity.till_now()

    @property
    def formatted_time(self):
        """
        Get a proper representation of all datetime date.

        Based on the time the event is gonna take/when it is going to
        start/when it's gonna end.
        """
        today = datetime.now(timezone.utc)
        start_time = self.activity.start_time.astimezone(self.timezone)
        end_time = self.activity.end_time.astimezone(self.timezone)
        is_same_month = start_time.month == end_time.month
        is_same_day = start_time.day == end_time.day

        if is_same_month and is_same_day:
            if start_time.year == today.year:
                return format_datetime(
                    start_time, "EEEE d MMM H:mm - ", locale=get_locale()
                ).capitalize() + format_datetime(end_time, "H:mm", locale=get_locale())
            else:
                return format_datetime(
                    start_time, "EEEE d MMM YYYY, H:mm - ", locale=get_locale()
                ).capitalize() + format_datetime(end_time, "H:mm", locale=get_locale())
        else:
            if start_time.year == today.year:
                return (
                    format_datetime(
                        start_time, "EEE d MMM (H:mm) - ", locale=get_locale()
                    ).capitalize()
                    + format_datetime(
                        end_time, "EEE d MMM (H:mm)", locale=get_locale()
                    ).capitalize()
                )
            else:
                return "%s - %s" % (
                    start_time.strftime(constants.ACT_DT_FORMAT),
                    end_time.strftime(constants.ACT_DT_FORMAT),
                )

    @property
    def picture_url(self):
        return url_for(
            "activity.picture", activity=self.activity, picture_type="thumbnail"
        )

    @property
    def has_picture(self):
        return self.activity.picture_file_id is not None

    @property
    def location(self):
        return self.activity.location

    @property
    def location_name(self):
        if self.activity.location.startswith("https://maps.google.com/?q="):
            return unquote(self.activity.location[len("https://maps.google.com/?q=") :])
        elif self.activity.location.startswith("http://"):
            return unquote(self.activity.location[len("http://") :])
        elif self.activity.location.startswith("https://"):
            return unquote(self.activity.location[len("https://") :])
        return self.activity.location

    @property
    def location_is_url(self):
        if self.activity.location.startswith(
            "http://"
        ) or self.activity.location.startswith("https://"):
            return True
        return False

    @property
    def formatted_price(self):
        def format_int_price(p: int):
            # The form price is currently a float (bad), but we will convert it
            # to cents. After converting it can be immediately used here.
            cents = round(p * 100)

            return _("Price") + ": € {:,.2f}".format(cents / 100.0)

        if self.activity.price:
            try:
                price = int(self.activity.price)
                return format_int_price(price)
            except ValueError:
                return self.activity.price

        return _("Free")

    @property
    def _via_membership_required(self):
        return self.pretix_event_settings.get("via_membership_required", True)

    @property
    def _via_favourer_allowed(self):
        return self.pretix_event_settings.get("via_favourer_allowed", True)

    @property
    def user_registration_state(self):
        if not self.is_pretix_event:
            return "disabled"

        if not self.user.is_authenticated:
            return "login"

        if self._via_membership_required:
            if self.user.favourer and not self._via_favourer_allowed:
                return "favourer_disallowed"
            if not self.user.has_paid and not self.user.favourer:
                return "membership"

        return "register"

    @property
    def thumbnail(self):
        return url_for(
            "activity.picture", activity=self.activity, picture_type="thumbnail"
        )

    @cached_property
    def pretix_event_settings(self) -> Dict[str, Any]:
        if not self.is_pretix_event:
            return {}
        return pretix_service.get_event_settings(self.activity.pretix_event_slug)

    @cached_property
    def pretix_order_code(self) -> Optional[PretixOrder]:
        if not self.is_pretix_event:
            return None
        return pretix_service.get_user_order(
            self.activity.pretix_event_slug, self.user, PretixPaymentStatus.active
        )

    @property
    def json_ld_object(self):
        offer = {
            "@type": "Offer",
            "url": url_for(
                "activity.get_activity", activity=self.activity, _external=True
            ),
            "priceCurrency": "EUR",
            "availability": "https://schema.org/InStock",
        }
        return json.dumps(
            {
                "@context": "https://schema.org",
                "@type": "Event",
                "name": self.activity.en_name,
                "startDate": self.activity.start_time.isoformat(),
                "endDate": self.activity.end_time.isoformat(),
                "eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
                "eventStatus": "https://schema.org/EventScheduled",
                "location": {"@type": "Place", "address": self.activity.location},
                "image": [
                    url_for("activity.picture", activity=self.activity, _external=True),
                    url_for(
                        "activity.picture",
                        activity=self.activity,
                        picture_type="thumbnail",
                        _external=True,
                    ),
                ],
                "offers": offer,
                "description": self.activity.en_description,
            }
        )
