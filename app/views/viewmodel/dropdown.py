from datetime import datetime

from flask import url_for
from flask_login import current_user

from app.roles import Roles
from app.service import role_service


class NavigationBarViewModel:
    def __init__(self):
        pass

    @property
    def name(self):
        return current_user.name

    @property
    def email(self):
        return current_user.email

    @property
    def avatar(self):
        return url_for("user.view_avatar")

    @property
    def show_admin_link(self):
        roles = [
            Roles.ACTIVITY_WRITE,
            Roles.REDIRECT_WRITE,
            Roles.USER_WRITE,
            Roles.GROUP_WRITE,
            Roles.NAVIGATION_WRITE,
            Roles.COMPANY_WRITE,
            Roles.EXAMINATION_WRITE,
            Roles.FILE_WRITE,
            Roles.BANNER_ADMIN,
        ]

        return role_service.user_has_any_role(current_user, *roles)

    @property
    def icon_uri(self):
        today = datetime.now()
        if today.month == 12 and 6 <= today.day:
            return "/static/img/logo_kerst.png"
        else:
            return "/static/img/logo.svg"
