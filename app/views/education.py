from flask import Blueprint, flash, redirect, render_template, request, session, url_for
from flask_babel import _

from app import constants
from app.decorators import require_role
from app.exceptions.base import DuplicateResourceException
from app.forms.examination import EducationForm
from app.models.education import Education
from app.roles import Roles
from app.service import education_service

blueprint = Blueprint("education", __name__, url_prefix="/education")

REDIR_PAGES = {
    "view": "examination.view_examination",
    "add": "examination.add",
    "educations": "education.view_educations",
    "courses": "course.view_courses",
}

DATE_FORMAT = constants.DATE_FORMAT


@blueprint.route("/", methods=["GET"])
@require_role(Roles.EXAMINATION_WRITE)
def view_educations():
    return render_template("vue_content.htm", title=_("Educations"))


@blueprint.route("/create/", methods=["GET", "POST"])
@require_role(Roles.EXAMINATION_WRITE)
def add_education():
    r = request.args.get("redir", True)
    if r in REDIR_PAGES:
        session["origin"] = url_for(REDIR_PAGES[r])
    elif r == "edit" and "examination_edit_id" in session:
        session["origin"] = "/examination/edit/{}".format(
            session["examination_edit_id"]
        )

    form = EducationForm(request.form)

    if form.validate_on_submit():
        try:
            education_service.add_education(
                form.nl_name.data,
                form.en_name.data,
                form.programme_type.data,
                form.is_via_programme.data,
                form.datanose_code.data,
            )

            flash(
                "%s/%s: " % (form.en_name.data, form.nl_name.data)
                + _("Education successfully added."),
                "success",
            )
        except DuplicateResourceException:
            flash(
                "%s/%s: " % (form.en_name.data, form.nl_name.data)
                + _("Already exists in the database"),
                "danger",
            )

        return redirect(url_for("education.view_educations"))

    return render_template("education/edit.htm", form=form, new=True)


@blueprint.route("/<education:education>/edit/", methods=["GET", "POST"])
@require_role(Roles.EXAMINATION_WRITE)
def edit_education(education: Education):
    r = request.args.get("redir")
    if r in REDIR_PAGES:
        session["origin"] = url_for(REDIR_PAGES[r])
    elif r == "edit" and "examination_edit_id" in session:
        session["origin"] = "/examination/edit/{}".format(
            session["examination_edit_id"]
        )

    course_count = education_service.count_courses_by_education_id(education.id)

    form = EducationForm(obj=education)

    if form.validate_on_submit():
        try:
            education_service.update_education(
                education.id,
                form.nl_name.data,
                form.en_name.data,
                form.programme_type.data,
                form.is_via_programme.data,
                form.datanose_code.data,
            )
            flash(
                "%s/%s: " % (form.en_name.data, form.nl_name.data)
                + _("Education successfully saved."),
                "success",
            )
        except DuplicateResourceException as e:
            flash("%s: " % e, "danger")

        return redirect(url_for("education.view_educations"))
    else:
        form.programme_type.data = education.programme_type.name

    return render_template(
        "education/edit.htm",
        new=False,
        form=form,
        redir=r,
        course_count=course_count,
        education=education,
    )
