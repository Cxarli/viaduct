from flask import Blueprint, render_template, redirect
from flask_babel import get_locale, gettext as _
from flask_login import login_required

from app.decorators import require_membership, require_role
from app.models.committee import Committee
from app.roles import Roles
from app.service import company_service
from app.views.file import send_picture

blueprint = Blueprint("vue", __name__)


@blueprint.route("/bug/report/")
@login_required
def bug_report():
    return public(title=_("Report a bug"))


@blueprint.route("/ereleden/", methods=["GET"])
@blueprint.route("/members-of-merit/", methods=["GET"])
def members_of_merit():
    return public(_("Members of merit"))


@blueprint.route("/jobs/", methods=["GET"])
def jobs():
    return public(_("Jobs"), "company-job-overview")


@blueprint.route("/companies/", methods=["GET"])
def companies():
    """Handle all public vue routes, defined in frontend/index.ts."""
    return public(_("Companies"), "company-overview")


def public(title, keep_alive_include=None):
    """Handle all public vue routes, defined in frontend/index.ts."""
    return render_template(
        "vue_content.htm", keep_alive_include=keep_alive_include, title=title
    )


@blueprint.route("/photos/", methods=["GET"], defaults=dict(title=_("Photos")))
@blueprint.route("/search/", methods=["GET"], defaults=dict(title=_("Search")))
@blueprint.route("/photos/<path:_>", methods=["GET"], defaults=dict(title=_("Photos")))
@require_membership
def membership(title, _=""):
    """Handle all member-only vue routes, defined in frontend/index.ts."""
    return render_template("vue_content.htm", title=title)


@blueprint.route("/jobs/<int:job_id>/", methods=["GET"])
def view(job_id):
    """Handle all company resource vue route, defined in frontend/index.ts."""
    job = company_service.get_job_by_id(job_id)
    title = job.title_en if get_locale() == "en" else job.title_nl
    return render_template(
        "vue_content.htm", keep_alive_include="company-overview", title=title
    )


@blueprint.route("/mailinglists/", methods=["GET"])
@blueprint.route("/mailinglists/<path:__>", methods=["GET"])
@require_role(Roles.MAILINGLIST_ADMIN)
def mailinglists(__=""):
    return render_template(
        "vue_content.htm",
        title=_("Mailinglists"),
        keep_alive_include="mailinglists-overview",
    )


@blueprint.route("/commissie/", methods=["GET"])
def committees():
    return render_template(
        "vue_content.htm",
        title=_("Committees"),
        keep_alive_include="committee-overview",
    )


@blueprint.route("/committee/picture/<committee:committee>/")
@blueprint.route("/committee/picture/<committee:committee>/<picture_type>/")
def committee_picture(committee: Committee, picture_type=None):
    if committee.picture_file_id is None:
        return redirect("/static/img/via_committee_default.jpg")

    return send_picture(
        committee.picture_file_id,
        f"committee_picture_{committee.en_name}",
        picture_type,
        thumbnail_size=(800, 600),
    )


@blueprint.route("/committee/create/", methods=["GET"])
@blueprint.route("/committee/<committee:committee>/edit/", methods=["GET"])
@require_role(Roles.GROUP_WRITE)
def committee_edit(committee=None):
    if committee:
        title = _("Edit committee")
    else:
        title = _("Create committee")
    return render_template(
        "vue_content.htm", title=title, keep_alive_include="committee-overview"
    )
