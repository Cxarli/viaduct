from datetime import timedelta

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_babel import _
from flask_login import current_user, login_user

from app import get_locale
from app.decorators import response_headers
from app.exceptions.base import BusinessRuleException, ResourceNotFoundException
from app.forms.user import (
    ManualSignUpForm,
    RequestPassword,
    ResetPasswordForm,
    SAMLSignUpForm,
)
from app.service import (
    education_service,
    membership_service,
    password_reset_service,
    saml_service,
    user_service,
)
from app.views import get_safe_redirect_url

blueprint = Blueprint("signup", __name__)

LOGIN_KWARGS = {"duration": timedelta(days=14), "remember": True}


@blueprint.route("/sign-up/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_up():
    return render_template("user/sign_up_chooser.htm")


@blueprint.route("/sign-up/manual/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def sign_up_manual():
    if current_user.is_authenticated:
        return redirect(get_safe_redirect_url())

    form = ManualSignUpForm()

    if form.validate_on_submit():
        try:
            user = user_service.register_new_user(
                email=form.email.data,
                password=form.password.data,
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                student_id=form.student_id.data,
                educations=form.educations.data,
                birth_date=form.birth_date.data,
                study_start=form.study_start.data,
                phone_nr=form.phone_nr.data,
                address=form.address.data,
                zip_=form.zip.data,
                city=form.city.data,
                country=form.country.data,
                locale=get_locale(),
            )

            login_user(user, **LOGIN_KWARGS)

            flash(
                _(
                    "Welcome %(name)s! Your profile has been succesfully "
                    "created and you have been logged in!",
                    name=current_user.first_name,
                ),
                "success",
            )

            return redirect(url_for("home.home"))

        except BusinessRuleException:
            flash(_("A user with this e-mail address already exists"), "danger")

    return render_template("user/sign_up.htm", form=form)


@blueprint.route("/sign-up/process-saml-response/", methods=["GET", "POST"])
@saml_service.ensure_data_cleared
def sign_up_saml_response():
    form = SAMLSignUpForm()

    # Redirect the user to the index page if he or she has been authenticated
    # already.
    if current_user.is_authenticated:

        # End the sign up session when it is still there somehow
        if saml_service.get_active_sign_up_session():
            saml_service.end_sign_up_session()

        return redirect(get_safe_redirect_url())

    if saml_service.get_active_sign_up_session():

        # Delete the old sign up session when
        # the user re-authenticates
        if saml_service.user_is_authenticated():
            saml_service.end_sign_up_session()

        # Otherwise, refresh the timeout timestamp of the session
        else:
            saml_service.update_sign_up_session_timestamp()

    if not saml_service.get_active_sign_up_session():

        if not saml_service.user_is_authenticated():
            flash(_("Authentication failed. Please try again."), "danger")
            return redirect(get_safe_redirect_url())

        if not saml_service.user_is_student():
            flash(
                _("You must authenticate with a student " "UvA account to register."),
                "danger",
            )
            return redirect(get_safe_redirect_url())

        if saml_service.uid_is_linked_to_other_user():
            flash(
                _(
                    "There is already an account linked to this UvA account. "
                    "If you are sure that this is a mistake please send "
                    "an email to the board."
                ),
                "danger",
            )
            return redirect(get_safe_redirect_url())

        # Start a new sign up session and pre-fill the form
        saml_service.start_sign_up_session()
        saml_session = saml_service.get_active_sign_up_session()
        form.first_name.data = saml_session.given_name
        form.last_name.data = saml_session.surname
        form.email.data = saml_session.email

    # When we encounter a GET request but a session is already active,
    # this means that the user did a refresh without submitting the form.
    # We redirect him/her to the SAML sign up, since otherwise all
    # pre-filled data would be gone.
    elif request.method == "GET":
        return redirect(url_for("saml.sign_up"))

    saml_session = saml_service.get_active_sign_up_session()
    educations = saml_service.get_study_programmes_by_codes(
        saml_session.study_programme_codes
    )
    education_names = list(
        map(
            lambda x: education_service.get_education_name(x, get_locale()),
            educations,
        )
    )
    membership_fee = None
    if membership_service.is_any_study_from_via(educations):
        membership_fee = membership_service.get_membership_price_from_via_studies(
            educations
        )

    if form.validate_on_submit():
        try:
            user = user_service.register_new_user(
                email=form.email.data,
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                student_id=saml_session.student_id,
                educations=educations,
                birth_date=form.birth_date.data,
                study_start=form.study_start.data,
                phone_nr=form.phone_nr.data,
                address=form.address.data,
                zip_=form.zip.data,
                city=form.city.data,
                country=form.country.data,
                locale=saml_session.locale,
                link_student_id=True,
            )

            login_user(user, **LOGIN_KWARGS)

            saml_service.end_sign_up_session()

            return redirect(url_for("membership.payment"))

        except BusinessRuleException:
            flash(_("A user with this e-mail address already exists"), "danger")

    return render_template(
        "user/sign_up.htm",
        form=form,
        student_id=saml_session.student_id,
        educations=education_names,
        get_education_name=education_service.get_education_name,
        membership_fee=membership_fee,
    )


@blueprint.route("/process-account-linking/")
@saml_service.ensure_data_cleared
def process_account_linking_saml_response():
    # Check whether a user is linking his/her own account
    # or an someone is linking another account
    linking_current_user = saml_service.is_linking_user_current_user()

    if not current_user.is_authenticated:
        if linking_current_user:
            flash(_("You need to be logged in to link your account."), "danger")
        else:
            flash(_("You need to be logged in to link an account."), "danger")

        return redirect(get_safe_redirect_url())

    if not saml_service.user_is_authenticated():
        flash(_("Authentication failed. Please try again."), "danger")
        return redirect(get_safe_redirect_url())

    if linking_current_user:
        try:
            saml_service.link_uid_to_user(current_user)
            flash(_("Your account is now linked to this UvA account."), "success")
        except BusinessRuleException:
            flash(
                _(
                    "There is already an account linked to this UvA account. "
                    "If you are sure that this is a mistake please send "
                    "an email to the board."
                ),
                "danger",
            )
    else:
        try:
            link_user = saml_service.get_linking_user()
            saml_service.link_uid_to_user(link_user)

            flash(_("The account is now linked to this UvA account."), "success")
        except BusinessRuleException:
            flash(
                _("There is already an account linked to this UvA account."), "danger"
            )
        except ResourceNotFoundException:
            # Should not happen normally
            flash(_("Could not find the user to link this UvA account to."), "danger")

    return redirect(get_safe_redirect_url())


@blueprint.route("/request_password/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def request_password():
    """Create a ticket and send a email with link to reset_password page."""
    if current_user.is_authenticated:
        return redirect(url_for("user.view_single_user", user=current_user))

    form = RequestPassword()

    if form.validate_on_submit():
        reset_link = url_for("signup.reset_password", _external=True)
        try:
            password_reset_service.create_password_ticket(form.email.data, reset_link)
            flash(
                _(
                    "An email has been sent to %(email)s with further " "instructions.",
                    email=form.email.data,
                ),
                "success",
            )
            return redirect(url_for("home.home"))

        except ResourceNotFoundException:
            flash(
                _("%(email)s is unknown to our system.", email=form.email.data),
                "danger",
            )

    return render_template("user/request_password.htm", form=form)


@blueprint.route("/reset_password/", methods=["GET", "POST"])
@blueprint.route("/reset_password/<string:hash_>/", methods=["GET", "POST"])
@response_headers({"X-Frame-Options": "SAMEORIGIN"})
def reset_password(hash_=None):
    """
    Reset form existing of two fields, password and password_repeat.

    Checks if the hash in the url is found in the database and timestamp
    has not expired.
    """
    if current_user.is_authenticated:
        flash(_("You are already logged in."), "info")
        return redirect(url_for("home.home"))

    try:
        ticket = password_reset_service.get_valid_ticket(hash_)
    except ResourceNotFoundException:
        flash(_("No valid ticket found"), "danger")
        return redirect(url_for("signup.request_password"))

    form = ResetPasswordForm()

    if form.validate_on_submit():
        password_reset_service.reset_password(ticket, form.password.data)
        flash(_("Your password has been updated."), "success")
        return redirect(url_for("login.sign_in", next=url_for("home.home")))

    return render_template("user/reset_password.htm", form=form)
