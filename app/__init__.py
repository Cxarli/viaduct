import logging.config
import mimetypes
import os

import sentry_sdk
from celery import Celery
from flask import Flask, has_request_context, request, session
from flask_login import current_user
from flask_restful import Api
from hashfs import HashFS
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

from app import constants
from app.extensions import babel, db
from app.utils.flask_init import FlaskInitializer
from app.utils.git_version import GitVersion
from config import Config

# Load the git hash from the .git folder. This works, since the docker
# image build in gitlab also contain the .git folder.
version: GitVersion = GitVersion()

app = Flask(__name__)
app_init = FlaskInitializer(app)

# Locate config file relative to this file, instead of based on working directory.
logging_conf_file = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), "logging.conf"
)
logging.config.fileConfig(logging_conf_file, disable_existing_loggers=False)


class NativeErrorsApi(Api):
    """
    Custom class to make sure our error handlers handle all API errors.

    See error_handlers.py for error handling logic.
    """

    def handle_error(self, e):
        raise e


rest_api = NativeErrorsApi(app=app, prefix="/api")
_logger = logging.getLogger(__name__)
_logger.info("Git hash: %s", version.hash)
_logger.info("Git date: %s", version.timestamp.isoformat())

hashfs = HashFS("app/uploads/")
mimetypes.init()

worker: Celery = Celery("viaduct")


def static_url(url):
    return url + "?v=" + version.hash


@babel.localeselector
def get_locale():
    languages = list(constants.LANGUAGES.keys())

    if not has_request_context():
        return languages[0]
    # Try to look-up an session set for language
    lang = session.get("lang")
    if lang and lang in languages:
        return lang

    # if a user is logged in, use the locale from the user settings
    if current_user.is_authenticated and current_user.locale is not None:
        return current_user.locale

    return request.accept_languages.best_match(languages, default="nl")


app_init.init_converters()
app_init.init_views()
app_init.init_migrate(db)
from app import cli  # noqa


def init_app() -> Flask:
    # Workarounds for template reloading
    app.jinja_env.auto_reload = app.debug
    app.config["TEMPLATES_AUTO_RELOAD"] = app.debug

    # Disable cookies on http (and thereby prevent hijack the user session)
    app.config["SESSION_COOKIE_SECURE"] = not app.debug

    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_DATABASE_URI"]
    db.init_app(app)

    _logger.info("Loading config")
    app.config.from_object(Config(database_url=app.config["SQLALCHEMY_DATABASE_URI"]))

    # Has to be imported *after* app is created and Babel is initialised
    from app import jinja_env  # noqa

    app_init.init_worker()
    app_init.init_babel()
    app_init.init_cors()
    app_init.init_cache()
    app_init.init_login()
    app_init.init_oauth()

    if not app.debug and "SENTRY_DSN" in app.config:
        sentry_sdk.init(
            dsn=app.config.get("SENTRY_DSN"),
            integrations=[
                FlaskIntegration(),
                RedisIntegration(),
                SqlalchemyIntegration(),
                CeleryIntegration(),
            ],
            release=version.hash,
            environment=app.config["ENVIRONMENT"],
            send_default_pii=True,
        )

    from app.utils import context_filters  # noqa
    from app.utils.json import JSONEncoder

    app.json_encoder = JSONEncoder

    return app
