from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.group import Group  # noqa


@mapper_registry.mapped
class GroupRole(BaseEntity):
    """Many to Many between Groups and Roles."""

    __tablename__ = "group_role"

    prints = ("group", "role")
    group_id = Column(Integer, ForeignKey("group.id"))
    group: "Group" = relationship("Group", back_populates="roles", uselist=False)

    role = Column(String(128), nullable=False)

    def __str__(self):
        return self.role
