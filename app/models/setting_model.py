from sqlalchemy import Column, String

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class Setting(BaseEntity):
    __tablename__ = "setting"

    key: str = Column(String(128), unique=True, nullable=False)
    value: str = Column(String(4096), nullable=False)
