import datetime
from typing import List, TYPE_CHECKING

from flask import has_request_context
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    Enum,
    ForeignKey,
    Integer,
    JSON,
    String,
)
from sqlalchemy.orm import relationship

from app import get_locale
from app.extensions import mapper_registry
from app.models.activity import Activity
from app.models.base_model import BaseEntity
from app.service.alv_parse_service import AlvEvent

if TYPE_CHECKING:
    from app.models.user import User  # noqa
    from app.models.file import File  # noqa


@mapper_registry.mapped
class Alv(BaseEntity):
    __tablename__ = "alv"
    nl_name: str = Column(String(128), nullable=False)
    en_name: str = Column(String(128), nullable=False)

    date: datetime.date = Column(Date, nullable=False)

    activity_id = Column(Integer, ForeignKey("activity.id"), nullable=True)
    activity: Activity = relationship("Activity")

    chairman_user_id = Column(Integer, ForeignKey("user.id"))
    chairman: "User" = relationship("User", foreign_keys=[chairman_user_id])

    secretary_user_id = Column(Integer, ForeignKey("user.id"))
    secretary: "User" = relationship("User", foreign_keys=[secretary_user_id])

    minutes_file_id = Column(Integer, ForeignKey("file.id"), nullable=True)
    minutes_file: "File" = relationship("File", foreign_keys=[minutes_file_id])

    documents: "AlvDocument" = relationship("AlvDocument", back_populates="alv")

    # Indicates if the minutes are "ingestemd"
    minutes_accepted = Column(Boolean)

    meta = Column(JSON)

    @property
    def name(self):
        return self.get_localized_name()

    def get_localized_basename(self, locale=None):
        if not locale:
            locale = get_locale()

        fallback = "Minutes {}".format(self.id)

        if locale == "nl":
            if self.nl_name:
                return self.nl_name
            elif self.en_name:
                return self.en_name
            else:
                return fallback
        elif locale == "en" and self.en_name:
            if self.en_name:
                return self.nl_name
            elif self.nl_name:
                return self.nl_name
        return fallback

    def get_localized_name(self, locale=None):
        if not locale:
            locale = get_locale()

        if locale == "nl" and self.nl_name:
            return self.nl_name
        elif locale == "en" and self.en_name:
            return self.en_name
        elif self.nl_name:
            return self.nl_name + " (Dutch)"
        elif self.en_name:
            return self.en_name + " (Engels)"
        else:
            return "N/A"


@mapper_registry.mapped
class AlvUser(BaseEntity):
    """Users that were present at an assembly.

    The `name` string is required, since those are parsed from the minutes.
    When saving the data a best-effort attempt is made to find user_ids
    belonging to the names. However, we might not find a user with that name.
    """

    __tablename__ = "alv_user"
    alv_id = Column(Integer(), ForeignKey("alv.id"), nullable=False)
    alv: Alv = relationship("Alv")

    user_id = Column(Integer(), ForeignKey("user.id"))
    user: "User" = relationship("User")

    name = Column(String(512), nullable=False)


@mapper_registry.mapped
class AlvUserProxy(BaseEntity):
    """Users that were absent, but had someone else vote for them.

    The `absentee_name` and `voter_name` strings are required, since those are
    parsed from the minutes. When saving the data a best-effort attempt is
    made to find user_ids belonging to the names.
    However, we might not find a user with that name.
    """

    __tablename__ = "alv_user_proxy"

    alv_id = Column(Integer(), ForeignKey("alv.id"), nullable=False)
    alv: Alv = relationship("Alv")

    absentee_id = Column(Integer(), ForeignKey("user.id"))
    absentee: "User" = relationship("User", foreign_keys=[absentee_id])

    voter_id = Column(Integer(), ForeignKey("user.id"))
    voter: "User" = relationship("User", foreign_keys=[voter_id])

    absentee_name = Column(String(512), nullable=False)
    voter_name = Column(String(512), nullable=False)


@mapper_registry.mapped
class AlvEventEntry(BaseEntity):
    __tablename__ = "alv_event"

    alv_id = Column(Integer(), ForeignKey("alv.id"), nullable=False)
    alv: Alv = relationship("Alv")

    event_type: AlvEvent = Column(Enum(AlvEvent, name="alv_event_type"), nullable=False)
    data = Column(JSON)


@mapper_registry.mapped
class AlvDocument(BaseEntity):
    __tablename__ = "alv_document"
    nl_name = Column(String(128), nullable=False)
    en_name = Column(String(128), nullable=False)

    alv_id = Column(Integer(), ForeignKey("alv.id"))
    alv: Alv = relationship("Alv", back_populates="documents")

    versions: List["AlvDocumentVersion"] = relationship(
        "AlvDocumentVersion", back_populates="alv_document"
    )

    @property
    def name(self):
        return self.get_localized_name()

    def get_localized_basename(self, locale="en"):
        if not locale and has_request_context():
            locale = get_locale()

        fallback = "Document {}".format(self.id)

        if locale == "nl":
            if self.nl_name:
                return self.nl_name
            elif self.en_name:
                return self.en_name
        elif locale == "en" and self.en_name:
            if self.en_name:
                return self.nl_name
            elif self.nl_name:
                return self.nl_name
        return fallback

    def get_localized_name(self, locale=None):
        if not locale and has_request_context():
            locale = get_locale()

        if locale == "nl" and self.nl_name:
            return self.nl_name
        elif locale == "en" and self.en_name:
            return self.en_name
        elif self.nl_name:
            return self.nl_name + " (Dutch)"
        elif self.en_name:
            return self.en_name + " (Engels)"
        else:
            return "N/A"


@mapper_registry.mapped
class AlvDocumentVersion(BaseEntity):
    __tablename__ = "alv_document_version"

    file_id = Column(Integer(), ForeignKey("file.id"))
    file: "File" = relationship("File")

    alv_document_id = Column(Integer(), ForeignKey("alv_document.id"))
    alv_document: AlvDocument = relationship("AlvDocument", back_populates="versions")
