import datetime
from typing import List, TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, Text
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.activity import Activity  # noqa
    from app.models.news import News  # noqa


@mapper_registry.mapped
class NewsletterActivity:
    __tablename__ = "newsletter_activities"

    newsletter_id = Column(
        Integer,
        ForeignKey("newsletter.id", ondelete="cascade"),
        nullable=False,
        primary_key=True,
    )
    activity_id = Column(
        Integer, ForeignKey("activity.id"), nullable=False, primary_key=True
    )
    position = Column(Integer, nullable=False)

    nl_description = Column(Text, server_default="", default="", nullable=False)
    en_description = Column(Text, server_default="", default="", nullable=False)

    activity: "Activity" = relationship("Activity")

    def get_localized_name_desc(self, locale):
        original_title, original_content = self.activity.get_localized_name_desc(locale)

        if locale == "nl":
            return original_title, self.nl_description or original_content
        elif locale == "en":
            return original_title, self.en_description or original_content


@mapper_registry.mapped
class NewsletterNewsItem:
    __tablename__ = "newsletter_news"

    newsletter_id = Column(
        Integer,
        ForeignKey("newsletter.id", ondelete="cascade"),
        nullable=False,
        primary_key=True,
    )
    news_id = Column(Integer, ForeignKey("news.id"), nullable=False, primary_key=True)
    position = Column(Integer, nullable=False)
    nl_description = Column(Text, server_default="", default="", nullable=False)
    en_description = Column(Text, server_default="", default="", nullable=False)
    news_item: "News" = relationship("News")

    def get_localized_title_content(self, locale):
        original_title, original_content = self.news_item.get_localized_title_content(
            locale
        )

        if locale == "nl":
            return original_title, self.nl_description or original_content
        elif locale == "en":
            return original_title, self.en_description or original_content


@mapper_registry.mapped
class Newsletter(BaseEntity):
    __tablename__ = "newsletter"

    activities: List[NewsletterActivity] = relationship(
        "NewsletterActivity",
        order_by="NewsletterActivity.position",
        collection_class=ordering_list("position"),
        cascade="all, delete-orphan",
    )

    news_items: List[NewsletterNewsItem] = relationship(
        "NewsletterNewsItem",
        order_by="NewsletterNewsItem.position",
        collection_class=ordering_list("position"),
        cascade="all, delete-orphan",
    )

    def __init__(self, start_day=None):
        if start_day is None:
            start_day = datetime.date.today()

        weekday = start_day.weekday()
        self.start_day = start_day - datetime.timedelta(days=weekday)
