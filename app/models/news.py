import datetime
from datetime import date
from functools import cached_property
from typing import TYPE_CHECKING

from flask import has_request_context
from flask_login import current_user
from sqlalchemy import Boolean, Column, Date, Integer, String, Text
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.user import User  # noqa


@mapper_registry.mapped
class News(BaseEntity):
    __tablename__ = "news"
    nl_title = Column(String(256), default="")
    en_title = Column(String(256), default="")
    nl_content = Column(Text, default="")
    en_content = Column(Text, default="")

    user_id = Column(Integer, ForeignKey("user.id"))
    user: "User" = relationship("User", backref=backref("news", lazy="dynamic"))
    needs_paid = Column(Boolean, default=False, nullable=False)

    publish_date = Column(Date, default=date.today)

    def __str__(self):
        return "{} ({})".format(self.title, self.publish_date)

    @property
    def modified_after_creation(self):
        return (self.modified - self.created) >= datetime.timedelta(seconds=1)

    def can_read(self, user=current_user):
        return not self.needs_paid or user.has_paid

    @cached_property
    def title(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()
        return self.get_localized_title_content(locale)[0]

    @cached_property
    def content(self):
        locale = "en"
        if has_request_context():
            locale = get_locale()
        return self.get_localized_title_content(locale)[1]

    def get_localized_title_content(self, locale=None):
        if not locale:
            locale = get_locale()

        nl_available = self.nl_title and self.nl_content
        en_available = self.en_title and self.en_content
        if locale == "nl" and nl_available:
            title = self.nl_title
            content = self.nl_content
        elif locale == "en" and en_available:
            title = self.en_title
            content = self.en_content
        elif nl_available:
            title = self.nl_title + " (Dutch)"
            content = self.nl_content
        elif en_available:
            title = self.en_title + " (Engels)"
            content = self.en_content
        else:
            title = "N/A"
            content = "N/A"

        return title, content
