import datetime
from typing import List, TYPE_CHECKING

import baas32 as b32
from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.orm import backref, relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.user import User  # noqa
    from app.models.group import Group  # noqa


@mapper_registry.mapped
class Task(BaseEntity):
    __tablename__ = "pimpy_task"

    prints = ("id", "title")

    title = Column(Text)
    content = Column(Text)
    line = Column(Integer)

    minute_id = Column(Integer, ForeignKey("pimpy_minute.id", ondelete="cascade"))
    minute: "Minute" = relationship("Minute", backref=backref("tasks", lazy="dynamic"))

    group_id = Column(Integer, ForeignKey("group.id"))
    group: "Group" = relationship("Group", backref=backref("tasks", lazy="dynamic"))

    users: List["User"] = relationship(
        "User",
        uselist=True,
        secondary="pimpy_task_user",
        lazy="dynamic",
    )

    status = Column(Integer)
    stati_raw = ["new", "started", "done", "remove", "finished", "deleted"]

    @property
    def b32_id(self):
        return b32.encode(self.id)

    @property
    def status_raw(self):
        return self.stati_raw[self.status]


@mapper_registry.mapped
class TaskUserRel:
    __tablename__ = "pimpy_task_user"

    id = Column(Integer, primary_key=True)
    task_id = Column(Integer, ForeignKey("pimpy_task.id", ondelete="cascade"))
    user_id = Column(Integer, ForeignKey("user.id"))

    task: Task = relationship("Task")
    user: "User" = relationship("User")


@mapper_registry.mapped
class Minute(BaseEntity):
    __tablename__ = "pimpy_minute"

    content = Column(Text)
    group_id = Column(Integer, ForeignKey("group.id"))
    group: "Group" = relationship("Group", backref=backref("minutes", lazy="dynamic"))

    # the date when the meeting took place
    minute_date = Column(DateTime, default=datetime.datetime.utcnow())
