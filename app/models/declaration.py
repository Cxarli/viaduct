from app import db
from app.models.base_model import BaseEntity


class Declaration(db.Model, BaseEntity):
    __tablename__ = "declaration"
    committee = db.Column(db.String(256), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    reason = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    user = db.relationship("User")
