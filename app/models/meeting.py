from datetime import datetime, timedelta
from typing import TYPE_CHECKING

from sqlalchemy import Boolean, Column, DateTime, Integer, String, true
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.group import Group  # noqa


@mapper_registry.mapped
class Meeting(BaseEntity):
    __tablename__ = "meeting"

    group_id: int = Column(Integer, ForeignKey("group.id"), nullable=False)
    group: "Group" = relationship("Group", uselist=False)

    name: str = Column(String(512), nullable=False)
    start_time: datetime = Column(DateTime(timezone=True), nullable=False)
    end_time: datetime = Column(DateTime(timezone=True), nullable=False)
    location: str = Column(String(64), nullable=False, server_default="")
    description: str = Column(String(2048), nullable=False, server_default="")
    cancelled: bool = Column(Boolean, nullable=False, server_default=true())
    sequence: int = Column(Integer, default=0)

    # TODO zie RFC 2445 4.8.7.4 sequence nummer omhoog als "DTSTART",
    #  "DTEND", "DUE", "RDATE", "RRULE", "EXDATE", "EXRULE", "STATUS"
    #  geüpdated word.

    def modified_different_then_created(self):
        if self.modified - self.created <= timedelta(seconds=2):
            return True

        return False
