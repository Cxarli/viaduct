import re

from flask_babel import lazy_gettext as _
from sqlalchemy import Column, Date, Enum, Integer, String
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.course import Course
from app.models.file import File

test_types = {
    "Mid-term": _("Mid-Term"),
    "End-term": _("End-Term"),
    "Retake": _("Retake"),
    "Unknown": _("Unknown"),
}

test_type_default = "Unknown"


@mapper_registry.mapped
class Examination(BaseEntity):
    __tablename__ = "examination"

    comment = Column(String(128))
    date = Column(Date)

    examination_file_id = Column(Integer, ForeignKey("file.id"))
    answers_file_id = Column(Integer, ForeignKey("file.id"))

    course_id = Column(Integer, ForeignKey("course.id"))
    test_type = Column(
        Enum(*list(test_types.keys()), name="examination_type"),
        nullable=False,
        server_default="Unknown",
    )
    course: Course = relationship(
        Course, backref=backref("examinations", lazy="dynamic")
    )

    examination_file: File = relationship(
        File, foreign_keys=[examination_file_id], lazy="joined"
    )
    answers_file: File = relationship(
        File, foreign_keys=[answers_file_id], lazy="joined"
    )

    def _get_filename(self, answers):
        fn = ""

        for word in re.split(r"\s+", self.course.name):
            fn += word.capitalize()

        if self.test_type == "Mid-term":
            fn += "_Midterm"
        elif self.test_type == "End-term":
            fn += "_Final"
        elif self.test_type == "Retake":
            fn += "_Retake"

        if self.date is not None:
            fn += self.date.strftime("_%d_%m_%Y")

        if answers:
            fn += f"_answers.{self.answers_file.extension}"
        else:
            fn += f".{self.examination_file.extension}"

        return fn

    @property
    def examination_filename(self):
        """
        Filename for the examination file (without extension).

        Create a filename for the examination file
        based on the exam's information.
        """
        return self._get_filename(False)

    @property
    def answers_filename(self):
        """
        Filename for the answers file file (without extension).

        Create a filename for the answers file
        based on the exam's information.
        """

        if self.answers_file is None:
            return None
        return self._get_filename(True)
