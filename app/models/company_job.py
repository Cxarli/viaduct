from flask import has_request_context
from sqlalchemy import Column, Enum, String, Text, func
from sqlalchemy.ext.hybrid import hybrid_property

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.company import CompanyModuleMixin

CONTRACT_OF_SERVICE_TYPES = ["full-time", "part-time", "internship", "thesis"]


@mapper_registry.mapped
class CompanyJob(BaseEntity, CompanyModuleMixin):
    __tablename__ = "company_job"
    title_nl = Column(String(200))
    title_en = Column(String(200))

    description_nl = Column(Text)
    description_en = Column(Text)

    contract_of_service = Column(
        Enum(*CONTRACT_OF_SERVICE_TYPES, name="company_job_type")
    )

    contact_name = Column(String(256), nullable=False)
    contact_email = Column(String(256), nullable=False)
    contact_address = Column(String(256), nullable=False)
    contact_city = Column(String(256), nullable=False)

    website = Column(String(512))
    phone = Column(String(64))

    @property
    def title(self):
        return self.get_localized_title_description()[0]

    @property
    def description(self):
        return self.get_localized_title_description()[1]

    def get_localized_title_description(self, locale: str = "en"):
        if not locale and has_request_context():
            locale = get_locale()

        if locale == "nl" and self.title_nl and self.description_nl:
            title = self.title_nl
            description = self.description_nl
        elif locale == "en" and self.title_en and self.description_en:
            title = self.title_en
            description = self.description_en
        elif self.title_nl and self.description_nl:
            title = self.title_nl + " (Dutch)"
            description = self.description_nl
        elif self.title_en and self.description_en:
            title = self.title_en + " (Engels)"
            description = self.description_en
        else:
            title = "N/A"
            description = "N/A"

        return title, description

    @hybrid_property
    def ordering(self):
        raise NotImplementedError()

    @ordering.expression  # type: ignore
    def ordering(self):
        return func.md5(func.cast(self.id + func.extract("doy", func.now()), Text))
