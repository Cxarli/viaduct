from typing import TYPE_CHECKING, Any

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Text, event
from sqlalchemy.orm import relationship
from sqlalchemy.util import deprecated

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.group import Group  # noqa
    from app.models.user import User  # noqa
    from app.models.page import Page  # noqa


@mapper_registry.mapped
class CommitteeTag(BaseEntity):
    __tablename__ = "committee_tag"

    nl_name = Column(String(256), nullable=False)
    en_name = Column(String(256), nullable=False)


@mapper_registry.mapped
class CommitteeTags:
    __tablename__ = "committee_tags"
    committee_id = Column(
        Integer, ForeignKey("committee.id", ondelete="cascade"), primary_key=True
    )
    tag_id = Column(Integer, ForeignKey("committee_tag.id"), primary_key=True)


@mapper_registry.mapped
class Committee(BaseEntity):
    __tablename__ = "committee"

    nl_name = Column(String(256), nullable=False)
    en_name = Column(String(256), nullable=False)

    page_id = Column(Integer, ForeignKey("page.id"), nullable=False)

    group_id = Column(Integer, ForeignKey("group.id"), nullable=False)
    coordinator_id = Column(Integer, ForeignKey("user.id"))
    picture_file_id = Column(Integer, ForeignKey("file.id"))
    coordinator_interim = Column(Boolean, nullable=False, default=False)
    open_new_members = Column(Boolean, nullable=False, default=False)
    pressure = Column(Integer, nullable=False, default=0)

    group: "Group" = relationship("Group")
    coordinator: "User" = relationship("User")
    page: "Page" = relationship("Page")

    nl_description = Column(String(256), nullable=False, default="")
    en_description = Column(String(256), nullable=False, default="")

    # TODO Remove any: https://github.com/sqlalchemy/sqlalchemy/issues/6229
    tags: Any = relationship(
        "CommitteeTag", secondary=CommitteeTags.__table__, lazy="dynamic", uselist=True
    )

    def get_localized_name(self, locale=None):
        if not locale:
            locale = get_locale()

        if locale == "nl" and self.nl_name:
            return self.nl_name
        elif locale == "en" and self.en_name:
            return self.en_name
        elif self.nl_name:
            return self.nl_name + " (Dutch)"
        elif self.en_name:
            return self.en_name + " (Engels)"
        else:
            return "N/A"

    def get_localized_description(self, locale=None):
        if not locale:
            locale = get_locale()

        if locale == "nl" and self.nl_description:
            return self.nl_description
        elif locale == "en" and self.en_description:
            return self.en_description
        else:
            return "N/A"


@event.listens_for(Committee, "load")
def set_committee_locale(committee, _):
    """
    Fill model content according to language.

    This function is called after an Activity model is filled with data from
    the database, but before is used in all other code.

    Use the locale of the current user/client to determine which language to
    display on the whole website. If the users locale is unavailable, select
    the alternative language, suffixing the title of the activity with the
    displayed language.
    """

    committee.name = committee.get_localized_name()
    committee.description = committee.get_localized_description()


@deprecated
@mapper_registry.mapped
class CommitteeRevision(BaseEntity):
    """
    This solely exists as a backup, use committee and page_revision instead.
    """

    __tablename__ = "committee_revision"

    nl_title = Column(String(128))
    en_title = Column(String(128))
    comment = Column(String(1024))
    nl_description = Column(Text)
    en_description = Column(Text)
    group_id = Column(Integer, ForeignKey("group.id"))
    coordinator_id = Column(Integer, ForeignKey("user.id"))
    interim = Column(Boolean)
    open_new_members = Column(Boolean, nullable=False, default=0)
    user_id = Column(Integer, ForeignKey("user.id"))
    page_id = Column(Integer, ForeignKey("page.id"))
