"""
This module contains a set of basic application errors.

Any custom extensions of the exceptions should be a subclass of any of these
classes.
"""
from marshmallow import RAISE, Schema, fields
from werkzeug.exceptions import HTTPException


class ApplicationException(HTTPException):
    """Base exception for the application."""

    class ErrorSchema(Schema):
        """Schema for the basic errors."""

        class Meta:
            unknown = RAISE
            ordered = True

        code = fields.Integer()
        title = fields.String()
        type = fields.Url(attribute="type_")
        detail = fields.String(attribute="_message")
        data = fields.Raw()

    code = 500
    title = "Internal Server Error"
    type_ = "about:blank"
    message = "An internal server error occurred."

    def __init__(self, **kwargs):
        self.data = kwargs
        self._message = self.message.format(**kwargs)
        super(ApplicationException, self).__init__(description=self._message)

    def __str__(self):
        return self._message


class ValidationException(ApplicationException):
    code = 400
    title = "Validation Error"
    message = "The request was not according to semantic rules. {details}"

    def __init__(self, details):
        super(ValidationException, self).__init__(details=details)


class AuthorizationException(ApplicationException):
    code = 403
    title = "Unauthorized"
    message = "You are not authorized for this operation. {details}"

    def __init__(self, details):
        super(AuthorizationException, self).__init__(details=details)


class ResourceNotFoundException(ApplicationException):
    code = 404
    title = "Not Found"
    message = "Cannot find {resource} identified by {identifier}."

    def __init__(self, resource, identifier):
        super(ResourceNotFoundException, self).__init__(
            resource=resource, identifier=identifier
        )


class BusinessRuleException(ApplicationException):
    code = 409
    title = "Conflict"
    message = "A business rule was violated. {details}"

    def __init__(self, details):
        super(BusinessRuleException, self).__init__(details=details)


class DuplicateResourceException(ApplicationException):
    code = 409
    title = "Duplicate Resource"
    message = "Duplicate {resource} identified by {identifier}."

    def __init__(self, resource, identifier):
        super(DuplicateResourceException, self).__init__(
            resource=resource, identifier=identifier
        )


class RateLimitedException(ApplicationException):
    code = 429
    title = "Too Many Requests"
    message = "You are performing this request too often. {details}"

    def __init__(self, details):
        super(RateLimitedException, self).__init__(details=details)


class ServiceUnavailableException(ApplicationException):
    code = 503
    title = "Service Unavailable"
    message = "The requested service is unavailable. {details}"

    def __init__(self, details):
        super(ServiceUnavailableException, self).__init__(details=details)
