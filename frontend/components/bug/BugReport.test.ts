import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import BugReport from "./BugReport.vue";
import VueRouter from "vue-router";
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);

jest.mock("../../scripts/api/schema.ts", () => {
    return {
        schemaApi: {
            getSchema: jest.fn(() =>
                Promise.resolve({
                    data: {
                        title: "BugReportRequest",
                        type: "object",
                        properties: {
                            project: {
                                title: "Project",
                                default: "viaduct",
                                enum: ["viaduct", "pretix", "pos"],
                                type: "string",
                            },
                            title: { title: "Title", type: "string" },
                            description: {
                                title: "Description",
                                format: "textarea",
                                type: "string",
                            },
                        },
                        required: ["title", "description"],
                    },
                })
            ),
        },
    };
});

const consoleSpy = jest.spyOn(console, "error");

describe(BugReport.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(BugReport, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            mocks: { $viaduct: { locale: "en" } },
        });

        await flushPromises();
        expect(getAllByText(/Report a bug/)).toHaveLength(2);
        expect(getAllByText(/Project/)).toHaveLength(1);
        expect(getAllByText(/Title/)).toHaveLength(1);
        expect(getAllByText(/Description/)).toHaveLength(1);
    });
});
