import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import ChallengeAdminSubmission from "./ChallengeAdminSubmission.vue";
import VueRouter from "vue-router";
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);

jest.mock("../../scripts/api/schema.ts", () => {
    return {
        schemaApi: {
            getSchema: jest.fn(() =>
                Promise.resolve({
                    data: {
                        title: "ChallengeAdminSubmission",
                        type: "object",
                        properties: {
                            user_id: {
                                title: "User Id",
                                exclusiveMinimum: 0,
                                format: "user",
                                type: "integer",
                            },
                        },
                        required: ["user_id"],
                    },
                })
            ),
        },
    };
});

const consoleSpy = jest.spyOn(console, "error");

describe(ChallengeAdminSubmission.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(ChallengeAdminSubmission, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            mocks: { $viaduct: { locale: "en" } },
        });

        await flushPromises();
        expect(getAllByText(/Manual solution/)).toHaveLength(1);
        expect(getAllByText(/Add manual submission/)).toHaveLength(1);
        expect(getAllByText(/User/)).toHaveLength(1);
    });
});
