import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import Challenges from "./challenge_overview.vue";
import VueRouter from "vue-router";
import flushPromises from "flush-promises";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/page.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";

const consoleSpy = jest.spyOn(console, "error");

describe(Challenges.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(Challenges, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            mocks: { $viaduct: { locale: "en" } },
        });
        await flushPromises();

        getByText("Ranking");
        getByText("challenge en title");
        getByText("challenge en content");
    });
});
