<template lang="pug">
a-select(
    mode="tags",
    :labelInValue="true",
    :value="value",
    :allowClear="true",
    placeholder="Select tag",
    style="width: 100%",
    :filterOption="false",
    @search="fetch",
    @focus="() => fetch()",
    :notFoundContent="fetching ? undefined : 'No tags found'",
    @change="(e) => $emit('change', e)"
)
    a-spin(v-if="fetching", slot="notFoundContent", size="small")
    a-select-option(v-for="d in data", :key="d.value") {{ d.text }}

    slot(v-for="(_, name) in $slots", :name="name", :slot="name")
</template>

<script lang="ts">
import { Component, Prop, Vue } from "vue-property-decorator";
import { Select, Spin } from "ant-design-vue";
import { committeeApi } from "../../scripts/api/committee";
import { Option } from "ant-design-vue/types/select/option";

@Component({
    components: {
        "a-spin": Spin,
        "a-select": Select,
        "a-select-option": Select.Option,
    },
})
export default class FormCommitteeTagField extends Vue {
    @Prop() value;
    private data: Partial<Option>[] = [];
    private fetching = false;

    private async fetch() {
        this.data = [];
        this.fetching = true;
        let data = await committeeApi.getAllTags();
        let tags = data.data;
        this.data = tags.map((tag) => ({
            text: tag.name[this.$viaduct.locale],
            value: tag.id.toString(),
        }));

        this.fetching = false;
    }
}
</script>

<style scoped lang="sass">

.form-display-description
    margin-left: 4px
</style>
