import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import PimpyTasks from "./pimpy_tasks.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

const consoleSpy = jest.spyOn(console, "error");
const userGroupsPropMock = [
    { id: 1, name: "Group A", maillist: null, mailtype: "none" },
    { id: 2, name: "Group B", maillist: null, mailtype: "none" },
];

jest.mock("../../scripts/api/pimpy", () => ({
    pimpyApi: {
        getTasksForGroup: jest.fn(() => ({
            data: [
                {
                    group: {
                        id: 1,
                        name: "Group A",
                        maillist: null,
                        mailtype: "none",
                    },
                    tasks: [
                        {
                            b32_id: "ABC",
                            title: "Taak 1",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                        {
                            b32_id: "DEF",
                            title: "Taak 2",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman", "Florens Douwes"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                    ],
                },
            ],
        })),
        getTasksForUser: jest.fn(() => ({
            data: [
                {
                    group: {
                        id: 1,
                        name: "Group A",
                        maillist: null,
                        mailtype: "none",
                    },
                    tasks: [
                        {
                            b32_id: "ABC",
                            title: "Taak 1",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                        {
                            b32_id: "DEF",
                            title: "Taak 2",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-26",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman", "Maico Timmerman2"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                    ],
                },
                {
                    group: {
                        id: 2,
                        name: "Group B",
                        maillist: null,
                        mailtype: "none",
                    },
                    tasks: [
                        {
                            b32_id: "GHI",
                            title: "Taak 3",
                            group_id: 2,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                        {
                            b32_id: "JKL",
                            title: "Taak 4",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-26",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman", "Maico Timmerman2"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                    ],
                },
            ],
        })),
        getTasks: jest.fn(() => ({
            data: [
                {
                    group: {
                        id: 1,
                        name: "Group A",
                        maillist: null,
                        mailtype: "none",
                    },
                    tasks: [
                        {
                            b32_id: "ABC",
                            title: "Taak 1",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                        {
                            b32_id: "DEF",
                            title: "Taak 2",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman", "Florens Douwes"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                    ],
                },
                {
                    group: {
                        id: 2,
                        name: "Group B",
                        maillist: null,
                        mailtype: "none",
                    },
                    tasks: [
                        {
                            b32_id: "GHI",
                            title: "Taak 3",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                        {
                            b32_id: "JKL",
                            title: "Taak 4",
                            group_id: 1,
                            status: "remove",
                            created: "2020-02-06",
                            modified: "2020-10-22",
                            users: ["Maico Timmerman", "Wilco Kruijer"],
                            content: "",
                            minute_id: 1,
                            line: 1,
                        },
                    ],
                },
            ],
        })),
    },
}));
import { pimpyApi } from "../../scripts/api/pimpy";

describe(PimpyTasks.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PimpyTasks, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        getByText("Tasks");
    });
    test("Show tasks for all users in all groups", async () => {
        const { getByText, getAllByText } = render(PimpyTasks, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            propsData: {
                userGroups: userGroupsPropMock,
                self: false,
                groupId: null,
            },
        });
        await flushPromises();
        expect(pimpyApi.getTasksForUser).toBeCalledTimes(0);
        expect(pimpyApi.getTasks).toBeCalledTimes(1);
        expect(pimpyApi.getTasksForGroup).toBeCalledTimes(0);

        getByText(/Group A/);
        getByText(/Group B/);

        expect(getAllByText(/Maico Timmerman/)).toHaveLength(7);
        expect(getAllByText(/Florens Douwes/)).toHaveLength(3);
        expect(getAllByText(/Wilco Kruijer/)).toHaveLength(3);

        expect(getAllByText(/ABC/)).toHaveLength(1);
        expect(getAllByText(/DEF/)).toHaveLength(2);
        expect(getAllByText(/GHI/)).toHaveLength(1);
        expect(getAllByText(/JKL/)).toHaveLength(2);
        expect(getAllByText(/Taak 1/)).toHaveLength(1);
        expect(getAllByText(/Taak 2/)).toHaveLength(2);
        expect(getAllByText(/Taak 3/)).toHaveLength(1);
        expect(getAllByText(/Taak 4/)).toHaveLength(2);
    });
    test("Show tasks for all users in single groups", async () => {
        const { getByText, getAllByText, queryByText } = render(PimpyTasks, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            propsData: {
                userGroups: userGroupsPropMock,
                self: false,
                groupId: 1,
            },
        });
        await flushPromises();
        expect(pimpyApi.getTasksForUser).toBeCalledTimes(0);
        expect(pimpyApi.getTasks).toBeCalledTimes(0);
        expect(pimpyApi.getTasksForGroup).toBeCalledTimes(1);

        getByText(/Group A/);
        expect(queryByText(/Group B/)).toBeNull();
        expect(queryByText(/Taak 3/)).toBeNull();
        expect(queryByText(/Taak 4/)).toBeNull();
        expect(queryByText(/GHI/)).toBeNull();
        expect(queryByText(/JKL/)).toBeNull();

        expect(getAllByText(/Maico Timmerman/)).toHaveLength(4);
        expect(getAllByText(/Florens Douwes/)).toHaveLength(3);
        expect(queryByText(/Wilco Kruijer/)).toBeNull();

        expect(getAllByText(/ABC/)).toHaveLength(1);
        expect(getAllByText(/DEF/)).toHaveLength(2);
        expect(getAllByText(/Taak 1/)).toHaveLength(1);
        expect(getAllByText(/Taak 2/)).toHaveLength(2);
    });
    test("Show tasks for self in all groups ", async () => {
        const { getByText, getAllByText } = render(PimpyTasks, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            propsData: {
                userGroups: userGroupsPropMock,
                self: true,
                groupId: undefined,
            },
        });
        await flushPromises();
        expect(pimpyApi.getTasksForUser).toBeCalledTimes(1);
        expect(pimpyApi.getTasks).toBeCalledTimes(0);
        expect(pimpyApi.getTasksForGroup).toBeCalledTimes(0);

        getByText(/Group A/);
        getByText(/Taak 1/);
        getByText(/Taak 2/);
        getByText(/Group B/);
        getByText(/Taak 3/);
        getByText(/Taak 4/);
        getByText(/ABC/);
        getByText(/DEF/);

        // Only find the name of the user in the task description, not the
        // header. This cannot be exact (e.g. <td>Maico Timmerman</td>) as
        // tasks may be shared between users
        const names = getAllByText("Maico Timmerman", { exact: false });
        // Only in the task description
        expect(names).toHaveLength(4);

        // Not in the headers.
        for (const task of names) {
            expect(task.nodeName).not.toBe("h4");
        }
    });
    test("Show tasks for self in single group", async () => {
        const { getByText, getAllByText, queryByText } = render(PimpyTasks, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            propsData: {
                userGroups: userGroupsPropMock,
                self: true,
                groupId: 1, // Group A
            },
        });
        await flushPromises();
        expect(pimpyApi.getTasksForUser).toBeCalledTimes(1);
        expect(pimpyApi.getTasks).toBeCalledTimes(0);
        expect(pimpyApi.getTasksForGroup).toBeCalledTimes(0);

        getByText(/Group A/);
        getByText(/Taak 1/);
        getByText(/Taak 2/);
        getByText(/ABC/);
        getByText(/DEF/);
        expect(queryByText(/Group B/)).toBeNull();
        expect(queryByText(/Taak 3/)).toBeNull();
        expect(queryByText(/Taak 4/)).toBeNull();
        expect(queryByText(/GHI/)).toBeNull();
        expect(queryByText(/JKL/)).toBeNull();

        // Only find the name of the user in the task description, not the
        // header. This cannot be exact (e.g. <td>Maico Timmerman</td>) as
        // tasks may be shared between users
        const names = getAllByText("Maico Timmerman", { exact: false });
        // Only in the task description
        expect(names).toHaveLength(2);

        // Not in the headers.
        for (const task of names) {
            expect(task.nodeName).not.toBe("h4");
        }
    });
});
