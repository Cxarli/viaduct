export const courseApi = {
    getCourse: jest.fn(() => ({
        data: {
            id: 74,
            name: "Advanced Networking",
            datanose_code: "5384ADNE6Y",
        },
    })),
};
