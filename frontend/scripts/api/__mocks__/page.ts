export const pageApi = {
    renderPage: jest.fn((page, locale) => ({
        data: {
            title: `${page} ${locale} title`,
            content: `${page} ${locale} content `,
        },
    })),
};
