import { Api, PaginatedResponse } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";
import { Course } from "../../types/examination";

class CourseApi extends Api {
    getCourses(
        search: string,
        page: number,
        dn_course_ids: number[],
        user_educations: [number, number][]
    ) {
        return this.get<PaginatedResponse<Course>>(
            Flask.url_for("api.courses", {
                search: search,
                page: page,
                dn_course_ids: dn_course_ids,
                user_educations: user_educations,
            })
        );
    }

    getCourse(courseId: number): AxiosPromise<Course> {
        return this.get<Course>(
            Flask.url_for("api.course", { course: courseId })
        );
    }

    getUserCourses(userId: number | null = null): AxiosPromise<Course[]> {
        return this.get<Course[]>(
            Flask.url_for("api.user.courses", {
                user: userId ? userId : "self",
            })
        );
    }
}

export const courseApi = new CourseApi();
