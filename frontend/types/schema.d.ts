export type JsonSchema = {
    title: string;
    type: "object";
    properties: { [name: string]: JsonSchemaProperty };
    required?: string[];
};

interface JsonSchemaPropertyBase {
    title: string;
    default?: string;
    enum?: string[];
}

export interface JsonSchemaStringProperty extends JsonSchemaPropertyBase {
    type: "string";
    format?: "textarea";
}

export interface JsonSchemaBooleanProperty extends JsonSchemaPropertyBase {
    type: "boolean";
    format: undefined;
}

export interface JsonSchemaIntegerProperty extends JsonSchemaPropertyBase {
    type: "integer";
    format?: "user";
}

export type JsonSchemaProperty =
    | JsonSchemaStringProperty
    | JsonSchemaBooleanProperty
    | JsonSchemaIntegerProperty;
