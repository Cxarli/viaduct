export class User {
    public id: number;
    public email: string;
    public first_name: string;
    public last_name: string;
    public student_id: string;
    public phone_nr: string;
    public member: boolean;
    public favourer: boolean;
    public member_of_merit_date: string | null;
    public disabled: boolean;
    public address: string;
    public city: string;
    public zip: string;
    public iban: string;
}
