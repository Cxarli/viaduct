class ActivityDetails {
    public id: number;
    public nl_name: string;
    public en_name: string;
    public end_time: string;
    public pretix_event_slug: string;
}

export class Form {
    public id: string;
    public name: string;
    public attendants: number;
    public archived: string;
    public followed: boolean;
    public activity: ActivityDetails;
}
