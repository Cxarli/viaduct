export class Photo {
    public id: string;
    public shorturl: string;
    public highres_url: string;
    public origin_url: string;
    public title: string;
    public url: string;
}

export class Album {
    public id: string;
    public title: string;
    public date_created: string;
    public description: string;
    public primary_photo: string;
    public photos: Photo[];
    public photo_count: number;
}
