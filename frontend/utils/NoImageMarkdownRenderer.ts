import { Renderer } from "marked-ts";

export class NoImageMarkdownRenderer extends Renderer {
    /**
     * Instead of rendering an <img> tag it just renders an <a> to the image.
     * @param href
     * @param title
     * @param text
     */
    image(href: string, title: string, text: string): string {
        title = title ? `${title} (click)` : "Image (click)";
        return this.link(href, title, text);
    }
}
