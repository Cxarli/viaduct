import { Renderer } from "marked-ts";

export class PlainMarkdownRenderer extends Renderer {
    code(code: string): string {
        return code;
    }

    blockquote(quote: string): string {
        return quote;
    }

    html(html: string): string {
        return html;
    }

    heading(text: string): string {
        return text + ": ";
    }

    hr(): string {
        return " ";
    }

    list(body: string): string {
        return body + "; ";
    }

    listitem(text: string): string {
        return text + " ";
    }

    paragraph(text: string): string {
        return text + " ";
    }

    table(header: string, body: string): string {
        return header + " " + body;
    }

    tablerow(content: string): string {
        return " " + content + " ";
    }

    tablecell(content: string): string {
        return " " + content + " ";
    }

    //*** Inline level renderer methods. ***

    strong(text: string): string {
        return text;
    }

    em(text: string): string {
        return text;
    }

    codespan(text: string): string {
        return text;
    }

    br(): string {
        return " ";
    }

    del(text: string): string {
        return text;
    }

    link(href: string, title: string, text: string): string {
        if (this.options.sanitize) {
            let prot: string;

            try {
                prot = decodeURIComponent(this.options.unescape!(href))
                    .replace(/[^\w:]/g, "")
                    .toLowerCase();
            } catch (e) {
                return text;
            }

            if (
                prot.indexOf("javascript:") === 0 ||
                prot.indexOf("vbscript:") === 0 ||
                prot.indexOf("data:") === 0
            ) {
                return text;
            }
        }

        return text;
    }

    image(href: string, title: string, text: string): string {
        return text;
    }
}
