import axios from "axios";
import Vue from "vue";

export function errorHandler(error: Error, vm: Vue): void {
    if (!axios.isAxiosError(error)) {
        throw error;
    }

    if (error.response && error.response.status == 401) {
        vm.$notification.error({
            message: "Authentication expired",
            description:
                "Your authentication token for this page has expired. Please refresh the page.",
        });
    } else if (
        error.response &&
        error.response.status === 400 &&
        error.response.data &&
        error.response.data.data.errors
    ) {
        vm.$notification.error({
            message: error.response.data.title,
            description: Object.entries(error.response.data.data)
                .map((error) => `Error for field ${error}: ${error}`)
                .join(),
        });
    } else if (
        error.response &&
        (error.response.status === 400 ||
            error.response.status === 409 ||
            error.response.status === 503)
    ) {
        vm.$notification.error({
            message: error.response.data.title,
            description: error.response.data.detail,
        });
    } else if (error.response && error.response.status === 500) {
        vm.$notification.error({
            message: "Internal Server Error",
            description:
                "An unexpected error occurred, our team has been notified",
        });
    }
}
